package ec.com.designtechx.taxidriver.ui.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import ec.com.designtechx.taxidriver.data.irepo.IRepoClient
import ec.com.designtechx.taxidriver.data.irepo.IRepoDriver
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class SplashScreenVM @Inject constructor(
    private val dataStoreManager: DataStoreManager,
    private val IRepoDriver: IRepoDriver,
    private val IRepoClient: IRepoClient,
    private val iRepoAuth: IRepoAuth
) : ViewModel() {

    private val _existSession = MutableStateFlow(Results.UNANSWERED)
    private val _idUser = MutableStateFlow("")
    private val _dataOfUser = MutableStateFlow(Results.UNANSWERED)

    val existSession: StateFlow<Results> get() = _existSession
    private val idUser: StateFlow<String> get() = _idUser
    val dataOfUser: StateFlow<Results> get() = _dataOfUser

    init {
        viewModelScope.launch { iRepoAuth.getId().collect { _idUser.value = it } }
        viewModelScope.launch {
            iRepoAuth.sessionOn()
                .collect { _existSession.value = if (it) Results.OK else Results.NON_EXISTENT }
        }
    }

    suspend fun getDataUser() {
        dataStoreManager.getAny(DataStoreManager.KEY_STATUS).collect {
            when (it) {
                Constants.V_DRIVER -> checkDataDriver()
                Constants.V_CLIENT -> checkDataClient()
            }
        }
    }

    private suspend fun checkDataDriver() {
        IRepoDriver.getDriver(idUser.value).collect { driver ->
            if (driver.id != "0") {
                val checkData =
                    driver.name.isNotEmpty() && driver.vehicleBrand.isNotEmpty() && driver.vehiclePlate.isNotEmpty()
                _dataOfUser.value = if (checkData) Results.OK else Results.EMPTY
            } else notExistent()
        }
    }

    private suspend fun checkDataClient() {
        IRepoClient.getClient(idUser.value).collect { client ->
            if (client.id != "0") {
                val checkData = client.name.isNotEmpty() && client.email.isNotEmpty()
                _dataOfUser.value = if (checkData) Results.OK else Results.EMPTY
            } else notExistent()
        }
    }

    private suspend fun notExistent() {
        iRepoAuth.logout()
        _dataOfUser.value = Results.NON_EXISTENT
    }
}