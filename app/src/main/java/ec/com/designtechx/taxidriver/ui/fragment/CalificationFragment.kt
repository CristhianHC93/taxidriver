package ec.com.designtechx.taxidriver.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.HistoryBooking
import ec.com.designtechx.taxidriver.databinding.FragmentCalificationBinding
import ec.com.designtechx.taxidriver.ui.view_models.CalificationVM
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.Results
import java.util.*

@AndroidEntryPoint
class CalificationFragment : Fragment() {
    private var _binding: FragmentCalificationBinding? = null
    private val binding get() = _binding!!

    private val viewModel: CalificationVM by viewModels()

    private var idClient = ""
    private var price = 0.0

    private var calification: Float = 0f

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentCalificationBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configInit()
        lifecycleScope.launchWhenStarted { viewModel.getStatus(idClient) }
        listenVM()
        eventClick()
        ratingBar()
    }

    private fun configInit() {
        requireArguments().let {
            idClient = it.getString(Constants.ID_CLIENT, "")
            price = it.getDouble(Constants.PRICE, 0.0)
        }
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            Toast.makeText(requireActivity(), getString(R.string.not_can_return), Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun ratingBar() {
        binding.ratingCalification.setOnRatingBarChangeListener { _, rating, _ ->
            calification = rating
        }
    }

    private fun fillText(clientBooking: HistoryBooking) {
        binding.txtOriginCalification.text = clientBooking.origin
        binding.txtDestinationCalification.text = clientBooking.destination
    }

    private fun eventClick() {
        binding.btCalification.setOnClickListener { qualify() }
    }

    private fun qualify() {
        if (calification > 0f) {
            val historyBooking = viewModel.historyBooking.value
            if (historyBooking.idHistoryBooking.isNotEmpty()) {
                historyBooking.timestamp = Date().time
                lifecycleScope.launchWhenStarted {
                    viewModel.getHistoryBooking(
                        historyBooking,
                        calification
                    )
                }
            }
        } else {
            Toast.makeText(
                requireContext(), getString(R.string.enter_calification), Toast.LENGTH_SHORT
            ).show()
        }
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.status.collect {
                when (it) {
                    Constants.V_DRIVER -> binding.btCalification.text =
                        getString(R.string.qualify_client)
                    Constants.V_CLIENT -> binding.btCalification.text =
                        getString(R.string.qualify_driver)
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.historyBooking.collect {
                if (it.idClient.isNotEmpty() && it.idClient != Constants.ID_NOT_EXIST)
                    fillText(it)
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.completeSaveOrUpdate.collect {
                when (it) {
                    Results.OK -> {
                        viewModel.clearDataStoreManager(DataStoreManager.KEY_STATUS_BOOKING)
                        findNavController().navigate(
                            R.id.mapFragment,
                            Bundle().apply { putBoolean(Constants.DATA_OF_USER, true) })
                    }
                    Results.FAIL -> Toast.makeText(
                        requireContext(),
                        getString(R.string.no_result_found),
                        Toast.LENGTH_SHORT
                    ).show()
                    else -> {
                    }
                }
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}