package ec.com.designtechx.taxidriver.data.irepo

import kotlinx.coroutines.flow.Flow

interface IRepoToken {
    suspend fun createToken(idUser: String):Flow<Boolean>
    suspend fun getToken(idUser: String):Flow<String>
}