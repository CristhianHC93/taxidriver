package ec.com.designtechx.taxidriver.data.model

data class HistoryBooking(
    val idHistoryBooking: String = "",
    val idClient: String = "",
    val idDriver: String = "",
    val destination: String = "",
    val origin: String = "",
    val time: String = "",
    val km: String = "",
    val status: String = "",
    val originLat: Double = 0.0,
    val originLng: Double = 0.0,
    val destinationLat: Double = 0.0,
    val destinationLng: Double = 0.0,
    var calificationClient: Float = 0f,
    var calificationDriver: Float = 0f,
    var timestamp: Long = 0,
)
