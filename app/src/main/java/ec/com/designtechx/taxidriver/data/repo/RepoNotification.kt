package ec.com.designtechx.taxidriver.data.repo

import ec.com.designtechx.taxidriver.data.irepo.IRepoNotification
import ec.com.designtechx.taxidriver.data.model.FCMBody
import ec.com.designtechx.taxidriver.provider.NotificationProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoNotification @Inject constructor(private val notificationProvider: NotificationProvider) :
    IRepoNotification {

    override suspend fun sendNotification(body: FCMBody) = flow {
        notificationProvider.sendNotification(body).collect { emit(it) }
    }
}