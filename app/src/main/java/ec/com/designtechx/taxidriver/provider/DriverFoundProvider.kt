package ec.com.designtechx.taxidriver.provider

import com.google.firebase.database.*
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.taxidriver.data.model.DriverFound
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class DriverFoundProvider {

    private val mDataBase: DatabaseReference = Firebase.database.reference.child("DriversFound")

    @ExperimentalCoroutinesApi
    fun create(driverFound: DriverFound) = callbackFlow {
        mDataBase.child(driverFound.idDriver).setValue(driverFound)
            .addOnCompleteListener { trySend(it.isSuccessful) }
        awaitClose { }
    }

    /*
     * SI UN CONDCUTOR ESTA RECIBIENDO LA NOTIFICACION
     */
    @ExperimentalCoroutinesApi
    fun getDriverFoundByIdDriver(idDriver: String): Flow<MutableList<DriverFound>> = callbackFlow {
        mDataBase.orderByChild("idDriver").equalTo(idDriver)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val listReturn = mutableListOf<DriverFound>()
                    for (d in snapshot.children) {
                        if (d.exists()) {
                            val driverFound = d.getValue(DriverFound::class.java)
                            listReturn.add(driverFound!!)
                        }
                    }
                    trySend(listReturn)
                }

                override fun onCancelled(error: DatabaseError) {
                }
            })
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun delete(idDriver: String) = callbackFlow {
        mDataBase.child(idDriver).removeValue().addOnCompleteListener { trySend(it.isSuccessful) }
        awaitClose { }
    }
}