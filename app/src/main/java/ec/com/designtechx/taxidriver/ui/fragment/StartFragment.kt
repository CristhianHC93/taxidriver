package ec.com.designtechx.taxidriver.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.FragmentStartBinding
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.GeneralHelper
import javax.inject.Inject

@AndroidEntryPoint
class StartFragment : Fragment() {
    private var _binding: FragmentStartBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var generalHelper: GeneralHelper

    @Inject
    lateinit var dataStore: DataStoreManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentStartBinding.inflate(inflater, container, false)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        eventClick()
    }

    private fun eventClick() {
        binding.btIAmClient.setOnClickListener { moveToFragment(Constants.V_CLIENT) }
        binding.btIAmDiver.setOnClickListener { moveToFragment(Constants.V_DRIVER) }
    }

    private fun moveToFragment(status: String) {
        if (generalHelper.isOnlineNet()) {
            lifecycleScope.launchWhenCreated { dataStore.saveAny(DataStoreManager.KEY_STATUS, status) }
            findNavController().navigate(R.id.authFragment)
        }else
            Toast.makeText(requireActivity(),requireActivity().getString(R.string.without_internet),Toast.LENGTH_LONG).show()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}