package ec.com.designtechx.taxidriver.provider

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class TokenProvider {
    private val dataBase: DatabaseReference = Firebase.database.reference.child("token")

    @ExperimentalCoroutinesApi
    suspend fun create(idUser: String) = callbackFlow {
        FirebaseMessaging.getInstance().token.addOnCompleteListener { task ->
            if (task.isSuccessful) {
                val token = task.result
                dataBase.child(idUser).setValue(token)
                    .addOnCompleteListener { trySend(it.isSuccessful) }
                // Log and toast
                Log.d(this@TokenProvider::class.java.simpleName, token)
            } else {
                trySend(false)
                Log.w(
                    this@TokenProvider::class.java.simpleName,
                    "Fetching FCM registration token failed", task.exception
                )
            }
        }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    suspend fun getToken(idUser: String): Flow<String> = callbackFlow {
        dataBase.child(idUser).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) trySend(snapshot.value.toString()) else trySend("")
            }

            override fun onCancelled(error: DatabaseError) {
                trySend("")
                Log.w(this@TokenProvider::class.java.simpleName, error.message)
            }
        })
        awaitClose { }
    }
}