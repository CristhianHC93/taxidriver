package ec.com.designtechx.taxidriver.ui.view_models

import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.*
import ec.com.designtechx.taxidriver.data.model.ClientBooking
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MapClientBookingVM @Inject constructor(
    private val iRepoGeoFire: IRepoGeoFire,
    private val iRepoAuth: IRepoAuth,
    private val iRepoDriver: IRepoDriver,
    private val iRepoClientBooking: IRepoClientBooking,
    private val iRepoGoogleApi: IRepoGoogleApi,
    private val dataStoreManager: DataStoreManager
) : ViewModel() {

    private val _statusBooking = MutableStateFlow("")
    private val _direction = MutableStateFlow("")
    private val _statusClientBookingListener = MutableStateFlow("")
    private val _clientBooking = MutableStateFlow(ClientBooking())
    private val _driver = MutableStateFlow(Driver())
    private val _driverLocationListener = MutableStateFlow(LatLng(0.0, 0.0))
    private val _idUser = MutableStateFlow("")

    val statusBooking: StateFlow<String> get() = _statusBooking
    val direction: StateFlow<String> get() = _direction
    val statusClientBookingListener: StateFlow<String> get() = _statusClientBookingListener
    val clientBooking: StateFlow<ClientBooking> get() = _clientBooking
    val driver: StateFlow<Driver> get() = _driver
    val driverLocationListener: StateFlow<LatLng> get() = _driverLocationListener
    val idUser: StateFlow<String> get() = _idUser

    init {
        viewModelScope.launch { iRepoAuth.getId().collect { _idUser.value = it } }
        if (statusBooking.value.isEmpty())
            viewModelScope.launch(Dispatchers.IO) {
                dataStoreManager.getAny(DataStoreManager.KEY_STATUS_BOOKING).collect {
                    configBooking(it)
                }
            }
    }

    fun getClientBooking() = viewModelScope.launch {
        iRepoClientBooking.getClientBooking(idUser.value).collect {
            _clientBooking.value = it
            viewModelScope.launch { getDriver(it.idDriver) }
        }
    }

    private fun configBooking(statusBooking: String) = viewModelScope.launch(Dispatchers.IO) {
        _statusBooking.value = statusBooking
        if (statusBooking == Constants.CREATE) saveStatusBookingPref(Constants.V_RIDE)
        //else saveStatusBookingPref(Constants.V_RIDE)
    }

    fun saveStatusBookingPref(status: String) = viewModelScope.launch(Dispatchers.IO) {
        dataStoreManager.saveAny(DataStoreManager.KEY_STATUS_BOOKING, status)
    }

    private fun getDriverLocationListener(idUser: String) = viewModelScope.launch(Dispatchers.IO) {
        iRepoGeoFire.getLocation(Constants.WORK_DRIVER, idUser)
            .collect { configDriverLocationListener(it) }
    }

    private fun configDriverLocationListener(latLng: LatLng) {
        _driverLocationListener.value = latLng
        if (statusBooking.value == Constants.V_START)
            viewModelScope.launch(Dispatchers.IO) {
                getDirections(
                    latLng,
                    LatLng(clientBooking.value.destinationLat, clientBooking.value.destinationLng)
                )
            }
        else
            viewModelScope.launch(Dispatchers.IO) {
                getDirections(
                    latLng, LatLng(clientBooking.value.originLat, clientBooking.value.originLng)
                )
            }
    }

    fun getDriver(idUser: String) = viewModelScope.launch {
        iRepoDriver.getDriver(idUser).collect {
            if (it.id != Constants.ID_NOT_EXIST) _driver.value = it.apply { id = idUser }
            viewModelScope.launch(Dispatchers.IO) { getDriverLocationListener(idUser) }
            //viewModelScope.launch(Dispatchers.IO) { getStatusClientBookingListener() }
        }
    }

    suspend fun getDirections(latLngOrigin: LatLng, latLngDestine: LatLng) =
        iRepoGoogleApi.getDirections(latLngOrigin, latLngDestine).collect { _direction.value = it }

    fun clearDataStoreManager(key: Preferences.Key<String>) =
        viewModelScope.launch { dataStoreManager.clearAny(key) }

    fun getStatusClientBookingListener() = viewModelScope.launch(Dispatchers.IO) {
        iRepoClientBooking.getStatusListener(idUser.value)
            .collect {
                _statusClientBookingListener.value = it
                _statusBooking.value = it
            }
    }
//    suspend fun getStatusBooking() {
//        dataStoreManager.getAny(DataStoreManager.KEY_STATUS_BOOKING)
//            .collect { _statusBooking.value = it }
//    }
}