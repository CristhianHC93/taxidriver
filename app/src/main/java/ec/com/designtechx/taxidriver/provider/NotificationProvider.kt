package ec.com.designtechx.taxidriver.provider

import ec.com.designtechx.taxidriver.data.model.FCMBody
import ec.com.designtechx.taxidriver.data.model.FCMResponse
import ec.com.designtechx.taxidriver.retrofit.RetrofitClient
import ec.com.designtechx.taxidriver.retrofit.RetrofitConf
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class NotificationProvider {
    private val url = "https://fcm.googleapis.com"

    @ExperimentalCoroutinesApi
    fun sendNotification(body: FCMBody): Flow<FCMResponse> = callbackFlow {
        val callback = object : Callback<FCMResponse> {
            override fun onResponse(call: Call<FCMResponse>, response: Response<FCMResponse>) {
                if (response.isSuccessful) response.body()?.let { trySend(it) } else trySend(FCMResponse())
            }

            override fun onFailure(call: Call<FCMResponse>, t: Throwable) {}
        }
        RetrofitClient.instance?.getClientObject(url)?.create(RetrofitConf::class.java)!!
            .send(body).enqueue(callback)
        awaitClose { }
    }
}