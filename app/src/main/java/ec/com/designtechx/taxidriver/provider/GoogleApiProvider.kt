package ec.com.designtechx.taxidriver.provider

import android.content.Context

import com.google.android.gms.maps.model.LatLng
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.retrofit.RetrofitClient
import ec.com.designtechx.taxidriver.retrofit.RetrofitConf
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.util.*

import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GoogleApiProvider(val context: Context) {
    @ExperimentalCoroutinesApi
    fun getDirections(originLatLng: LatLng, destineLatLng: LatLng): Flow<String> = callbackFlow {
        val baseUrl = "https://maps.googleapis.com"
        val query =
            ("/maps/api/directions/json?mode=driving&transit_routing_preferences=less_driving&"
                    + "origin=" + originLatLng.latitude + "," + originLatLng.longitude + "&"
                    + "destination=" + destineLatLng.latitude + "," + destineLatLng.longitude + "&"
                    + "departure_time=" + (Date().time + 60 * 60 * 1000) + "&"
                    + "traffic_model=best_guess&"
                    + "key=" + context.resources.getString(R.string.google_map_key))

        val callback = object : Callback<String?> {
            override fun onResponse(call: Call<String?>, response: Response<String?>) {
                if (response.isSuccessful) response.body()?.let { trySend(it) } else trySend("")
            }

            override fun onFailure(call: Call<String?>, t: Throwable) {
                trySend(t.message.toString())
            }
        }
        RetrofitClient.instance!!.getClient(baseUrl).create(RetrofitConf::class.java)
            .getDirections(baseUrl + query).enqueue(callback)
        awaitClose { }
    }
}