package ec.com.designtechx.taxidriver.ui.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.repo.RepoAuth
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class HistoryBookingVM @Inject constructor(
    private val iRepoAuth: RepoAuth,
    private val dataStoreManager: DataStoreManager
) : ViewModel() {

    private val _idUser = MutableStateFlow("")
    private val _status = MutableStateFlow("")

    val idUser: StateFlow<String> get() = _idUser
    val status: StateFlow<String> get() = _status

    init {
        viewModelScope.launch { iRepoAuth.getId().collect { _idUser.value = it } }
    }

    fun getStatus() = viewModelScope.launch {
        dataStoreManager.getAny(DataStoreManager.KEY_STATUS).collect { _status.value = it }
    }
}