package ec.com.designtechx.taxidriver.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.FragmentRecoveryPasswordBinding
import ec.com.designtechx.taxidriver.provider.AuthProvider

class RecoveryPasswordFragment : Fragment() {
    private var _binding: FragmentRecoveryPasswordBinding? = null
    private val binding get() = _binding!!

    private val authProvider = AuthProvider.instance

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRecoveryPasswordBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickButton()
    }

    private fun clickButton() {
        binding.btRecoveryPassword.setOnClickListener { recoveryPassword(binding.txtUsername.text.toString()) }
    }

    private fun recoveryPassword(email: String) {
        if (validateField()) {
            authProvider!!.resetPassword(email).addOnCompleteListener {
                if (it.isSuccessful) {
                    showSucessfull()
                } else {
                    showAlert()
                }
                activeProgressBar(false)
            }
        }
    }

    private fun showSucessfull() {
        Toast.makeText(requireContext(), getString(R.string.email_sent), Toast.LENGTH_SHORT).show()
        findNavController().navigate(R.id.loginFragment)
    }

    private fun showAlert() {
        try {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Error")
            builder.setMessage("Imposible enviar Correo")
            builder.setPositiveButton("Aceptar", null)
            val dialog = builder.create()
            dialog.show()
        } catch (_: Exception) {
        }
    }

    private fun validateField(): Boolean {
        var devolver = true
        if (binding.txtUsername.text!!.isEmpty()) {
            devolver = false
            Toast.makeText(requireContext(), getString(R.string.required_field), Toast.LENGTH_SHORT)
                .show()
        } else
            activeProgressBar(true)
        return devolver
    }


    private fun activeProgressBar(enabled: Boolean) {
        binding.progressBar.visibility = if (enabled) View.VISIBLE else View.GONE
        binding.btRecoveryPassword.isEnabled = !enabled
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}