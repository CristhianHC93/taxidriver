package ec.com.designtechx.taxidriver.utils

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import ec.com.designtechx.taxidriver.R
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException

class DataStoreManager constructor(private val context: Context) {
    private val Context.dataStore: DataStore<Preferences> by preferencesDataStore(
        name = context.getString(
            R.string.prefs_file
        )
    )
//    private val dataStore = context.createDataStore(context.getString(R.string.prefs_file))

    suspend fun getAny(key: Preferences.Key<String>): Flow<String> =
        context.dataStore.data.map { it[key] ?: Constants.ID_NOT_EXIST }
            .catch {
                if (it is IOException) {
                    it.printStackTrace()
                    emit(emptyPreferences().toString())
                } else throw it
            }

    suspend fun saveAny(key: Preferences.Key<String>, value: String) =
        context.dataStore.edit { it[key] = value }

    suspend fun clearAny(key: Preferences.Key<String>) =
        context.dataStore.edit { it[key] = "" }

    companion object {
        val KEY_STATUS = stringPreferencesKey(Constants.KEY_STATUS)
        val KEY_STATUS_BOOKING = stringPreferencesKey(Constants.KEY_STATUS_BOOKING)
        val KEY_ID_DRIVER = stringPreferencesKey(Constants.KEY_ID_DRIVER)
        val KEY_ID_CLIENT = stringPreferencesKey(Constants.KEY_ID_CLIENT)
    }
}