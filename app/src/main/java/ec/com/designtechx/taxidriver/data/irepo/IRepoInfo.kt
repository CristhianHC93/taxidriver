package ec.com.designtechx.taxidriver.data.irepo

import ec.com.designtechx.taxidriver.data.model.Info
import kotlinx.coroutines.flow.Flow

interface IRepoInfo {
    suspend fun getInfo():Flow<Info>
}