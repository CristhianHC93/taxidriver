package ec.com.designtechx.taxidriver.ui.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.*
import ec.com.designtechx.taxidriver.data.model.*
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class RequestDriverVM @Inject constructor(
    private val iRepoGeoFire: IRepoGeoFire,
    private val iRepoToken: IRepoToken,
    private val iRepoAuth: IRepoAuth,
    private val iRepoClientBooking: IRepoClientBooking,
    private val iRepoDriverFound: IRepoDriverFound,
    private val iRepoNotification: IRepoNotification,
    private val iRepoGoogleApi: IRepoGoogleApi,
    private val dataStoreManager: DataStoreManager
) :
    ViewModel() {


    private val _responseGoogleApi = MutableStateFlow("")
    private val _responseNotificationCancel = MutableStateFlow(FCMResponse())
    private val _responseNotification = MutableStateFlow(FCMResponse())
    private val _listDriverFound = MutableStateFlow(mutableListOf<DriverFound>())
    private val _completeCreateCB = MutableStateFlow(Results.UNANSWERED)
    private val _clientBooking = MutableStateFlow(ClientBooking())
    private val _clientBookingListen = MutableStateFlow(ClientBooking())
    private val _idUser = MutableStateFlow("")
    private val _active = MutableStateFlow(mapOf<String, Any>())
    private val _token = MutableStateFlow("")

    val responseNotificationCancel: StateFlow<FCMResponse> get() = _responseNotificationCancel
    val responseNotification: StateFlow<FCMResponse> get() = _responseNotification
    val listDriverFound: StateFlow<MutableList<DriverFound>> get() = _listDriverFound
    val completeCreateCB: StateFlow<Results> get() = _completeCreateCB
    val clientBooking: StateFlow<ClientBooking> get() = _clientBooking
    val clientBookingListen: StateFlow<ClientBooking> get() = _clientBookingListen
    val idUser: StateFlow<String> get() = _idUser
    val active: StateFlow<Map<String, Any>> get() = _active
    val token: StateFlow<String> get() = _token

    init {
        viewModelScope.launch { iRepoAuth.getId().collect { _idUser.value = it } }
    }

    fun saveStatusBookingPref(status: String) = viewModelScope.launch(Dispatchers.IO) {
        dataStoreManager.saveAny(DataStoreManager.KEY_STATUS_BOOKING, status)
    }

    suspend fun activeLocation(referent: String, latLng: LatLng) = viewModelScope.launch {
        iRepoGeoFire.getActive(referent, latLng, 10.0).collect { _active.value = it }
    }

    fun getToken(idUser: String) = viewModelScope.launch {
        iRepoToken.getToken(idUser).collect { _token.value = it }
    }

    fun getClientBooking(idClientBooking: String) = viewModelScope.launch {
        iRepoClientBooking.getClientBooking(idClientBooking).collect { _clientBooking.value = it }
    }

    suspend fun getClientBookingListen(idClientBooking: String) = viewModelScope.launch {
        iRepoClientBooking.getClientBookingListen(idClientBooking)
            .collect { if (it.idClient != "-1") _clientBookingListen.value = it }
    }

    fun deleteClientBooking(idClientBooking: String) = viewModelScope.launch {
        iRepoClientBooking.delete(idClientBooking).collect { }
    }

    fun createClientBooking(clientBooking: ClientBooking) = viewModelScope.launch {
        iRepoClientBooking.create(clientBooking)
            .collect { _completeCreateCB.value = if (it) Results.OK else Results.FAIL }
    }

    suspend fun deleteDriverFound(id: String) = viewModelScope.launch {
        iRepoDriverFound.delete(id).collect { }
    }

    suspend fun getDriverFoundByIdDriver(idDriver: String) = viewModelScope.launch {
        iRepoDriverFound.getDriverFoundByIdDriver(idDriver).collect { _listDriverFound.value = it }
    }

    fun createDriverFound(driverFound: DriverFound) = viewModelScope.launch(Dispatchers.IO) {
        iRepoDriverFound.create(driverFound).collect { }
    }

    suspend fun sendNotificationCancelAll(fcmBody: FCMBody) = viewModelScope.launch {
        iRepoNotification.sendNotification(fcmBody).collect { }
    }

    suspend fun sendNotification(fcmBody: FCMBody) = viewModelScope.launch {
        iRepoNotification.sendNotification(fcmBody).collect { _responseNotification.value = it }
    }

    suspend fun sendNotificationCancel(fcmBody: FCMBody) = viewModelScope.launch {
        iRepoNotification.sendNotification(fcmBody)
            .collect { _responseNotificationCancel.value = it }
    }

    @ExperimentalCoroutinesApi
    suspend fun getDirections(originLatLng: LatLng, desetinationLatLng: LatLng) =
        viewModelScope.launch {
            iRepoGoogleApi.getDirections(originLatLng, desetinationLatLng)
                .collect { _responseGoogleApi.value = it }
        }
}