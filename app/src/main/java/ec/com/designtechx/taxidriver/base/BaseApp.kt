package ec.com.designtechx.taxidriver.base

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * @author Cristhian Holguin on 26 February 2021
 */

@HiltAndroidApp
class BaseApp:Application()