package ec.com.designtechx.taxidriver.retrofit

import ec.com.designtechx.taxidriver.data.model.FCMBody
import ec.com.designtechx.taxidriver.data.model.FCMResponse
import retrofit2.Call
import retrofit2.http.*


interface RetrofitConf {
    /*IGoogleApi*/
    @GET
    fun getDirections(@Url url: String): Call<String>

    /*IFCMApi*/
    @Headers(
        "Content-Type:application/json", "Authorization:key=AAAA7yRs1gE:APA91bH0DoGx0ebBxZk7oPg-8UdlH63oI-rpr4IG78MxRR85W-4N9mrX3MPVP8kki_89qjz39fMpIlw3Xj9B-G5W44_4FOANNL8UM_Z_2ljhpjEx-ApZfnt4w7NY8LEK2jI4p9_nIM62"
    )
    @POST("fcm/send")
    fun send(@Body body: FCMBody): Call<FCMResponse>
}