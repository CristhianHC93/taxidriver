package ec.com.designtechx.taxidriver.data.model

data class Driver(
    var id: String="",
    var email: String="",
    val password: String="",
    var name: String="",
    val phone: String="",
    val vehicleBrand: String = "",
    val vehiclePlate: String = "",
    var image: String = ""
) {
    constructor(id: String, phone: String) : this(id, "", "", "", phone,"","","")
    constructor(
        id: String,
        email: String,
        name: String,
        vehicleBrand: String,
        vehiclePlate: String
    ) : this(id, email,"", name,"", vehicleBrand, vehiclePlate)
}