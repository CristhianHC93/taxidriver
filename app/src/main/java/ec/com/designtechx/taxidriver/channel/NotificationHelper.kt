package ec.com.designtechx.taxidriver.channel

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.ContextWrapper
import android.graphics.Color
import android.media.AudioAttributes
import android.net.Uri
import ec.com.designtechx.taxidriver.R

class NotificationHelper(base: Context?, private val soundUri: Uri) : ContextWrapper(base) {

    private var manager: NotificationManager? = null

    init {
        createChannel()
    }

    private fun createChannel() {
        val attr = AudioAttributes.Builder().apply {
            setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
            setUsage(AudioAttributes.USAGE_NOTIFICATION_RINGTONE)
        }.build()
        val notificationChannel =
            NotificationChannel(CHANNEL_ID, CHANNEL_NAME, NotificationManager.IMPORTANCE_HIGH)
                .apply {
                    enableLights(true)
                    enableVibration(true)
                    lightColor = Color.GRAY
                    lockscreenVisibility = Notification.VISIBILITY_PRIVATE
                    setSound(soundUri, attr)
                }
        getManager().apply { createNotificationChannel(notificationChannel) }
    }

    fun getManager(): NotificationManager {
        if (manager == null) manager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        return manager as NotificationManager
    }

    fun getNotification(
        title: String, body: String, intent: PendingIntent
    ): Notification.Builder {
        return Notification.Builder(applicationContext, CHANNEL_ID).apply {
            setContentText(title)
            setContentText(body).setAutoCancel(true)
            setContentIntent(intent)
            setSmallIcon(R.drawable.icon_car)
            style = Notification.BigTextStyle().bigText(body).setBigContentTitle(title)
        }
    }

    fun getNotificationActions(
        title: String, body: String,
        acceptAction: Notification.Action, cancelAction: Notification.Action
    ): Notification.Builder {
        return Notification.Builder(applicationContext, CHANNEL_ID).apply {
            setContentText(title)
            setContentText(body).setAutoCancel(true)
            setSmallIcon(R.drawable.icon_car)
            style = Notification.BigTextStyle().bigText(body).setBigContentTitle(title)
            addAction(acceptAction)
            addAction(cancelAction)
        }
    }
/*
    fun getNotificationOldApi(
        title: String, body: String, intent: PendingIntent, soundUri: Uri
    ): NotificationCompat.Builder {
        return NotificationCompat.Builder(applicationContext, CHANNEL_ID).apply {
            setContentTitle(title)
            setContentText(body)
            setAutoCancel(true)
            setSound(soundUri)
            setContentIntent(intent)
            setSmallIcon(R.drawable.icon_car)
            setStyle(NotificationCompat.BigTextStyle().bigText(body).setBigContentTitle(title))
        }

    }

    fun getNotificationOldApiActions(
        title: String, body: String, soundUri: Uri, acceptAction: NotificationCompat.Action,
        cancelAction: NotificationCompat.Action
    ): NotificationCompat.Builder {
        return NotificationCompat.Builder(applicationContext, CHANNEL_ID).apply {
            setContentTitle(title)
            setContentText(body)
            setAutoCancel(true)
            setSound(soundUri)
            setSmallIcon(R.drawable.icon_car)
            setStyle(NotificationCompat.BigTextStyle().bigText(body).setBigContentTitle(title))
            addAction(acceptAction)
            addAction(cancelAction)
        }

    }*/

    companion object {
        const val CHANNEL_ID = "ec.com.designtechx.taxidriver"
        const val CHANNEL_NAME = "TAXI_DRIVER"

    }
}