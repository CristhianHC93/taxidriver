package ec.com.designtechx.taxidriver.data.irepo

import ec.com.designtechx.taxidriver.data.model.ClientBooking
import kotlinx.coroutines.flow.Flow

interface IRepoClientBooking {
    suspend fun create(clientBooking: ClientBooking): Flow<Boolean>
    suspend fun delete(idClientBooking: String): Flow<Boolean>
    suspend fun getClientBooking(idClientBooking: String): Flow<ClientBooking>
    suspend fun getClientBookingListen(idClientBooking: String): Flow<ClientBooking>
    suspend fun updateStatus(idClientBooking: String, status: String): Flow<Boolean>
    suspend fun updateStatusAndIdDriver(
        idClientBooking: String, status: String, idDriver: String
    ): Flow<Boolean>

    suspend fun getStatusListener(idClientBooking: String):Flow<String>
    suspend fun updateIdHistoryBooking(idClientBooking: String):Flow<Boolean>
}