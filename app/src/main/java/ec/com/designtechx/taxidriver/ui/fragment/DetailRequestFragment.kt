package ec.com.designtechx.taxidriver.ui.fragment

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetBehavior.BottomSheetCallback
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.addPolyline
import com.google.maps.android.ktx.awaitMap
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.FragmentDetailRequestBinding
import ec.com.designtechx.taxidriver.ui.dialogFragment.BottonSheetPrice
import ec.com.designtechx.taxidriver.ui.view_models.DetailRequestVM
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DecodePoints
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.json.JSONArray
import org.json.JSONObject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class DetailRequestFragment : Fragment() {
    private var _binding: FragmentDetailRequestBinding? = null
    private val binding get() = _binding!!

    private val viewModel: DetailRequestVM by viewModels()

    private val latLonOrigin by lazy {
        LatLng(
            requireArguments().getDouble(Constants.ORIGIN_LAT),
            requireArguments().getDouble(Constants.ORIGIN_LNG)
        )
    }
    private val latLonDestine by lazy {
        LatLng(
            requireArguments().getDouble(Constants.DESTINE_LAT),
            requireArguments().getDouble(Constants.DESTINE_LNG)
        )
    }
    private val nameDestine by lazy { requireArguments().getString(Constants.DESTINE_NAME, "") }
    private val nameOrigin by lazy { requireArguments().getString(Constants.ORIGIN_NAME, "") }
    private val idDriver by lazy { requireArguments().getString(Constants.ID_DRIVER, "") }
    private val driverLatLng by lazy {
        LatLng(
            requireArguments().getDouble(Constants.DRIVER_LAT, 0.0),
            requireArguments().getDouble(Constants.DRIVER_LNG, 0.0)
        )
    }

    private lateinit var map: GoogleMap
    private var price = 0.0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentDetailRequestBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listenVM()
        fillView()
        configMap()
        eventClick()
        configBottonSheet()
    }

    private fun fillView() {
        binding.txtOrigin.setText(nameOrigin)
        binding.txtDestine.setText(nameDestine)
    }

    private fun configBottonSheet() {
        BottomSheetBehavior.from(binding.bottomSheetInfo)
            .addBottomSheetCallback(object : BottomSheetCallback() {
                override fun onStateChanged(bottomSheet: View, newState: Int) {
                    when (newState) {
                        BottomSheetBehavior.STATE_HIDDEN -> BottomSheetBehavior.from(binding.bottomSheetInfo)
                            .setState(BottomSheetBehavior.STATE_COLLAPSED)
                        else -> {
                        }

                    }
                }

                override fun onSlide(bottomSheet: View, slideOffset: Float) {
                }
            })
    }

    private fun eventClick() {
        binding.btRequestNow.setOnClickListener { goToRequestDriver() }
    }

    private fun goToRequestDriver() {
        val bundle = Bundle().apply {
            putDouble(Constants.ORIGIN_LAT, latLonOrigin.latitude)
            putDouble(Constants.ORIGIN_LNG, latLonOrigin.longitude)
            putDouble(Constants.DESTINE_LAT, latLonDestine.latitude)
            putDouble(Constants.DESTINE_LNG, latLonDestine.longitude)
            putString(Constants.ORIGIN_NAME, nameOrigin)
            putString(Constants.DESTINE_NAME, nameDestine)
        }
        if (idDriver.isNotEmpty()) {
            bundle.apply {
                putDouble(Constants.DRIVER_LAT, driverLatLng.latitude)
                putDouble(Constants.DRIVER_LNG, driverLatLng.longitude)
                putString(Constants.ID_DRIVER, idDriver)
            }
        }
        val modalBottomSheetFragment = BottonSheetPrice.newInstance(bundle, price)
        modalBottomSheetFragment!!.show(childFragmentManager, modalBottomSheetFragment.tag)
    }

    @ExperimentalCoroutinesApi
    private fun configMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(binding.map.id) as SupportMapFragment
        lifecycle.coroutineScope.launchWhenCreated {
            map = mapFragment.awaitMap()
            map.mapType = GoogleMap.MAP_TYPE_NORMAL
            map.uiSettings.isZoomControlsEnabled = true
            map.addMarker {
                position(latLonOrigin)
                title(getString(R.string.origin))
                icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_red))
            }
            map.addMarker {
                position(latLonDestine)
                title(getString(R.string.destine))
                icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_blue))
            }
            map.animateCamera(
                CameraUpdateFactory.newCameraPosition(
                    CameraPosition.Builder()
                        .target(latLonOrigin)
                        .zoom(16f)
                        .build()
                )
            )
            lifecycleScope.launchWhenStarted {
                viewModel.getDirections(latLonOrigin, latLonDestine)
            }
        }
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.response.collect { if (it.isNotEmpty()) drawRoute(it) }
        }
    }

    private fun drawRoute(response: String) {
        try {
            val jsonObject = JSONObject(response)
            val jsonArray: JSONArray = jsonObject.getJSONArray("routes")
            val route = jsonArray.getJSONObject(0)
            val polyline = route.getJSONObject("overview_polyline")
            val points = polyline.getString("points")
            val polylineList = DecodePoints.decodePoly(points)
            map.addPolyline {
                color(Color.DKGRAY)
                width(8f)
                startCap(SquareCap())
                jointType(JointType.ROUND)
                addAll(polylineList)
            }
            val legs = route.getJSONArray("legs")
            val leg = legs.getJSONObject(0)
            val distance = leg.getJSONObject("distance")
            val duration = leg.getJSONObject("duration")
            val distanceText = distance.getString("text")
            val durationText = duration.getString("text")
            binding.txtTime.setText(durationText)
            binding.txtDistance.setText(distanceText)
            val dinstanceAndKm = distanceText.split(" ")
            val distanceValue = dinstanceAndKm[0].toDouble()
            val durationAndKm = durationText.split(" ")
            val durationValue = durationAndKm[0].toDouble()
            calculatePrice(distanceValue, durationValue)
        } catch (e: Exception) {
            Log.e(
                this@DetailRequestFragment::class.java.name,
                "Error encontrado " + e.message
            )
        }
    }

    private fun calculatePrice(distanceValue: Double, durationValue: Double) {
        val info = viewModel.info.value
        val totalDistance = distanceValue * (info.km)
        val totalDuration = durationValue * (info.min)
        price = totalDistance + totalDuration
        val minValue = price - (price * 0.10)
        val maxValue = price + (price * 0.10)
        val max = String.format("%.2f", maxValue)
        val min = String.format("%.2f", minValue)
        val valueText = "$min$ -$max$"
        binding.txtSuggestedPrice.setText(valueText)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}