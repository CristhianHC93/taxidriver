package ec.com.designtechx.taxidriver.data.repo

import android.content.Context
import ec.com.designtechx.taxidriver.data.irepo.IRepoDriver
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.provider.DriverProvider
import ec.com.designtechx.taxidriver.provider.ImageProvider
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import java.io.File
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoDriver @Inject constructor(
    private val driverProvider: DriverProvider,
    private val imageProvider: ImageProvider
) : IRepoDriver {
    override suspend fun getDriver(id: String): Flow<Driver> = flow {
        driverProvider.getDriver(id).collect { emit(it) }
    }

    override suspend fun saveDriver(driver: Driver): Flow<Boolean> = callbackFlow {
        driverProvider.create(driver).addOnCompleteListener { trySend(it.isSuccessful) }
        awaitClose { }
    }

    override suspend fun updateDriver(driver: Driver): Flow<Results> = callbackFlow {
        driverProvider.update(driver).addOnCompleteListener {
            if (it.isSuccessful) trySend(Results.OK) else trySend(Results.FAIL)
        }
        awaitClose { }
    }

    override suspend fun saveImageDriver(
        driver: Driver,
        imageFile: File,
        context: Context,
        id: String
    ): Flow<String> =
        callbackFlow {
            imageProvider.selectRef("driver_image")
                .saveImage(context, imageFile, id)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        imageProvider.getStorage().downloadUrl.addOnCompleteListener { uri ->
                            if (uri.isSuccessful) trySend(uri.result.toString()) else trySend("")
                        }
                    } else {
                        trySend("")
                    }
                }
            awaitClose { }
        }
}