package ec.com.designtechx.taxidriver.data.irepo

import android.content.Context
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.flow.Flow
import java.io.File

interface IRepoDriver {
    suspend fun getDriver(id:String): Flow<Driver>
    suspend fun saveDriver(driver: Driver): Flow<Boolean>
    suspend fun updateDriver(driver: Driver): Flow<Results>
    suspend fun saveImageDriver(driver: Driver, imageFile: File, context: Context,id:String): Flow<String>
}