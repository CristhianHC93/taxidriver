package ec.com.designtechx.taxidriver.ui.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.FirebaseDatabase
import com.google.firebase.database.Query
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.data.model.HistoryBooking
import ec.com.designtechx.taxidriver.databinding.FragmentHistoryBookingBinding
import ec.com.designtechx.taxidriver.ui.activity.MainActivity
import ec.com.designtechx.taxidriver.ui.adapter.HistoryBookingAdapter
import ec.com.designtechx.taxidriver.ui.view_models.HistoryBookingVM
import ec.com.designtechx.taxidriver.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class HistoryBookingFragment : Fragment() {
    private var _binding: FragmentHistoryBookingBinding? = null
    private val binding get() = _binding!!

    private val viewModel: HistoryBookingVM by viewModels()

    private lateinit var historyBookingAdapter: HistoryBookingAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHistoryBookingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.VISIBLE
        viewModel.getStatus()
        listenVM()
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.status.collect {
                if (it.isNotEmpty() && it != Constants.ID_NOT_EXIST) configRecyclerAndAdapter()
            }
        }
    }

    private fun configRecyclerAndAdapter() {
        val linearLayoutManager = LinearLayoutManager(requireContext())
        binding.rvHistoryBooking.layoutManager = linearLayoutManager
        val query: Query = FirebaseDatabase.getInstance().reference
            .child("history_booking")
            .orderByChild(if (viewModel.status.value == Constants.V_DRIVER) "idDriver" else "idClient")
            .equalTo(viewModel.idUser.value)
        val options = FirebaseRecyclerOptions.Builder<HistoryBooking>()
            .setQuery(query, HistoryBooking::class.java)
            .build()
        historyBookingAdapter = HistoryBookingAdapter(
            options,
            requireContext(),
            (viewModel.status.value == Constants.V_DRIVER)
        )
        binding.rvHistoryBooking.adapter = historyBookingAdapter
        historyBookingAdapter.startListening()
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.INVISIBLE
    }

    override fun onStop() {
        super.onStop()
        historyBookingAdapter.stopListening()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}