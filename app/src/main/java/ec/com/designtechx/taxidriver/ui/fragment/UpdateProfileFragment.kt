package ec.com.designtechx.taxidriver.ui.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.BitmapFactory
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.*
import android.widget.Toast
import androidx.activity.addCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.databinding.FragmentUpdateProfileBinding
import ec.com.designtechx.taxidriver.ui.activity.MainActivity
import ec.com.designtechx.taxidriver.ui.view_models.UpdateProfileVM
import ec.com.designtechx.taxidriver.utils.*
import java.io.File
import java.io.IOException

@AndroidEntryPoint
class UpdateProfileFragment : Fragment() {
    private var _binding: FragmentUpdateProfileBinding? = null
    private val binding get() = _binding!!

    private val viewModel: UpdateProfileVM by viewModels()
    private val validateField by lazy { ValidateField.instance!! }
    private var statusDriver = true

    private var urlImage = ""
    private var imageFile: File? = null

    private var valName = false
    private var valEmail = false
    private var valBrand = false
    private var valPlate = false

    private lateinit var resultGPS: ActivityResultLauncher<Intent>
    private lateinit var resultCamera: ActivityResultLauncher<Intent>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        resultGPS =
            registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
                if (it.resultCode == RESULT_OK) {
                    try {
                        it.data?.data?.let { data ->
                            imageFile = FileUtil.from(requireContext(), data)
                            binding.imgProfile.setImageBitmap(BitmapFactory.decodeFile(imageFile!!.absolutePath))
                            urlImage = imageFile!!.absolutePath
                        }
                    } catch (e: Exception) {
                        Log.e(this::class.java.simpleName, e.message!!)
                    }
                }
            }

        resultCamera = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
            if (it.resultCode == RESULT_OK) handleCameraPhoto()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentUpdateProfileBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getStatus()
        eventClick()
        listenChangeEditText()
        backPressed()
    }

    private fun getStatus() {
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.VISIBLE
        lifecycleScope.launchWhenCreated {
            viewModel.status.collect {
                statusDriver = it == Constants.V_DRIVER
                when (statusDriver) {
                    true -> configUIDriver()
                    false -> configUIClient()
                }
            }
        }
    }

    private fun eventClick() {
        binding.imgProfile.setOnClickListener { openGallery() }
        binding.btUpdateProfile.setOnClickListener {
            if (statusDriver) updateProfileDriver(getDriver()) else updateProfileClient(getClient())
        }
        binding.btCamera.setOnClickListener { takePhoto() }
    }

    private fun listenChangeEditText() {
        binding.txtName.doAfterTextChanged {
            valName = validateField.validateNotEmpty(
                binding.txtName, binding.tilName, requireContext()
            )
        }
        binding.txtEmail.doAfterTextChanged {
            valEmail =
                validateField.validateEmail(binding.txtEmail, binding.tilEmail, requireContext())
        }
        //if (statusDriver) {
        binding.txtVehicleBrand.doAfterTextChanged {
            valBrand = validateField.validateNotEmpty(
                binding.txtVehicleBrand, binding.tilVehicleBrand, requireContext()
            )
        }
        binding.txtVehiclePlate.doAfterTextChanged {
            valPlate = validateField.validateNotEmpty(
                binding.txtVehiclePlate, binding.tilVehiclePlate, requireContext()
            )
        }
        //}
    }

    private fun backPressed() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            if (viewModel.dataOfUser.value != Results.OK)
                Toast.makeText(
                    requireActivity(), getString(R.string.required_data_for_continue),
                    Toast.LENGTH_LONG
                ).show()
            else (requireActivity() as MainActivity).navController.navigateUp()
        }
    }

    private fun configUIDriver() {
        binding.tilVehicleBrand.visibility = View.VISIBLE
        binding.tilVehiclePlate.visibility = View.VISIBLE
        lifecycleScope.launchWhenCreated {
            viewModel.profileDriver.collect { fillTextDriver(it) }
        }
    }

    private fun configUIClient() {
        binding.tilVehicleBrand.visibility = View.GONE
        binding.tilVehiclePlate.visibility = View.GONE
        lifecycleScope.launchWhenCreated {
            viewModel.profileClient.collect { fillTextClient(it) }
        }
    }

    private fun getDriver(): Driver {
        return if (valName && valEmail && valBrand && valPlate && urlImage.isNotEmpty()) {
            val id = viewModel.idUser.value
            val name = binding.txtName.text.toString()
            val email = binding.txtEmail.text.toString()
            val vehicleBrand = binding.txtVehicleBrand.text.toString()
            val vehiclePlate = binding.txtVehiclePlate.text.toString()
            Driver(id, email, name, vehicleBrand, vehiclePlate)
        } else Driver()
    }

    private fun getClient(): Client {
        return if (valName && valEmail && urlImage.isNotEmpty()) {
            val id = viewModel.idUser.value
            val name = binding.txtName.text.toString()
            val email = binding.txtEmail.text.toString()
            Client(id, email, name)
        } else Client()
    }

    private fun updateProfileDriver(driver: Driver) {
        if (driver.id.isNotEmpty()) {
            activeProgressBar(true)
            if (imageFile == null) updateDriver(driver.apply { image = urlImage })
            else {
                lifecycleScope.launchWhenCreated {
                    viewModel.saveImageDriver(driver, imageFile!!, requireContext())
                }
                lifecycleScope.launchWhenCreated {
                    viewModel.uriImage.collect {
                        if (it.isNotEmpty()) completeSaveImageDriver(it, driver)
                    }
                }
            }
        } else
            Toast.makeText(requireContext(), getString(R.string.required_data), Toast.LENGTH_SHORT)
                .show()
    }


    private fun completeSaveImageDriver(image: String, driver: Driver) {
        if (image.isNotEmpty()) {
            driver.image = image
            updateDriver(driver)
        } else {
            Toast.makeText(
                requireContext(), "Hubo un error al subir la imagen",
                Toast.LENGTH_SHORT
            ).show()
            activeProgressBar(false)
        }
    }


    private fun completeSaveImageClient(image: String, client: Client) {
        if (image.isNotEmpty()) {
            client.image = image
            updateClient(client)
        } else {
            Toast.makeText(
                requireContext(), "Hubo un error al subir la imagen",
                Toast.LENGTH_SHORT
            ).show()
            activeProgressBar(false)
        }
    }

    private fun updateDriver(driver: Driver) {
        lifecycleScope.launchWhenCreated { viewModel.updateDriver(driver) }
        lifecycleScope.launchWhenCreated { viewModel.updateProfile.collect { completeUpdate(it) } }
    }

    private fun completeUpdate(complete: Results) {
        if (complete == Results.OK) {
            Toast.makeText(
                requireContext(), "Su informacion se actualizo correctamente", Toast.LENGTH_SHORT
            ).show()
            findNavController().navigate(
                R.id.mapFragment,
                Bundle().apply { putBoolean(Constants.DATA_OF_USER, true) })
        } else if (complete == Results.FAIL) {
            Toast.makeText(
                requireContext(), "Hubo un error al actualizar Cliente", Toast.LENGTH_SHORT
            ).show()
        }
        activeProgressBar(false)
    }


    private fun updateProfileClient(client: Client) {
        if (client.id.isNotEmpty()) {
            activeProgressBar(true)
            if (imageFile == null) updateClient(client.apply { image = urlImage }) else {
                lifecycleScope.launchWhenCreated {
                    viewModel.saveImageClient(client, imageFile!!, requireContext())
                }
                lifecycleScope.launchWhenCreated {
                    viewModel.uriImage.collect {
                        if (it.isNotEmpty()) completeSaveImageClient(it, client)
                    }
                }
            }
        } else Toast.makeText(
            requireContext(), getString(R.string.required_data), Toast.LENGTH_SHORT
        ).show()
    }


    private fun updateClient(client: Client) {
        lifecycleScope.launchWhenCreated { viewModel.updateClient(client) }
        lifecycleScope.launchWhenCreated { viewModel.updateProfile.collect { completeUpdate(it) } }
    }

    private fun fillTextDriver(driver: Driver) {
        binding.txtName.setText(driver.name)
        binding.txtEmail.setText(driver.email)
        binding.txtVehicleBrand.setText(driver.vehicleBrand)
        binding.txtVehiclePlate.setText(driver.vehiclePlate)
        urlImage = driver.image
        if (driver.image.isNotEmpty()) Picasso.get().load(driver.image).into(binding.imgProfile)
        activeProgressBar(false)
    }

    private fun fillTextClient(client: Client) {
        binding.txtName.setText(client.name)
        binding.txtEmail.setText(client.email)
        urlImage = client.image
        if (client.image.isNotEmpty()) Picasso.get().load(client.image).into(binding.imgProfile)
        activeProgressBar(false)
    }

    private fun activeProgressBar(enabled: Boolean) {
        try {
            (requireActivity() as MainActivity).binding.progressBar.visibility =
                if (enabled) View.VISIBLE else View.GONE
            binding.lyAll.isEnabled = !enabled
        } catch (e: Exception) {
            Log.d(this::class.java.simpleName, e.message.toString())
        }
    }

    private fun openGallery() {
        val intentGallery = Intent(Intent.ACTION_GET_CONTENT).apply { type = "image/*" }
        resultGPS.launch(intentGallery)
    }

    /*override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == Constants.REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) handleCameraPhoto()
        if (requestCode == Constants.GALLERY_REQUEST && resultCode == RESULT_OK) {
            try {
                data?.data?.let {
                    imageFile = FileUtil.from(requireContext(), it)
                    binding.imgProfile.setImageBitmap(BitmapFactory.decodeFile(imageFile!!.absolutePath))
                    urlImage = imageFile!!.absolutePath
                }
            } catch (e: Exception) {
                Log.e(this::class.java.simpleName, e.message!!)
            }

        }
    }*/

    @SuppressLint("QueryPermissionsNeeded")
    private fun takePhoto() {
        if (checkCameraPermissionCamera()) {
            val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(requireActivity().packageManager) != null) {
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
                } catch (ex: IOException) {
                    Log.d(
                        UpdateProfileFragment::class.java.simpleName,
                        "Error ocurrido cuando se estaba creando el archivo de la imagen. Detalle: $ex"
                    )
                }
                if (photoFile != null) {
                    val mPhotoURI = FileProvider.getUriForFile(
                        requireActivity().applicationContext,
                        "ec.com.designtechx.taxidriver.fileprovider",
                        photoFile
                    )
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, mPhotoURI)
                    resultCamera.launch(intent)
                }
            }
        }
    }

    private fun checkCameraPermissionCamera(): Boolean {
        var retorno = false
        val permissionCheck = ContextCompat.checkSelfPermission(
            requireActivity(), Manifest.permission.CAMERA
        )
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) ActivityCompat.requestPermissions(
            requireActivity(), arrayOf(
                Manifest.permission.CAMERA
            ), 225
        ) else retorno = true
        return retorno
    }

    private fun createImageFile(): File? {
        val directory: File =
            requireActivity().getExternalFilesDir(Environment.DIRECTORY_PICTURES)!!
        val image =
            File.createTempFile(viewModel.idUser.value, ".jpg", directory)
        urlImage = image.absolutePath
        return image
    }

    private fun handleCameraPhoto() {
        if (urlImage.isNotEmpty()) {
            imageFile = File(urlImage)
            binding.imgProfile.setImageBitmap(BitmapFactory.decodeFile(imageFile!!.absolutePath))
            urlImage = imageFile!!.absolutePath
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}