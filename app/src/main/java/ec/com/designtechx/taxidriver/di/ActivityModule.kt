package ec.com.designtechx.taxidriver.di

import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ActivityRetainedComponent
import ec.com.designtechx.taxidriver.data.irepo.*
import ec.com.designtechx.taxidriver.data.repo.*
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@Module
@InstallIn(ActivityRetainedComponent::class)
abstract class ActivityModule {

    @Binds
    abstract fun bindAuth(repoAuth: RepoAuth): IRepoAuth

    @Binds
    abstract fun bindDriver(repoDriver: RepoDriver): IRepoDriver

    @Binds
    abstract fun bindClient(repoClient: RepoClient): IRepoClient

    @Binds
    abstract fun bindClientBooking(repoClientBooking: RepoClientBooking): IRepoClientBooking

    @Binds
    abstract fun bindDriverFound(repoDriverFound: RepoDriverFound): IRepoDriverFound

    @Binds
    abstract fun bindGeoFire(repoGeoFire: RepoGeoFire): IRepoGeoFire

    @Binds
    abstract fun bindGoogleApi(repoGoogleApi: RepoGoogleApi): IRepoGoogleApi

    @Binds
    abstract fun bindHistoryBooking(repoHistoryBooking: RepoHistoryBooking): IRepoHistoryBooking

    @Binds
    abstract fun bindInfo(repoInfo: RepoInfo): IRepoInfo

    @Binds
    abstract fun bindLocationManager(repoLocationManager: RepoLocationManager): IRepoLocationManager

    @Binds
    abstract fun bindNotification(repoNotification: RepoNotification): IRepoNotification

    @Binds
    abstract fun bindToken(repoToken: RepoToken): IRepoToken
}