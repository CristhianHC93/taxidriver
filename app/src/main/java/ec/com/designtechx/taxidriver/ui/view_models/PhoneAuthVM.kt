package ec.com.designtechx.taxidriver.ui.view_models

import android.app.Activity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.firebase.auth.PhoneAuthProvider.*
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import ec.com.designtechx.taxidriver.data.irepo.IRepoClient
import ec.com.designtechx.taxidriver.data.irepo.IRepoDriver
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject
import kotlin.reflect.KSuspendFunction0

@HiltViewModel
class PhoneAuthVM @Inject constructor(
    private val dataStoreManager: DataStoreManager,
    private val iRepoDriver: IRepoDriver,
    private val iRepoClient: IRepoClient,
    private val iRepoAuth: IRepoAuth
) : ViewModel() {

    private var phone = ""
    private var userId = ""
    private var status = ""

    private val _verificationId = MutableStateFlow("")
    private val _complete = MutableStateFlow(Results.UNANSWERED)

    val verificationId: StateFlow<String> get() = _verificationId
    val complete: StateFlow<Results> get() = _complete

    init {
        viewModelScope.launch {
            dataStoreManager.getAny(DataStoreManager.KEY_STATUS).collect { status = it }
        }
    }

    suspend fun signIn(code: String) {
        iRepoAuth.signIn(verificationId.value, code)
            .collect {
                if (it) checkData()
                else _complete.value = Results.FAIL
            }
    }

    private suspend fun checkData() {
        iRepoAuth.getId().collect { userId = it }
        when (status) {
            Constants.V_DRIVER -> {
                iRepoDriver.getDriver(userId)
                    .collect { driver ->
                        if (!checkIfExist(driver.id, ::createDriver)) {
                            _complete.value =
                                if (checkDataDriver(driver)) Results.USER_WITH_DATA else Results.EMPTY
                        }
                    }
            }
            Constants.V_CLIENT -> {
                iRepoClient.getClient(userId)
                    .collect { client ->
                        if (!checkIfExist(client.id, ::createClient)) {
                            _complete.value =
                                if (checkDataClient(client)) Results.USER_WITH_DATA else Results.EMPTY
                        }
                    }
            }
        }
    }

    private fun checkDataDriver(any: Any): Boolean {
        val driver = any as Driver
        return driver.name.isNotEmpty() && driver.vehicleBrand.isNotEmpty() &&
                driver.vehiclePlate.isNotEmpty()
    }

    private fun checkDataClient(any: Any): Boolean {
        val client = any as Client
        return client.name.isNotEmpty() && client.email.isNotEmpty()
    }

    private suspend fun checkIfExist(id: String, saveData: KSuspendFunction0<Unit>): Boolean {
        val validateIfExist = (id == Constants.ID_NOT_EXIST)
        if (validateIfExist) saveData.invoke()
        return validateIfExist
    }

    private suspend fun createDriver() {
        val driver = Driver(userId, phone)
        saveDriver(driver)
    }

    private suspend fun createClient() {
        val client = Client(userId, phone)
        saveClient(client)
    }

    fun sendCodeVerification(
        _phone: String,
        activity: Activity,
        callback: OnVerificationStateChangedCallbacks
    ) = viewModelScope.launch(Dispatchers.IO) {
        phone = _phone
        iRepoAuth.sendCodeVerification(phone, activity, callback)
    }

    fun setVerificationId(verificationId: String) = viewModelScope.launch(Dispatchers.IO) {
        _verificationId.value = verificationId
    }

    private suspend fun saveDriver(driver: Driver) {
        iRepoDriver.saveDriver(driver)
            .collect { if (it) _complete.value = Results.OK else Results.FAIL }
    }

    private suspend fun saveClient(client: Client) {
        iRepoClient.saveClient(client)
            .collect { if (it) _complete.value = Results.OK else Results.FAIL }
    }

    suspend fun logout() {
        iRepoAuth.logout()
    }
}