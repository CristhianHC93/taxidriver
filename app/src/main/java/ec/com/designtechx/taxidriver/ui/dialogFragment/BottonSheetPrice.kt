package ec.com.designtechx.taxidriver.ui.dialogFragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.BottomSheetPriceBinding
import ec.com.designtechx.taxidriver.utils.ValidateField

class BottonSheetPrice : BottomSheetDialogFragment() {
    private var _binding: BottomSheetPriceBinding? = null
    private val binding get() = _binding!!

    private val validateField by lazy { ValidateField.instance!! }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = BottomSheetPriceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        fillView()
        eventClick()
    }

    private fun fillView() =
        binding.txtPrice.setText(String.format("%.2f", requireArguments().getDouble("price", 0.0)))

    private fun eventClick() {
        binding.btCancel.setOnClickListener { dismiss() }
        binding.btSave.setOnClickListener { openFragment() }
    }

    private fun openFragment() {
        if (validateView()) {
            val bundle = requireArguments().getBundle("args")
            val price = binding.txtPrice.text.toString().toDouble()
            bundle?.putDouble("price", price)
            findNavController().navigate(R.id.requestDriverFragment, bundle)
        }
    }

    private fun validateView(): Boolean {
        return (validateField.validateDouble(binding.txtPrice, binding.tilPrice, requireContext()))
    }

    companion object {
        private var instance: BottonSheetPrice? = null
        fun newInstance(args: Bundle, price: Double): BottonSheetPrice? {
            /*if (instance == null)*/ instance = BottonSheetPrice()
            val args1 = Bundle()
            args1.putBundle("args", args)
            args1.putDouble("price", price)
            instance
            instance!!.arguments = args1
            return instance
        }
    }
}