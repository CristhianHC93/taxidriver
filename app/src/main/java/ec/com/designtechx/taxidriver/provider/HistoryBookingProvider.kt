package ec.com.designtechx.taxidriver.provider

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.taxidriver.data.model.HistoryBooking
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class HistoryBookingProvider {
    private val mDataBase: DatabaseReference = Firebase.database.reference.child("history_booking")

    @ExperimentalCoroutinesApi
    fun create(historyBooking: HistoryBooking): Flow<Boolean> = callbackFlow {
        mDataBase.child(historyBooking.idHistoryBooking).setValue(historyBooking)
            .addOnCompleteListener {
                trySend(it.isSuccessful)
            }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun updateCalificationClient(idHistoryBooking: String, calification: Float): Flow<Boolean> =
        callbackFlow {
            val map = mapOf(Pair("calificationClient", calification))
            mDataBase.child(idHistoryBooking).updateChildren(map)
                .addOnCompleteListener { trySend(it.isSuccessful) }
            awaitClose { }
        }

    @ExperimentalCoroutinesApi
    fun updateCalificationDriver(idHistoryBooking: String, calification: Float): Flow<Boolean> =
        callbackFlow {
            val map = mapOf(Pair("calificationDriver", calification))
            mDataBase.child(idHistoryBooking).updateChildren(map)
                .addOnCompleteListener { trySend(it.isSuccessful) }
            awaitClose { }
        }

    @ExperimentalCoroutinesApi
    fun getHistoryBooking(idHistoryBooking: String): Flow<HistoryBooking> = callbackFlow {
        mDataBase.child(idHistoryBooking)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()){
                        snapshot.getValue(HistoryBooking::class.java)?.let {
                            trySend(it)
                        }
                    }

                }

                override fun onCancelled(error: DatabaseError) {
                }
            })
        awaitClose { }
    }

    companion object {
        var instance: HistoryBookingProvider? = null
            get() {
                if (field == null)
                    field = HistoryBookingProvider()
                return field
            }
            private set
    }
}