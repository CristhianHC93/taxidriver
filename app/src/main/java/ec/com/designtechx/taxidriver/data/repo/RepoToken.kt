package ec.com.designtechx.taxidriver.data.repo

import ec.com.designtechx.taxidriver.data.irepo.IRepoToken
import ec.com.designtechx.taxidriver.provider.TokenProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoToken @Inject constructor(private val tokenProvider: TokenProvider) : IRepoToken {
    override suspend fun createToken(idUser: String) = flow {
        tokenProvider.create(idUser).collect { emit(it) }
    }

    override suspend fun getToken(idUser: String) = flow {
        tokenProvider.getToken(idUser).collect { emit(it) }
    }
}