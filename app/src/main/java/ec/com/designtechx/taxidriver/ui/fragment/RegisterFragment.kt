package ec.com.designtechx.taxidriver.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.databinding.FragmentRegisterBinding
import ec.com.designtechx.taxidriver.provider.AuthProvider
import ec.com.designtechx.taxidriver.provider.ClientProvider
import ec.com.designtechx.taxidriver.provider.DriverProvider
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.GeneralHelper
import ec.com.designtechx.taxidriver.utils.ValidateField
import javax.inject.Inject

class RegisterFragment : Fragment() {
    private var _binding: FragmentRegisterBinding? = null
    private val binding get() = _binding!!

    private val validateField = ValidateField.instance
    private var authProvider = AuthProvider.instance
    private val clientProvider = ClientProvider.instance
    private val driverProvider = DriverProvider.instance

    @Inject
    lateinit var generalHelper: GeneralHelper
    private lateinit var status: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configStatus()
        clickButton()
        listenChangeEditText()
    }

    private fun configStatus() {
        status = generalHelper.getStatus()
        if (status == Constants.V_CLIENT) {
            binding.tilVehicleBrand.visibility = View.GONE
            binding.tilVehiclePlate.visibility = View.GONE
        } else {
            binding.tilVehicleBrand.visibility = View.VISIBLE
            binding.tilVehiclePlate.visibility = View.VISIBLE
        }
    }

    private fun listenChangeEditText() {
        binding.txtPassword.doAfterTextChanged {
            validateField!!.validatePassword(
                binding.txtPassword, binding.tilPassword, requireContext()
            )
        }
        binding.txtEmail.doAfterTextChanged {
            validateField!!.validateEmail(
                binding.txtEmail, binding.tilEmail, requireContext()
            )
        }
        binding.txtName.doAfterTextChanged {
            validateField!!.validateNotEmpty(
                binding.txtName, binding.tilName, requireContext()
            )
        }
    }

    private fun clickButton() {
        binding.btRegister.setOnClickListener {
            register(
                binding.txtEmail.text.toString(), binding.txtPassword.text.toString()
            )
        }
    }

    private fun register(email: String, password: String) {
        if (validateField()) {
            activeProgressBar(true)
            authProvider!!.register(email, password).addOnCompleteListener {
                if (it.isSuccessful) {
                    if (status == Constants.V_CLIENT)
                        saveClient(Client(
                            it.result!!.user!!.uid, it.result!!.user!!.email.toString(),
                                binding.txtName.text.toString())
                        )
                    else
                        saveDriver(
                            Driver(
                                it.result!!.user!!.uid, it.result!!.user!!.email.toString(),
                                binding.txtName.text.toString(), binding.txtVehicleBrand.text.toString(),
                                binding.txtVehiclePlate.text.toString()
                            )
                        )
                } else {
                    showAlert(it.exception?.message)
                    activeProgressBar(false)
                }
            }
        }
    }

    private fun saveClient(client: Client) {
        clientProvider!!.create(client).addOnCompleteListener {
            if (it.isSuccessful)
                completeRegister()
            else {
                activeProgressBar(false)
                showAlert(it.exception?.message)
            }
        }
    }

    private fun saveDriver(driver: Driver) {
        driverProvider!!.create(driver).addOnCompleteListener {
            if (it.isSuccessful)
                completeRegister()
            else {
                activeProgressBar(false)
                showAlert(it.exception?.message)
            }
        }
    }

    private fun completeRegister() {
        authProvider!!.sendEmailConfirmation()!!.addOnCompleteListener {
            if (it.isSuccessful) {
                showAlert(getString(R.string.msj_confirmation_email))
                findNavController().navigate(R.id.loginFragment)
            } else
                showAlert(it.exception?.message)
            activeProgressBar(false)
        }
    }

    private fun showAlert(mensaje: String?) {
        try {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Aviso")
            builder.setMessage(mensaje)
            builder.setPositiveButton("Aceptar", null)
            val dialog = builder.create()
            dialog.show()
        } catch (_: Exception) {
        }
    }

    private fun validateField(): Boolean {
        return validateField!!.validateNotEmpty(binding.txtName, binding.tilName, requireContext())
                && validateField.validateEmail(binding.txtEmail, binding.tilEmail, requireContext())
                && validateField.validatePassword(
            binding.txtPassword, binding.tilPassword, requireContext()
        )
    }

    private fun activeProgressBar(enabled: Boolean) {
        binding.progressBar.visibility = if (enabled) View.VISIBLE else View.GONE
        binding.btRegister.isEnabled = !enabled
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}