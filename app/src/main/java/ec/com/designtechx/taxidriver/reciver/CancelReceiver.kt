package ec.com.designtechx.taxidriver.reciver

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import ec.com.designtechx.taxidriver.provider.AuthProvider
import ec.com.designtechx.taxidriver.provider.ClientBookingProvider
import ec.com.designtechx.taxidriver.provider.DriverFoundProvider
import ec.com.designtechx.taxidriver.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Inject

/**
 * @author Cristhian Holguin carrillo
 */

@ExperimentalCoroutinesApi
class CancelReceiver : BroadcastReceiver() {
    @Inject
    lateinit var driverFoundProvider: DriverFoundProvider
    @Inject
    lateinit var authProvider: AuthProvider
    @Inject
    lateinit var clientBookingProvider: ClientBookingProvider

    override fun onReceive(context: Context, intent: Intent) {
        intent.extras?.let {
            val idClient = it.getString(Constants.ID_CLIENT)
            val searchById = it.getString(Constants.SEARCH_BY_ID)
            if (idClient != null) {
                if (searchById!!.isNotEmpty()) clientBookingProvider.updateStatus(idClient,Constants.CANCEL)
                driverFoundProvider.delete(authProvider.getUserId())
                val manager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                manager.cancel(2)
            }
        }
    }
}