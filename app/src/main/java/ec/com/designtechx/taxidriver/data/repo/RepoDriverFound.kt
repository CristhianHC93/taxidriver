package ec.com.designtechx.taxidriver.data.repo

import ec.com.designtechx.taxidriver.data.irepo.IRepoDriverFound
import ec.com.designtechx.taxidriver.data.model.DriverFound
import ec.com.designtechx.taxidriver.provider.DriverFoundProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoDriverFound @Inject constructor(private val driverFoundProvider: DriverFoundProvider) :
    IRepoDriverFound {

    override suspend fun delete(idDriver: String) = flow {
        driverFoundProvider.delete(idDriver).collect { emit(it) }
    }

    override suspend fun getDriverFoundByIdDriver(idDriver: String) = flow {
        driverFoundProvider.getDriverFoundByIdDriver(idDriver).collect { emit(it) }
    }

    override suspend fun create(driverFound: DriverFound) =
        flow { driverFoundProvider.create(driverFound).collect { emit(it) } }
}