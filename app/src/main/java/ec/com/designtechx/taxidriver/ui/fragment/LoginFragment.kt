package ec.com.designtechx.taxidriver.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.widget.doAfterTextChanged
import androidx.navigation.fragment.findNavController
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.User
import ec.com.designtechx.taxidriver.databinding.FragmentLoginBinding
import ec.com.designtechx.taxidriver.provider.AuthProvider
import ec.com.designtechx.taxidriver.ui.activity.MainActivity
import ec.com.designtechx.taxidriver.utils.GeneralHelper
import ec.com.designtechx.taxidriver.utils.ValidateField
import javax.inject.Inject

class LoginFragment : Fragment() {
    private var _binding: FragmentLoginBinding? = null
    private val binding get() = _binding!!

    private val validateField = ValidateField.instance
    private val authProvider = AuthProvider.instance

    @Inject
    lateinit var generalHelper: GeneralHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickButton()
        listenChangeEditText()
    }


    private fun listenChangeEditText() {
        binding.txtPassword.doAfterTextChanged {
            validateField!!.validatePassword(
                binding.txtPassword,
                binding.tilPassword,
                requireContext()
            )
        }
        binding.txtEmail.doAfterTextChanged {
            validateField!!.validateEmail(binding.txtEmail, binding.tilEmail, requireContext())
        }
    }

    private fun clickButton() {
        binding.btLogin.setOnClickListener { login() }
        binding.btRecoveryPassword.setOnClickListener { recoveryPassword() }
    }

    private fun login() {
        if (validateField()) {
            activeProgressBar(true)
            authProvider?.login(
                binding.txtEmail.text.toString(),
                binding.txtPassword.text.toString()
            )?.addOnCompleteListener {
                if (it.isSuccessful) {
                    if (authProvider.existSession()) {
                        if (authProvider.getUserId().isNotEmpty()) {
                            showMain(
                                User(
                                    it.result?.user?.uid.toString(),
                                    it.result?.user?.email.toString(),
                                    binding.txtPassword.text.toString()
                                )
                            )
                        } else
                            showAlert(getString(R.string.confirme_user))
                    } else
                        showAlert(getString(R.string.confirme_user))
                } else
                    showAlert(it.exception?.message)
                activeProgressBar(false)
            }
        }
    }

    private fun showMain(user: User) {
        generalHelper.saveSession(user)
        startActivity(Intent(requireContext(), MainActivity::class.java))
        requireActivity().finish()
    }

    private fun showAlert(msj: String?) {
        try {
            val builder = AlertDialog.Builder(requireContext())
            builder.setTitle("Atencion")
            builder.setMessage(msj)
            builder.setPositiveButton("Aceptar", null)
            val dialog = builder.create()
            dialog.show()
        } catch (_: Exception) {
        }
    }

    private fun validateField(): Boolean {
        return validateField!!.validateEmail(
            binding.txtEmail,
            binding.tilEmail,
            requireContext()
        ) && validateField.validatePassword(
            binding.txtPassword,
            binding.tilPassword,
            requireContext()
        )
    }

    private fun recoveryPassword() {
        findNavController().navigate(R.id.recoveryPasswordFragment)
    }

    private fun activeProgressBar(enabled: Boolean) {
        try {
            binding.progressBar.visibility = if (enabled) View.VISIBLE else View.GONE
            binding.btRecoveryPassword.isEnabled = !enabled
            binding.btLogin.isEnabled = !enabled
        } catch (e: Exception) {
            Log.e(LoginFragment::class.java.simpleName, e.message.toString())
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}