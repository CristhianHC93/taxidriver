package ec.com.designtechx.taxidriver.data.model

data class FCMBody(
    val registration_ids: MutableList<String>, val priority: String, val data: Map<String, String>,
    val ttl:String = "4500s")