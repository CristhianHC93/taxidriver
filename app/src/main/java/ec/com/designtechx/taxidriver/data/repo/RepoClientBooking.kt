package ec.com.designtechx.taxidriver.data.repo

import ec.com.designtechx.taxidriver.data.irepo.IRepoClientBooking
import ec.com.designtechx.taxidriver.data.model.ClientBooking
import ec.com.designtechx.taxidriver.provider.ClientBookingProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoClientBooking @Inject constructor(private val clientBookingProvider: ClientBookingProvider) :
    IRepoClientBooking {


    override suspend fun create(clientBooking: ClientBooking) = flow {
        clientBookingProvider.create(clientBooking).collect { emit(it) }
    }

    override suspend fun delete(idClientBooking: String) = flow {
        clientBookingProvider.delete(idClientBooking).collect { emit(it) }
    }

    override suspend fun getClientBooking(idClientBooking: String) = flow {
        clientBookingProvider.getClientBooking(idClientBooking).collect { emit(it) }
    }

    override suspend fun getClientBookingListen(idClientBooking: String) = flow {
        clientBookingProvider.getClientBookingListen(idClientBooking).collect { emit(it) }
    }


    override suspend fun updateStatus(idClientBooking: String, status: String) = flow {
        clientBookingProvider.updateStatus(idClientBooking, status).collect { emit(it) }
    }

    override suspend fun updateStatusAndIdDriver(
        idClientBooking: String,
        status: String,
        idDriver: String
    ) = flow {
        clientBookingProvider.updateStatusAndIdDriver(idClientBooking, status, idDriver)
            .collect { emit(it) }
    }

    override suspend fun getStatusListener(idClientBooking: String) = flow {
        clientBookingProvider.getStatusListener(idClientBooking).collect { emit(it) }
    }

    override suspend fun updateIdHistoryBooking(idClientBooking: String) = flow {
        clientBookingProvider.updateIdHistoryBooking(idClientBooking).collect { emit(it) }
    }
}