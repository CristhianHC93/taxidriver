package ec.com.designtechx.taxidriver.ui.fragment

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.activity.addCallback
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.view.MenuHost
import androidx.core.view.MenuProvider
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.android.libraries.places.api.Places
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.api.model.RectangularBounds
import com.google.android.libraries.places.api.net.PlacesClient
import com.google.android.libraries.places.widget.AutocompleteSupportFragment
import com.google.maps.android.SphericalUtil
import com.google.maps.android.collections.MarkerManager
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.awaitMap
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.DriverLocation
import ec.com.designtechx.taxidriver.databinding.FragmentMapBinding
import ec.com.designtechx.taxidriver.ui.activity.AuthActivity
import ec.com.designtechx.taxidriver.ui.activity.MainActivity
import ec.com.designtechx.taxidriver.ui.adapter.PopupAdapter
import ec.com.designtechx.taxidriver.ui.view_models.MapVM
import ec.com.designtechx.taxidriver.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import java.util.*


@AndroidEntryPoint
class MapFragment : Fragment(), MenuProvider {
    private var _binding: FragmentMapBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MapVM by viewModels()

    private val listDriverMarker = mutableListOf<Marker>()
    private val listImagesMarker = HashMap<String, String>()
    private var counter = 0
    private var isStartLocation = false
    private var startLatLng: LatLng? = null
    private var endLatLng: LatLng? = null
    private val listDriverLocation = mutableListOf<DriverLocation>()

    private var mMap: GoogleMap? = null
    private var collection: MarkerManager.Collection? = null
    private var mMarker: Marker? = null
    private lateinit var mPlace: PlacesClient
    private var mIsFirstTime = true

    private lateinit var mAutoCompleteOrigin: AutocompleteSupportFragment
    private lateinit var mAutoCompleteDestine: AutocompleteSupportFragment

    private lateinit var resultGPS: ActivityResultLauncher<IntentSenderRequest>
    private lateinit var resultPermission: ActivityResultLauncher<String>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        resultGPS =
            registerForActivityResult(ActivityResultContracts.StartIntentSenderForResult()) {
                checkOnLocation()
            }
        resultPermission = registerForActivityResult(ActivityResultContracts.RequestPermission()) {
            checkOnLocation()
        }
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapBinding.inflate(inflater, container, false)
        val menuHost: MenuHost = requireActivity()
        menuHost.addMenuProvider(this, viewLifecycleOwner, Lifecycle.State.RESUMED)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configInit()
    }

    private fun configInit() {
        //setHasOptionsMenu(true)
        arguments?.let {
            if (it.getBoolean(Constants.OF_NOTIFICATION, false)) openFragmentBooking(
                it.getString(Constants.ID_CLIENT, ""),
                Constants.ID_CLIENT, R.id.mapDriverBookingFragment
            ) else initFragment(it.getBoolean(Constants.DATA_OF_USER))
        }
        requireActivity().onBackPressedDispatcher.addCallback(this) { requireActivity().finish() }
    }


    private fun initFragment(dataOfUser: Boolean) {
        if (dataOfUser) {
            viewModel.getStatusBooking()
            listenVM()
            generateToken()
            if (viewModel.location.value.latitude == 0.0) configMap()
            varInitialization()
            eventClick()
        } else {
            Toast.makeText(
                requireActivity(),
                getString(R.string.required_data_for_continue),
                Toast.LENGTH_LONG
            ).show()
            findNavController().navigate(R.id.updateProfileFragment)
        }
    }

    private val onCameraIdleListener = GoogleMap.OnCameraIdleListener {
        mMap?.let { viewModel.changeLocateSelect(it.cameraPosition.target, requireContext()) }
    }

    private fun configListenerGPS(location: LatLng) {
        if (startLatLng != null) endLatLng = startLatLng
        startLatLng = location
        if (endLatLng != null) CarMoveAnim.carAnim(mMarker!!, endLatLng!!, startLatLng!!)
        mMap?.let { viewModel.animateCamera(it, location) }
        updateLocation(location)
    }

    private fun configUIClient() {
        binding.btConect.visibility = View.GONE
        binding.btRequestDriver.visibility = View.VISIBLE
        binding.autocompleteFragmentOrigin.visibility = View.VISIBLE
        binding.autocompleteFragmentDestine.visibility = View.VISIBLE
        binding.btChange.visibility = View.VISIBLE
        binding.iconLocation.visibility = View.VISIBLE
    }

    private fun configUIDriver() {
        binding.btConect.visibility = View.VISIBLE
        binding.btRequestDriver.visibility = View.GONE
        binding.autocompleteFragmentOrigin.visibility = View.GONE
        binding.autocompleteFragmentDestine.visibility = View.GONE
        binding.btChange.visibility = View.GONE
        binding.iconLocation.visibility = View.GONE
    }

    private fun varInitialization() {
        mAutoCompleteOrigin =
            childFragmentManager.findFragmentById(binding.autocompleteFragmentOrigin.id) as AutocompleteSupportFragment
        mAutoCompleteDestine =
            childFragmentManager.findFragmentById(binding.autocompleteFragmentDestine.id) as AutocompleteSupportFragment
        if (!Places.isInitialized())
            Places.initialize(requireContext(), resources.getString(R.string.google_map_key))
        mPlace = Places.createClient(requireContext())
        configAutoComplete(mAutoCompleteOrigin, Constants.ORIGIN)
        configAutoComplete(mAutoCompleteDestine, Constants.DESTINE)
    }

    /* Start method's varInitialization*/

    @SuppressLint("MissingPermission")

    private fun configStatusLocationCallback(latLng: LatLng) {
        viewModel.stopLocation()
        when (viewModel.status.value) {
            Constants.V_DRIVER -> {
                mMarker?.remove()
                try {
                    mMarker = mMap?.addMarker {
                        position(viewModel.location.value)
                        title(requireActivity().getString(R.string.tittle_market))
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.uber_car))
                    }
                    updateLocation(latLng)
                    locationGPS()
                } catch (_: Exception) {
                }
            }
            Constants.V_CLIENT -> {
                if (mIsFirstTime) {
                    mMap?.let { it.isMyLocationEnabled = true }
                    limitSearch(mAutoCompleteOrigin)
                    limitSearch(mAutoCompleteDestine)
                    lifecycle.coroutineScope.launch { activeLocation() }
                    mIsFirstTime = false
                }
            }
        }
    }

    private suspend fun activeLocation() = withContext(Dispatchers.IO) {
        viewModel.activeLocation(Constants.ACTIVE_DRIVER, viewModel.location.value)
    }


    private fun locationGPS() = lifecycleScope.launch { viewModel.startLocationGps() }

    private fun updateLocation(location: LatLng) {
        if (viewModel.isConnect.value) {
            lifecycleScope.launchWhenStarted {
                viewModel.saveLocation(Constants.ACTIVE_DRIVER, location)
                viewModel.deleteLocation(Constants.WORK_DRIVER)
            }
        }
    }

    private fun getDriverInfo() {
        counter = 0
        for (marker in listDriverMarker) {
            val id = marker.tag?.toString()
            lifecycleScope.launchWhenStarted { id?.let { viewModel.getDriver(it) } }
            listenDriverInfo(marker)
        }
    }

    private fun requestDriverSelect(marker: Marker) {
        if (viewModel.nameOrigin.value.isNotEmpty() && viewModel.nameDestine.value.isNotEmpty()) {
            val bundle = Bundle().apply {
                putDouble(Constants.ORIGIN_LAT, viewModel.latLonOrigin.value.latitude)
                putDouble(Constants.ORIGIN_LNG, viewModel.latLonOrigin.value.longitude)
                putDouble(Constants.DESTINE_LAT, viewModel.latLonDestine.value.latitude)
                putDouble(Constants.DESTINE_LNG, viewModel.latLonDestine.value.longitude)
                putString(Constants.ORIGIN_NAME, viewModel.nameOrigin.value)
                putString(Constants.DESTINE_NAME, viewModel.nameDestine.value)
                putString(Constants.ID_DRIVER, marker.tag?.toString())
                putDouble(Constants.DRIVER_LAT, marker.position.latitude)
                putDouble(Constants.DRIVER_LNG, marker.position.longitude)
            }
            findNavController().navigate(R.id.detailRequestFragment, bundle)
        }
    }

    private fun configAutoComplete(
        autoComplete: AutocompleteSupportFragment, typeAutoComplete: Int
    ) {
        autoComplete.setPlaceFields(
            listOf(
                Place.Field.ID, Place.Field.LAT_LNG, Place.Field.NAME
            )
        )
        listenAutoComplete(autoComplete, typeAutoComplete)
    }

    private fun listenAutoComplete(
        mAutoComplete: AutocompleteSupportFragment, typeAutoComplete: Int
    ) {
        mAutoComplete.setHint(if (typeAutoComplete == Constants.ORIGIN) "Lugar de recogida" else "Lugar Destino")
        mAutoComplete.setOnPlaceSelectedListener(viewModel.listenPlaceSelected(requireContext()))
    }

    private fun limitSearch(mAutoComplete: AutocompleteSupportFragment) {
        val northSide = SphericalUtil.computeOffset(viewModel.location.value, 5000.0, 0.0)
        val southSide = SphericalUtil.computeOffset(viewModel.location.value, 5000.0, 180.0)
        mAutoComplete.setCountry("ECU")
        mAutoComplete.setLocationBias(RectangularBounds.newInstance(southSide, northSide))
    }

    /*End method's varInitialization*/


    private fun configMap() {
        lifecycleScope.launchWhenStarted {
            val mapFragment =
                childFragmentManager.findFragmentById(binding.map.id) as? SupportMapFragment
            mMap = mapFragment?.awaitMap()
            mMap?.mapType = GoogleMap.MAP_TYPE_NORMAL
            mMap?.uiSettings?.isMyLocationButtonEnabled
            mMap?.uiSettings?.isZoomControlsEnabled = true
            collection = MarkerManager(mMap).newCollection()
            configStatusOnMapReady()
        }
    }

    /*Start method's configMap*/

    private fun configStatusOnMapReady() {
        when (viewModel.status.value) {
//            Constants.V_DRIVER -> {
//                mMap.isMyLocationEnabled = false
//            }
            Constants.V_CLIENT -> {
                mMap?.setOnCameraIdleListener(onCameraIdleListener)
                checkOnLocation()
            }
        }
    }

    /*End method's configMap*/
    private fun eventClick() {
        binding.btConect.setOnClickListener { viewModel.connect() }
        binding.btRequestDriver.setOnClickListener { requestDriver() }
        binding.btChange.setOnClickListener { viewModel.changeSelectMap() }
    }

    private fun requestDriver() {
        if (viewModel.nameOrigin.value.isNotEmpty() && viewModel.nameDestine.value.isNotEmpty()) {
            val bundle = Bundle().apply {
                putDouble(Constants.ORIGIN_LAT, viewModel.latLonOrigin.value.latitude)
                putDouble(Constants.ORIGIN_LNG, viewModel.latLonOrigin.value.longitude)
                putDouble(Constants.DESTINE_LAT, viewModel.latLonDestine.value.latitude)
                putDouble(Constants.DESTINE_LNG, viewModel.latLonDestine.value.longitude)
                putString(Constants.ORIGIN_NAME, viewModel.nameOrigin.value)
                putString(Constants.DESTINE_NAME, viewModel.nameDestine.value)
            }
            findNavController().navigate(R.id.detailRequestFragment, bundle)
        } else
            Toast.makeText(
                requireActivity(), R.string.complete_origin_and_destine,
                Toast.LENGTH_SHORT
            ).show()
    }

    private suspend fun disconnect() {
        viewModel.stopLocation()
        binding.btConect.text = requireActivity().getString(R.string.connect)
        isStartLocation = false
        viewModel.deleteLocation(Constants.ACTIVE_DRIVER)
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.INVISIBLE
    }


    private fun checkOnLocation() {
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.VISIBLE
        binding.btConect.text = getText(R.string.disconnect)
        lifecycleScope.launchWhenStarted {
            viewModel.checkOnLocation(this@MapFragment, resultGPS, resultPermission)
        }
    }

    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menu.findItem(R.id.exit).isVisible = true
        menu.findItem(R.id.profile).isVisible = true
        menu.findItem(R.id.history).isVisible = true
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        return when (menuItem.itemId) {
            R.id.exit -> {
                logout()
                true
            }
            R.id.profile -> {
                findNavController().navigate(R.id.updateProfileFragment)
                true
            }
            R.id.history -> {
                findNavController().navigate(R.id.historyBookingFragment)
                true
            }
            else -> {true}
        }
    }

    private fun logout() {
        lifecycleScope.launch { viewModel.deleteLocation(Constants.ACTIVE_DRIVER) }
        lifecycleScope.launchWhenStarted { viewModel.logOut() }
        startActivity(Intent(requireContext(), AuthActivity::class.java))
        requireActivity().finish()
    }

    private fun generateToken() = lifecycleScope.launchWhenStarted { viewModel.createToken() }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onDestroy() {
        (requireActivity() as MainActivity).binding.progressBar.visibility = View.INVISIBLE
        lifecycleScope.launch { viewModel.deleteLocation(Constants.ACTIVE_DRIVER) }
        isStartLocation = false
        super.onDestroy()
    }


    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.idClient.collect {
                if (it.isNotEmpty()) findNavController().navigate(
                    R.id.calificationFragment,
                    Bundle().apply {
                        putString(Constants.ID_CLIENT, it)
                        putDouble(Constants.PRICE, 0.0)
                    })
            }
        }
        //configStatus
        lifecycleScope.launchWhenStarted {
            viewModel.status.collect {
                when (it) {
                    Constants.V_DRIVER -> configUIDriver()
                    Constants.V_CLIENT -> configUIClient()
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.location.collect {
                if (it.latitude != 0.0) {
                    (requireActivity() as MainActivity).binding.progressBar.visibility = View.GONE
                    if (!isStartLocation) {
                        isStartLocation = true
                        mMap?.let { it1 -> viewModel.animateCamera(it1, it) }
                        configStatusLocationCallback(it)
                    }
                }
            }
        }
        //isDriverWorking
        lifecycleScope.launchWhenStarted { viewModel.isWorking.collect { if (it == Results.OK) disconnect() } }
        //DriverActive
        lifecycleScope.launchWhenStarted {
            viewModel.active.collect { geo ->
                when (geo["NAME"]) {
                    "onKeyEntered" -> {
                        var flag = true
                        val key = geo["KEY"] as String
                        val lat = geo["LAT"] as Double
                        val lng = geo["LNG"] as Double
                        if (listDriverMarker.size > 0) {
                            listDriverMarker.forEach {
                                if (it.tag != null) {
                                    if (it.tag!! == key)
                                        flag = false
                                    //return
                                }
                            }
                        }
                        if (flag) {
                            val latLng = LatLng(lat, lng)
                            val marker = mMap?.addMarker(
                                MarkerOptions().position(latLng)
                                    .icon(BitmapDescriptorFactory.fromResource(R.drawable.uber_car))
                            )
                            marker?.tag = key
                            marker?.let { listDriverMarker.add(it) }

                            val driverLocation = DriverLocation(key)
                            listDriverLocation.add(driverLocation)

                            getDriverInfo()
                            collection?.setOnInfoWindowClickListener { requestDriverSelect(it) }
                        }
                    }
                    "onKeyExited" -> removeExited(geo["KEY"] as String)
                    "onKeyMoved" -> keyMoved(
                        geo["KEY"] as String,
                        geo["LAT"] as Double,
                        geo["LNG"] as Double
                    )
                    "onGeoQueryReady" -> {
                    }
                    "onGeoQueryError" -> {
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.locationGPS.collect {
                when (it["NAME"]) {
                    "onLocationChanged" -> configListenerGPS(
                        LatLng(
                            it["LAT"] as Double,
                            it["LNG"] as Double
                        )
                    )
                    "onProviderDisabled" -> viewModel.requestActiveLocation(
                        this@MapFragment,
                        resultGPS
                    )
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            if (viewModel.status.value == Constants.V_DRIVER) {
                viewModel.isConnect.collect {
                    when (it) {
                        true -> checkOnLocation()
                        false -> disconnect()
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.idClientPref.collect {
                if (it.isNotEmpty()) openFragmentBooking(
                    it, Constants.ID_CLIENT, R.id.mapDriverBookingFragment
                )
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.idDriverPref.collect {
                if (it.isNotEmpty()) openFragmentBooking(
                    it, Constants.ID_DRIVER, R.id.mapClientBookingFragment
                )
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.selectInMap.collect { selectedMap ->
                if (viewModel.status.value == Constants.V_CLIENT) {
                    when (selectedMap) {
                        Constants.ORIGIN -> {
                            mMap?.let { viewModel.animateCamera(it, viewModel.latLonOrigin.value) }
                            binding.btChange.setImageResource(R.drawable.outline_trip_origin_24)
                            binding.iconLocation.visibility = View.VISIBLE
                        }
                        Constants.DESTINE -> {
                            mMap?.let { viewModel.animateCamera(it, viewModel.latLonDestine.value) }
                            binding.btChange.setImageResource(R.drawable.outline_place_24)
                            binding.iconLocation.visibility = View.VISIBLE
                        }
                        Constants.CAR -> {
                            binding.btChange.setImageResource(R.drawable.outline_directions_car_24)
                            binding.iconLocation.visibility = View.GONE
                        }
                    }
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.locationPlaceSelected.collect {
                if (it.latitude != 0.0 && it.longitude != 0.0)
                    mMap?.let { it1 -> viewModel.animateCamera(it1, it) }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.nameOrigin.collect {
                if (it.isNotEmpty()) mAutoCompleteOrigin.setText(
                    it
                )
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.nameDestine.collect {
                if (it.isNotEmpty()) mAutoCompleteDestine.setText(
                    it
                )
            }
        }
    }

    private fun keyMoved(key: String, lat: Double, lng: Double) {
        listDriverMarker.stream().forEach {
            val start = LatLng(lat, lng)
            var end: LatLng? = null

            var local: DriverLocation? = null
            listDriverLocation.stream().filter { x -> x.id == it.tag.toString() }
                .forEach { x -> local = x }
            if (it.tag != null) {
                if (it.tag == key) {
                    if (local != null) {
                        end = local?.latLng
                    }
                    local?.latLng = start
                    if (end != null) {
                        CarMoveAnim.carAnim(it, end, start)
                    }
                }
            }
        }
    }

    private fun removeExited(key: String) {
        if (listDriverMarker.size > 0) {
            listDriverMarker.stream().filter { it.tag == key }.forEach { it.remove() }
            listDriverMarker.removeIf { it.tag == key }
            if (listDriverLocation.size > 0) listDriverLocation.removeIf { it.id == key }
        }
    }

    private fun openFragmentBooking(id: String, key: String, fragment: Int) =
        findNavController().navigate(fragment, Bundle().apply { putString(key, id) })

    private fun listenDriverInfo(marker: Marker) = lifecycleScope.launchWhenStarted {
        viewModel.profileDriver.collect {
            counter += 1
            if (it.id.isNotEmpty()) {
                marker.title = it.name
                listImagesMarker[marker.tag.toString()] = it.image
            }
            if (counter == listDriverMarker.size) {
                collection?.setInfoWindowAdapter(
                    PopupAdapter(
                        requireActivity(),
                        layoutInflater,
                        listImagesMarker
                    )
                )
            }
        }
    }
}