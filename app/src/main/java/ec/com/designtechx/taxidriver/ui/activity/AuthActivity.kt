package ec.com.designtechx.taxidriver.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.core.content.ContextCompat
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.ActivityAuthBinding
import ec.com.designtechx.taxidriver.utils.GeneralHelper
import javax.inject.Inject

@AndroidEntryPoint
class AuthActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAuthBinding

    @Inject
    lateinit var generalHelper: GeneralHelper
    lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBiding()
        setupToolBarAndNavigation()
    }

    private fun configBiding() {
        binding = ActivityAuthBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun setupToolBarAndNavigation() {
        setSupportActionBar(binding.toolBar)
        val navHostFragment =
            supportFragmentManager.findFragmentById(binding.navAuthFragment.id) as NavHostFragment
        navController = navHostFragment.navController
        NavigationUI.setupActionBarWithNavController(this, navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        //Helper.setColorItemMenu(menu.findItem(R.id.salir).icon,ContextCompat.getColor(applicationContext, R.color.colorPrimary))
        val listDrawable = listOf(menu.findItem(R.id.exit).icon)
        generalHelper.setColorItemMenu(
            listDrawable, ContextCompat.getColor(applicationContext, R.color.colorPrimary)
        )
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.exit -> finish()
        }
        return super.onOptionsItemSelected(item)
    }
}