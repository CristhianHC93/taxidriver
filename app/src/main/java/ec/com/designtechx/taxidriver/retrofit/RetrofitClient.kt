package ec.com.designtechx.taxidriver.retrofit

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory

class RetrofitClient {

    fun getClient(url: String): Retrofit {
        return Retrofit.Builder().apply {
            baseUrl(url)
            addConverterFactory(ScalarsConverterFactory.create())
        }.build()
    }

    fun getClientObject(url: String): Retrofit? {
        return Retrofit.Builder().apply {
            baseUrl(url)
            addConverterFactory(GsonConverterFactory.create())
        }.build()
    }

    companion object {
        var instance: RetrofitClient? = null
            get() {
                if (field == null)
                    field = RetrofitClient()
                return field
            }
            private set
    }
}