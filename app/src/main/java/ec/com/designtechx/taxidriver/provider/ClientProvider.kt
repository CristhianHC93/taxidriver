package ec.com.designtechx.taxidriver.provider

import com.google.android.gms.tasks.Task
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.utils.Constants

class ClientProvider {
    private val mDataBase: DatabaseReference =
        Firebase.database.reference.child("user").child(Constants.V_CLIENT)

    fun create(client: Client): Task<Void> {
        val map = mapOf(Pair("phone", client.phone))
        return mDataBase.child(client.id).setValue(map)
    }

    fun getClient(idClient: String): DatabaseReference {
        return mDataBase.child(idClient)
    }

    fun update(client: Client): Task<Void?> {
        val map = mapOf(
            Pair("name", client.name),
            Pair("email", client.email),
            Pair("image", client.image)
        )
        return mDataBase.child(client.id).updateChildren(map)
    }

    companion object {
        var instance: ClientProvider? = null
            get() {
                if (field == null)
                    field = ClientProvider()
                return field
            }
            private set
    }
}