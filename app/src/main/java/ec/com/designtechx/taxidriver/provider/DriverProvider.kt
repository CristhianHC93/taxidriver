package ec.com.designtechx.taxidriver.provider

import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class DriverProvider {

    private val mDataBase: DatabaseReference = Firebase.database.reference.child("user").child(Constants.V_DRIVER)

    fun create(driver: Driver): Task<Void> {
        val map = mapOf(Pair("phone", driver.phone))
        return mDataBase.child(driver.id).setValue(map)
    }

    fun getDriverAdapter(idDriver: String): DatabaseReference {
        return mDataBase.child(idDriver)
    }

    @ExperimentalCoroutinesApi
    fun getDriver(idDriver: String):Flow<Driver> = callbackFlow {
        mDataBase.child(idDriver).addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val driver = snapshot.getValue(Driver::class.java)
                    driver?.apply { id = idDriver }?.let { trySend(it) }
                } else trySend(Driver(Constants.ID_NOT_EXIST))
            }

            override fun onCancelled(error: DatabaseError) {
                Log.e(TAG,"${error.message} -> ${error.details}")
                trySend(Driver(Constants.ID_NOT_EXIST))
            }
        })
        awaitClose {  }
    }



    fun update(driver: Driver): Task<Void> {
        val map = mapOf(
            Pair("name", driver.name),
            Pair("image", driver.image),
            Pair("vehicleBrand", driver.vehicleBrand),
            Pair("vehiclePlate", driver.vehiclePlate),
            Pair("email", driver.email)
        )
        return mDataBase.child(driver.id).updateChildren(map)
    }

    companion object {
        val TAG: String = DriverProvider::class.java.simpleName
        var instance: DriverProvider? = null
            get() {
                if (field == null)
                    field = DriverProvider()
                return field
            }
            private set
    }

}