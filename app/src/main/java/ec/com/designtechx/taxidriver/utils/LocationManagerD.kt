package ec.com.designtechx.taxidriver.utils

import android.Manifest
import android.annotation.SuppressLint
import android.content.Context
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.widget.Toast
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.core.app.ActivityCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.util.concurrent.TimeUnit


class LocationManagerD(private val context: Context) {
    private val fusedLocation: FusedLocationProviderClient =
        LocationServices.getFusedLocationProviderClient(context)

    private val locationManager: LocationManager =
        context.getSystemService(Context.LOCATION_SERVICE) as LocationManager

    private val mLocationRequest = LocationRequest.Builder(Priority.PRIORITY_HIGH_ACCURACY, TimeUnit.MILLISECONDS.toMillis(5))
        .setWaitForAccurateLocation(false)
        .setMinUpdateIntervalMillis(500)
        .setMaxUpdateDelayMillis(1000)
        .build()


    /*LocationRequest.create().apply {
    interval = TimeUnit.MILLISECONDS.toMillis(5)
    fastestInterval = TimeUnit.MILLISECONDS.toMillis(1)
    maxWaitTime = TimeUnit.MILLISECONDS.toMillis(15)
    priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    smallestDisplacement = 5F*/
    //}

    //Check Permission
    fun checkLocationPermission(): Boolean {
        return (ActivityCompat.checkSelfPermission(
            context, Manifest.permission.ACCESS_FINE_LOCATION
        ) == PackageManager.PERMISSION_GRANTED)
    }

    fun isLocationEnabled(): Boolean {
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) // && locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER)
    }

    private var locationCallbackD: LocationCallback? = null

    @SuppressLint("MissingPermission")
    @ExperimentalCoroutinesApi
    fun startLocation(): Flow<LatLng> = callbackFlow {
        val locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                super.onLocationResult(locationResult)
                trySend(
                    LatLng(
                        locationResult.lastLocation!!.latitude,
                        locationResult.lastLocation!!.longitude
                    )
                )
            }
        }
        fusedLocation.requestLocationUpdates(mLocationRequest, locationCallback, Looper.getMainLooper())
        locationCallbackD = locationCallback
        awaitClose { fusedLocation.removeLocationUpdates(locationCallback) }
    }

    fun requestPermission(resultPermission: ActivityResultLauncher<String>) =
        resultPermission.launch(
            Manifest.permission.ACCESS_FINE_LOCATION
        )

    fun requestGPSSettings(
        fragment: Fragment,
        resultGPS: ActivityResultLauncher<IntentSenderRequest>
    ) {
        val builder = LocationSettingsRequest.Builder().addLocationRequest(mLocationRequest)
        builder.setAlwaysShow(true)
        val result =
            LocationServices.getSettingsClient(fragment.requireContext()).checkLocationSettings(
                builder.build()
            )
        result.addOnCompleteListener { task ->
            try {
                task.getResult(ApiException::class.java)
            } catch (ex: ApiException) {
                when (ex.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> try {
                        resultGPS.launch(
                            IntentSenderRequest.Builder(ex.status.resolution?.intentSender!!)
                                .build()
                        )
                    } catch (e: IntentSender.SendIntentException) {
                        Toast.makeText(context, "Error: " + e.message, Toast.LENGTH_SHORT)
                            .show()
                    }
                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }
            }
        }
    }

    fun stopLocation(): Task<Void>? =
        locationCallbackD?.let { return fusedLocation.removeLocationUpdates(it) }


    /*GPS*/
    @ExperimentalCoroutinesApi
    fun startLocationGPS(): Flow<Map<String, Any>> = callbackFlow {
        val listener = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                trySend(
                    mapOf(
                        Pair("NAME", "onLocationChanged"),
                        Pair("LAT", location.latitude),
                        Pair("LNG", location.longitude)
                    )
                )
            }

            @Deprecated("Deprecated in Java",
                ReplaceWith("trySend(mapOf(Pair(\"NAME\", \"onProviderDisabled\"), Pair(\"LAT\", 0.0), Pair(\"LNG\", 0.0)))")
            )
            override fun onStatusChanged(s: String?, i: Int, bundle: Bundle?) {
                trySend(
                    mapOf(
                        Pair("NAME", "onProviderDisabled"),
                        Pair("LAT", 0.0),
                        Pair("LNG", 0.0)
                    )
                )
            }

            override fun onProviderEnabled(provider: String) {}
            override fun onProviderDisabled(provider: String) {
                trySend(
                    mapOf(
                        Pair("NAME", "onProviderDisabled"),
                        Pair("LAT", 0.0),
                        Pair("LNG", 0.0)
                    )
                )
            }
        }
        if (ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                context,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            locationManager.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                2000,
                10F,
                listener
            )
        }
        awaitClose { locationManager.removeUpdates(listener) }
    }
}