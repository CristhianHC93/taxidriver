package ec.com.designtechx.taxidriver.data.repo

import ec.com.designtechx.taxidriver.data.irepo.IRepoInfo
import ec.com.designtechx.taxidriver.provider.InfoProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoInfo @Inject constructor(private val infoProvider: InfoProvider) : IRepoInfo {

    override suspend fun getInfo() = flow { infoProvider.getInfo().collect { emit(it) } }
}