package ec.com.designtechx.taxidriver.provider

import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.taxidriver.data.model.Info
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class InfoProvider {
    private val dataBase: DatabaseReference = Firebase.database.reference.child("Info")

    @ExperimentalCoroutinesApi
    suspend fun getInfo(): Flow<Info> = callbackFlow {
        dataBase.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists())
                    snapshot.getValue(Info::class.java)?.let { trySend(it) }
                else
                    trySend(Info())
            }

            override fun onCancelled(error: DatabaseError) {}
        })
        awaitClose { }
    }
}