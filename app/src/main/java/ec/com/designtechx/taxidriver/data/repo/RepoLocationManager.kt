package ec.com.designtechx.taxidriver.data.repo

import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import ec.com.designtechx.taxidriver.data.irepo.IRepoLocationManager
import ec.com.designtechx.taxidriver.utils.LocationManagerD
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoLocationManager @Inject constructor(
    private val locationManagerD: LocationManagerD
) : IRepoLocationManager {

    override suspend fun checkOnLocation(): Flow<Results> = flow {
        emit(if (locationManagerD.checkLocationPermission()) if (locationManagerD.isLocationEnabled()) Results.OK else Results.NOT_ACTIVE else Results.WITHOUT_PERMISSION)
    }

    override suspend fun requestGPSSettings(fragment: Fragment,resultGPS: ActivityResultLauncher<IntentSenderRequest>) =
        locationManagerD.requestGPSSettings(fragment,resultGPS)

    override suspend fun requestPermission(resultPermission: ActivityResultLauncher<String>) =
        locationManagerD.requestPermission(resultPermission)

    override suspend fun startLocationGPS(): Flow<Map<String, Any>> =
        flow { locationManagerD.startLocationGPS().collect { emit(it) } }

    override suspend fun startLocation(): Flow<LatLng> =
        flow { locationManagerD.startLocation().collect { emit(it) } }

    override suspend fun stopLocation(): Flow<Task<Void>?> =
        flow { emit(locationManagerD.stopLocation()) }
}