package ec.com.designtechx.taxidriver.utils

class Constants {
    companion object {
        const val LANGUAJE = "es"


        const val ID_NOT_EXIST = "-1"

        const val KEY_USER = "KEY_USER"
        const val KEY_STATUS = "KEY_STATUS"
        const val KEY_STATUS_BOOKING = "KEY_STATUS_BOOKING"
        const val KEY_ID_CLIENT = "KEY_ID_CLIENT"
        const val KEY_ID_DRIVER = "KEY_ID_DRIVER"

        /*For KEY_STATUS*/
        const val V_DRIVER = "driver"
        const val V_CLIENT = "client"

        /*For KEY_STATUS_BOOKING*/


        const val V_RIDE = "ride"
        const val V_START = "start"

        /*Para GPS*/
        private const val PACKAGE_NAME = "ec.com.designtechx.appchat"

        internal const val ACTION_FOREGROUND_ONLY_LOCATION_BROADCAST =
            "$PACKAGE_NAME.action.FOREGROUND_ONLY_LOCATION_BROADCAST"

        internal const val EXTRA_LOCATION = "$PACKAGE_NAME.extra.LOCATION"

        private const val EXTRA_CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION =
            "$PACKAGE_NAME.extra.CANCEL_LOCATION_TRACKING_FROM_NOTIFICATION"


        const val LOCATION_ACTIVE_REQUEST_CODE      = 502


        /*Constants for Budle and Intent paraments*/
        const val ORIGIN_LAT_LNG  = "ORIGIN_LAT_LNG"
        const val DESTINE_LAT_LNG  = "DESTINE_LAT_LNG"
        const val ORIGIN_LAT = "ORIGIN_LAT"
        const val ORIGIN_LNG = "ORIGIN_LNG"
        const val DESTINE_LAT = "DESTINE_LAT"
        const val DESTINE_LNG = "DESTINE_LNG"
        const val STATUS  = "STATUS"
        const val PRICE  = "PRICE"
        const val DISTANCE  = "DISTANCE"
        const val ID_CLIENT  = "id_client"
        const val ID_DRIVER  = "id_driver"
        const val DRIVER_LAT  = "driver_lat"
        const val DRIVER_LNG  = "id_driver_lng"
        const val ID_HISTORY_BOOKING  = "idHistoryBooking"
        const val SEARCH_BY_ID  = "searchById"

        const val OF_NOTIFICATION  = "of_notification"

        const val M_ORIGIN  = "ORIGIN"
        const val DESTINATION  = "DESTINATION"
        const val MIN  = "MIN"
        const val CODE: String = "CODE"
        const val PHONE: String = "PHONE"
        const val DATA_OF_USER: String = "DATA_OF_USER"


        const val ACTIVE_DRIVER  = "active_driver"
        const val WORK_DRIVER  = "work_driver"


        const val ORIGIN_NAME  = "ORIGIN_LAT_LNG"
        const val DESTINE_NAME  = "DESTINE_LAT_LNG"

        /*STATES*/
        const val CREATE  = "create"
        const val ACCEPT  = "accept"
        const val CANCEL  = "cancel"
        const val START  = "start"
        const val END  = "end"

        /*Constans for autocomplete type*/
        const val ORIGIN  = 1
        const val DESTINE  = 2
        const val CAR  = 3


        const val GALLERY_REQUEST = 1
        const val REQUEST_IMAGE_CAPTURE = 666
    }
}