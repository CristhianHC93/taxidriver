package ec.com.designtechx.taxidriver.reciver

import android.app.NotificationManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.provider.AuthProvider
import ec.com.designtechx.taxidriver.provider.ClientBookingProvider
import ec.com.designtechx.taxidriver.provider.GeoFireProvider
import ec.com.designtechx.taxidriver.ui.activity.MainActivity
import ec.com.designtechx.taxidriver.utils.Constants
import kotlinx.coroutines.*
import javax.inject.Inject

/**
 * @author Cristhian Holguin carrillo
 */

@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
@AndroidEntryPoint
class AcceptReceiver : BroadcastReceiver() {

    @Inject
    lateinit var clientBookingProvider: ClientBookingProvider

    @Inject
    lateinit var geoFireProvider: GeoFireProvider

    @Inject
    lateinit var authProvider: AuthProvider

    private fun BroadcastReceiver.goAsync(
        coroutineScope: CoroutineScope,
        dispatcher: CoroutineDispatcher,
        block: suspend () -> Unit
    ) {
        val pendingResult = goAsync()
        coroutineScope.launch(dispatcher) {
            block()
            pendingResult.finish()
        }
    }

    override fun onReceive(context: Context, intent: Intent) {

        goAsync(GlobalScope, Dispatchers.Default) {
            intent.extras?.let {
                val idClient = it.getString(Constants.ID_CLIENT)
                val searchById = it.getString(Constants.SEARCH_BY_ID)
                idClient?.let {
                    geoFireProvider.changeReferent(Constants.ACTIVE_DRIVER)
                        .apply { removeLocation(authProvider.getUserId()) }
                    val manager =
                        context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    manager.cancel(2)
                    if (searchById!!.isEmpty()) checkIfClientBookingAccept(idClient, context)
                    else {
                        clientBookingProvider.updateStatus(idClient, Constants.ACCEPT).collect { reponseUpdate ->
                            if (reponseUpdate) openMainActivity(idClient, context)
                        }

                    }
                }
            }
        }
        /*intent.extras?.let {
            val idClient = it.getString(Constants.ID_CLIENT)
            val searchById = it.getString(Constants.SEARCH_BY_ID)
            idClient?.let {
                geoFireProvider.changeReferent(Constants.ACTIVE_DRIVER)
                    .apply { removeLocation(authProvider.getUserId()) }
                val manager =
                    context.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                manager.cancel(2)
                if (searchById!!.isEmpty()) checkIfClientBookingAccept(idClient, context)
                else {
                    clientBookingProvider.updateStatus(idClient, Constants.ACCEPT)
                    openMainActivity(idClient, context)
                }
            }
        }*/
    }

    private suspend fun checkIfClientBookingAccept(idClient: String, context: Context) {
        //Reescribir con flow
        clientBookingProvider.getClientBooking(idClient).collect {
            if (it.idDriver == Constants.ID_NOT_EXIST && it.idClient == Constants.ID_NOT_EXIST) {
                if (it.status == Constants.CREATE && it.idDriver.isEmpty()) {
                    clientBookingProvider.updateStatusAndIdDriver(
                        idClient,
                        Constants.ACCEPT,
                        authProvider.getUserId()
                    )
                    openMainActivity(idClient, context)
                } else {
                    Toast.makeText(
                        context,
                        context.getString(R.string.busy_trip), Toast.LENGTH_SHORT
                    ).show()
                    openMainActivity("", context)
                }
            }else openMainActivity("", context)
        }



           /* .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        if (snapshot.hasChild("idDriver") && snapshot.hasChild("status")) {
                            val status = snapshot.child("status").value.toString()
                            val idDriver = snapshot.child("idDriver").value.toString()
                            if (status == Constants.CREATE && idDriver.isEmpty()) {
                                clientBookingProvider.updateStatusAndIdDriver(
                                    idClient,
                                    Constants.ACCEPT,
                                    authProvider.getIdUser()
                                )
                                openMainActivity(idClient, context)
                            } else {
                                Toast.makeText(
                                    context,
                                    context.getString(R.string.busy_trip), Toast.LENGTH_SHORT
                                ).show()
                                openMainActivity("", context)
                            }
                        } else
                            openMainActivity("", context)
                    } else
                        openMainActivity("", context)
                }
                override fun onCancelled(error: DatabaseError) {}
            })*/
    }

    private fun openMainActivity(idClient: String, context: Context) {
        val intentNot = Intent(context, MainActivity::class.java).apply {
            if (idClient.isNotEmpty()) {
                putExtra(Constants.OF_NOTIFICATION, true)
                putExtra(Constants.ID_CLIENT, idClient)
            }
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            action = Intent.ACTION_RUN
        }
        context.startActivity(intentNot)
    }
}