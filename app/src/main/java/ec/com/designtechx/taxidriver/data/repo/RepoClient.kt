package ec.com.designtechx.taxidriver.data.repo

import android.content.Context
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import ec.com.designtechx.taxidriver.data.irepo.IRepoClient
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.provider.ClientProvider
import ec.com.designtechx.taxidriver.provider.ImageProvider
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.io.File
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoClient @Inject constructor(
    private val clientProvider: ClientProvider,
    private val imageProvider: ImageProvider
) : IRepoClient {
    override suspend fun getClient(id: String): Flow<Client> = callbackFlow {
        clientProvider.getClient(id)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val client = snapshot.getValue(Client::class.java)
                        client?.let { trySend(it).isSuccess }
                    } else trySend(Client(Constants.ID_NOT_EXIST))
                }

                override fun onCancelled(error: DatabaseError) {
                    //trySend(error)
                }
            })
        awaitClose { }
    }

    override suspend fun saveClient(client: Client): Flow<Boolean> = callbackFlow {
        clientProvider.create(client).addOnCompleteListener { trySend(it.isSuccessful) }
        awaitClose { }
    }

    override suspend fun updateClient(client: Client): Flow<Results> = callbackFlow {
        clientProvider.update(client)
            .addOnCompleteListener { if (it.isSuccessful) trySend(Results.OK) else trySend(Results.FAIL) }
        awaitClose { }
    }

    override suspend fun saveImageClient(
        client: Client,
        imageFile: File,
        context: Context,
        id: String
    ): Flow<String> =
        callbackFlow {
            imageProvider.selectRef("client_image")
                .saveImage(context, imageFile, id)
                .addOnCompleteListener {
                    if (it.isSuccessful) {
                        imageProvider.getStorage().downloadUrl.addOnCompleteListener { uri ->
                            if (uri.isSuccessful) trySend(uri.result.toString()) else trySend("")
                        }
                    } else {
                        trySend("")
                    }
                }
            awaitClose { }
        }
}