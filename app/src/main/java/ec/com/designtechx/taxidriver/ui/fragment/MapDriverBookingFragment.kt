package ec.com.designtechx.taxidriver.ui.fragment

import android.content.Intent
import android.graphics.Color
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.coroutineScope
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.addPolyline
import com.google.maps.android.ktx.awaitMap
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.data.model.FCMBody
import ec.com.designtechx.taxidriver.databinding.FragmentMapDriverBookingBinding
import ec.com.designtechx.taxidriver.services.ForegroundService
import ec.com.designtechx.taxidriver.ui.activity.MainActivity
import ec.com.designtechx.taxidriver.ui.view_models.MapDriverBookingVM
import ec.com.designtechx.taxidriver.utils.*
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MapDriverBookingFragment : Fragment() {
    private var _binding: FragmentMapDriverBookingBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MapDriverBookingVM by viewModels()

    private lateinit var map: GoogleMap
    private var currentLatLng: LatLng = LatLng(0.0, 0.0)

    private var idClient = ""

    private var isFirstTime = true
    private var mIsClosetClient = false

    private var mMarker: Marker? = null
    private var polyLine: Polyline? = null

    private var statusNotification = ""

    //private var mIsFinishBooking = false

    private var mStartLatLng: LatLng? = null
    private var mEndLatLng: LatLng? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapDriverBookingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configInit()
        configMap()
        listenVM()
        eventClick()
    }

    private fun configInit() {
        requireArguments().let { idClient = it.getString(Constants.ID_CLIENT).toString() }
        lifecycleScope.launchWhenStarted { viewModel.getStatusBooking(idClient) }
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            Toast.makeText(requireActivity(), getString(R.string.not_can_return), Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun configMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(binding.map.id) as? SupportMapFragment
        lifecycle.coroutineScope.launchWhenStarted {
            map = mapFragment?.awaitMap()!!
            map.mapType = GoogleMap.MAP_TYPE_NORMAL
            map.uiSettings.isZoomControlsEnabled = true
        }
    }

    private fun eventClick() {
        binding.btStartBooking.setOnClickListener { startBooking() }
        binding.btEndBooking.setOnClickListener { endBooking() }
    }

    private fun startBooking() {
        if (mIsClosetClient) {
            lifecycleScope.launch { viewModel.saveStatusBookingPref(Constants.V_START) }
            configStatusStart()
            lifecycleScope.launchWhenStarted {
                viewModel.updateStatusClientBooking(
                    idClient,
                    Constants.START
                )
            }
        } else {
            Toast.makeText(requireContext(), R.string.required_closet_client, Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun configStatusStart() {
        binding.btStartBooking.visibility = View.GONE
        binding.btEndBooking.visibility = View.VISIBLE
        map.clear()
        map.addMarker {
            position(
                LatLng(
                    viewModel.clientBooking.value.destinationLat,
                    viewModel.clientBooking.value.destinationLng
                )
            )
            title(getString(R.string.destine))
            icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_blue))
        }
        mMarker = map.addMarker {
            position(currentLatLng)
            title(getString(R.string.tittle_market))
            icon(BitmapDescriptorFactory.fromResource(R.drawable.uber_car))
        }
    }

    private fun configStatusRide() {
        map.addMarker {
            position(
                LatLng(
                    viewModel.clientBooking.value.originLat,
                    viewModel.clientBooking.value.originLng
                )
            )
            title(getString(R.string.pick_up_here))
            icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_red))
        }
    }

    private fun getDistanceBetween(clientLatLng: LatLng, driverLatLng: LatLng): Float {
        val clientLocation = Location("")
        val driverLocation = Location("")
        clientLocation.latitude = clientLatLng.latitude
        clientLocation.longitude = clientLatLng.longitude
        driverLocation.latitude = driverLatLng.latitude
        driverLocation.longitude = driverLatLng.longitude
        return clientLocation.distanceTo(driverLocation)
    }

    private fun endBooking() =
        lifecycleScope.launchWhenStarted { viewModel.updateIdHistoryBooking(idClient) }

    private fun fillView(client: Client) {
        val valueTextOrigin =
            requireActivity().getString(R.string.pick_up_in) + viewModel.clientBooking.value.origin
        val valueTextDestine =
            requireActivity().getString(R.string.destine) + ": " + viewModel.clientBooking.value.destination
        binding.txtOriginClientBooking.text = valueTextOrigin
        binding.txtDestinationClientBooking.text = valueTextDestine
        binding.txtEmailClientBooking.text = client.email
        binding.txtClientBooking.text = client.name
        if (client.image.isNotEmpty()) Picasso.get().load(client.image)
            .into(binding.imageClientBooking)
        when (viewModel.statusBooking.value) {
            Constants.V_START -> configStatusStart()
            Constants.V_RIDE -> configStatusRide()
            Constants.END -> openCalificationFragment()
        }
//        if (viewModel.statusBooking.value == Constants.V_START)  else
        //lifecycleScope.launchWhenStarted { viewModel.checkOnLocation(this@MapDriverBookingFragment) }
    }

    private fun updateLocation() {
        lifecycleScope.launch{ viewModel.saveLocation(Constants.WORK_DRIVER, currentLatLng) }
        val distance = getDistanceBetween(
            LatLng(
                viewModel.clientBooking.value.originLat,
                viewModel.clientBooking.value.originLng
            ), currentLatLng
        )
        if (!mIsClosetClient) {
            if (distance <= 200.0) {
                mIsClosetClient = true
                Toast.makeText(
                    requireContext(), R.string.closetToClient, Toast.LENGTH_SHORT
                ).show()
            }
        }
    }

    private fun openCalificationFragment() {
        isFirstTime = true
        val bundle = Bundle().apply {
            putString(Constants.ID_CLIENT, idClient)
            putDouble(Constants.PRICE, 0.0)
        }
        findNavController().navigate(R.id.calificationFragment, bundle)
    }

    private fun disconnect() {
        lifecycleScope.launchWhenStarted { viewModel.deleteLocation(Constants.WORK_DRIVER) }
        stopService()
    }

    private fun getToken() =
        lifecycleScope.launchWhenStarted { viewModel.getToken(idClient) }

/*End method's configMap*/

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.statusBooking.collect {
                if (it.isNotEmpty() && it != Constants.ID_NOT_EXIST) {
                    if (it != Constants.END) viewModel.getClientBooking(idClient)
                    else openCalificationFragment()
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.locationOK.collect {
                if (it == Results.OK) {
                    (requireActivity() as MainActivity).binding.progressBar.visibility =
                        View.GONE
                    map.moveCamera(
                        CameraUpdateFactory.newCameraPosition(
                            CameraPosition.Builder().target(
                                LatLng(
                                    currentLatLng.latitude,
                                    currentLatLng.longitude
                                )
                            ).zoom(16F).build()
                        )
                    )
                    mMarker?.remove()
                    mMarker = map.addMarker {
                        position(currentLatLng)
                        title(getString(R.string.tittle_market))
                        icon(BitmapDescriptorFactory.fromResource(R.drawable.uber_car))
                    }
                    isFirstTime = false
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.locationGPS.collect {
                when (it["NAME"]) {
                    "onLocationChanged" -> listenLocationChanged(
                        LatLng(
                            it["LAT"] as Double,
                            it["LNG"] as Double
                        )
                    )
                    //"onProviderDisabled" -> viewModel.requestActiveLocation(this@MapDriverBookingFragment)
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            lifecycleScope.launchWhenStarted {
                viewModel.updateStatusClientBooking.collect {
                    if (it == Results.OK) {
                        statusNotification = getString(R.string.started)
                        getToken()
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.direction.collect {
                if (it.isNotEmpty()) drawRoute(it)
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.token.collect {
                if (it.isNotEmpty()) sendNotification(it)
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.client.collect {
                if (it.id.isNotEmpty() && it.id != Constants.ID_NOT_EXIST) fillView(it)
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.updateIdHistoryBooking.collect {
                if (it == Results.OK) {
                    binding.btEndBooking.visibility = View.GONE
                    statusNotification = getString(R.string.finished)
                    getToken()
                    disconnect()
                    lifecycleScope.launchWhenStarted {
                        viewModel.updateStatusClientBooking(idClient, Constants.END)
                    }
                }
            }
        }
    }

    private fun listenLocationChanged(latLng: LatLng) {
        currentLatLng = latLng
        if (mStartLatLng != null) {
            mEndLatLng = mStartLatLng
        }
        mStartLatLng = LatLng(currentLatLng.latitude, currentLatLng.longitude)
        if (mEndLatLng != null) CarMoveAnim.carAnim(
            mMarker!!,
            mEndLatLng!!,
            mStartLatLng!!
        )
        map.moveCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.Builder().target(
                    LatLng(
                        currentLatLng.latitude,
                        currentLatLng.longitude
                    )
                ).zoom(17F).build()
            )
        )
        updateLocation()
    }

    private fun sendNotification(token: String) {
        val map = mapOf(
            Pair("title", "ESTADO DE TU VIAJE"),
            Pair("body", "Tu estado es $statusNotification")
        )
        val list = mutableListOf(token)
        val body = FCMBody(list, "high", map)
        lifecycleScope.launch { viewModel.sendNotification(body) }
    }

    private fun drawRoute(response: String) {
        try {
            val jsonObject = JSONObject(response)
            val jsonArray: JSONArray = jsonObject.getJSONArray("routes")
            val route = jsonArray.getJSONObject(0)
            val polyline = route.getJSONObject("overview_polyline")
            val points = polyline.getString("points")
            val mPolylineList = DecodePoints.decodePoly(points)
            polyLine?.remove()
            polyLine = map.addPolyline {
                color(Color.DKGRAY)
                width(8f)
                startCap(SquareCap())
                jointType(JointType.ROUND)
                addAll(mPolylineList)
            }
        } catch (_: Exception) {
        }
    }

    private fun startService() {
        val serviceIntent = Intent(requireActivity(), ForegroundService::class.java)
        ContextCompat.startForegroundService(requireActivity(), serviceIntent)
    }

    private fun stopService() {
        lifecycleScope.launchWhenStarted { viewModel.checkOnLocation() }
        val serviceIntent = Intent(requireActivity(), ForegroundService::class.java)
        requireActivity().stopService(serviceIntent)
    }


    override fun onStop() {
        super.onStop()
        if (!isFirstTime) startService()
    }

    override fun onResume() {
        super.onResume()
        stopService()
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}