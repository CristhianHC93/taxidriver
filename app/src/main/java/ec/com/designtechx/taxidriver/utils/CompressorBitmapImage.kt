package ec.com.designtechx.taxidriver.utils

import android.content.Context
import android.graphics.Bitmap
import id.zelory.compressor.Compressor
import id.zelory.compressor.constraint.format
import id.zelory.compressor.constraint.quality
import id.zelory.compressor.constraint.resolution
import id.zelory.compressor.constraint.size
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.io.File
import java.io.IOException
import java.nio.file.Files
import java.nio.file.Paths

class CompressorBitmapImage {
    /*
     * Metodo que permite comprimir imagenes y transformarlas a bitmap
     */
    companion object {
        suspend fun getImage(ctx: Context, path: String, width: Int=1280, height: Int=80): ByteArray? {
            var fileThumbPath = File(path)
            try {
                fileThumbPath = Compressor.compress(ctx, fileThumbPath) {
                    resolution(width,height)
                    quality(80)
                    format(Bitmap.CompressFormat.PNG)
                    size(2_097_152) // 2 MB
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return withContext(Dispatchers.IO) {
                Files.readAllBytes(Paths.get(fileThumbPath.absolutePath))
            }
        }
    }
}