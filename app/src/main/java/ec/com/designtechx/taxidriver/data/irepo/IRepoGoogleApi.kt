package ec.com.designtechx.taxidriver.data.irepo

import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.flow.Flow

interface IRepoGoogleApi {
    suspend fun getDirections(originLatLng: LatLng, destineLatLng: LatLng): Flow<String>
}