package ec.com.designtechx.taxidriver.data.irepo

import ec.com.designtechx.taxidriver.data.model.DriverFound
import kotlinx.coroutines.flow.Flow

interface IRepoDriverFound {
    suspend fun delete(idDriver: String): Flow<Boolean>
    suspend fun getDriverFoundByIdDriver(idDriver: String): Flow<MutableList<DriverFound>>
    suspend fun create(driverFound: DriverFound): Flow<Boolean>
}