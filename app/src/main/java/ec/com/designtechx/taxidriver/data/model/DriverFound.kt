package ec.com.designtechx.taxidriver.data.model

data class DriverFound(val idDriver:String="", val idClientBooking:String="")
