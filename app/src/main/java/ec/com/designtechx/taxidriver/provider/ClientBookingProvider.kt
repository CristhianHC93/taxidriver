package ec.com.designtechx.taxidriver.provider

import android.util.Log
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.taxidriver.data.model.ClientBooking
import ec.com.designtechx.taxidriver.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class ClientBookingProvider {
    private var mDataBase: DatabaseReference = Firebase.database.reference.child("client_booking")

    @ExperimentalCoroutinesApi
    fun create(clientBooking: ClientBooking) = callbackFlow {
        mDataBase.child(clientBooking.idClient).setValue(clientBooking).addOnCompleteListener {
            trySend(it.isSuccessful)
        }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun updateStatus(idClientBooking: String, status: String) = callbackFlow {
        val map = mapOf(Pair("status", status))
        mDataBase.child(idClientBooking).updateChildren(map)
            .addOnCompleteListener { trySend(it.isSuccessful) }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun updateStatusAndIdDriver(
        idClientBooking: String, status: String, idDriver: String
    ) = callbackFlow {
        val map = mapOf(Pair("status", status), Pair("idDriver", idDriver))
        mDataBase.child(idClientBooking).updateChildren(map)
            .addOnCompleteListener { trySend(it.isSuccessful) }
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun getStatusListener(idClientBooking: String): Flow<String> = callbackFlow {
        val callback = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) trySend(snapshot.value.toString())
            }

            override fun onCancelled(error: DatabaseError) {
            }
        }
        val listen = mDataBase.child(idClientBooking).child("status")
        listen.addValueEventListener(callback)
        awaitClose { listen.removeEventListener(callback) }
    }

    @ExperimentalCoroutinesApi
    fun getClientBooking(idClientBooking: String): Flow<ClientBooking> = callbackFlow {
        mDataBase.child(idClientBooking)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    if (snapshot.exists()) {
                        val clientBooking = snapshot.getValue(ClientBooking::class.java)
                        clientBooking?.let { trySend(it) }
                    } else trySend(ClientBooking(Constants.ID_NOT_EXIST, Constants.ID_NOT_EXIST))
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.w(TAG, error.message)
                    trySend(ClientBooking(Constants.ID_NOT_EXIST, Constants.ID_NOT_EXIST))
                }
            })
        awaitClose { }
    }

    @ExperimentalCoroutinesApi
    fun getClientBookingListen(idClientBooking: String): Flow<ClientBooking> = callbackFlow {
        val callback = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val clientBooking = snapshot.getValue(ClientBooking::class.java)
                    clientBooking?.let { trySend(it) }
                } else trySend(ClientBooking(Constants.ID_NOT_EXIST, Constants.ID_NOT_EXIST))
            }

            override fun onCancelled(error: DatabaseError) {
                Log.w(TAG, error.message)
                trySend(ClientBooking(Constants.ID_NOT_EXIST, Constants.ID_NOT_EXIST))
            }
        }
        val listen = mDataBase.child(idClientBooking)
        listen.addValueEventListener(callback)
        awaitClose { listen.removeEventListener(callback) }
    }

    @ExperimentalCoroutinesApi
    fun updateIdHistoryBooking(idClientBooking: String) = callbackFlow {
        val idPush = mDataBase.push().key
        val map = mapOf(Pair("idHistoryBooking", idPush))
        mDataBase.child(idClientBooking).updateChildren(map)
            .addOnCompleteListener { trySend(it.isSuccessful) }
        awaitClose { }
    }

    /*fun updatePrice(idClientBooking: String, price: Double): Task<Void> {
        val map = mapOf(Pair("price", price))
        return mDataBase.child(idClientBooking).updateChildren(map)
    }*/

    /*fun getClientBookingByDriver(idDriver: String?): Query {
        return mDataBase.orderByChild("idDriver").equalTo(idDriver)
    }*/

    @ExperimentalCoroutinesApi
    fun delete(idClientBooking: String) = callbackFlow {
        mDataBase.child(idClientBooking).removeValue()
            .addOnCompleteListener { trySend(it.isSuccessful) }
        awaitClose { }
    }

    companion object {
        private val TAG = ClientBookingProvider::class.java.simpleName
    }
}