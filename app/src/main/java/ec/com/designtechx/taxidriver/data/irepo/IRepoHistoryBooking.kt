package ec.com.designtechx.taxidriver.data.irepo

import ec.com.designtechx.taxidriver.data.model.HistoryBooking
import kotlinx.coroutines.flow.Flow

interface IRepoHistoryBooking {
    suspend fun create(historyBooking: HistoryBooking): Flow<Boolean>
    suspend fun updateQualifyClient(idHistoryBooking: String, calification: Float): Flow<Boolean>
    suspend fun updateQualifyDriver(idHistoryBooking: String, calification: Float): Flow<Boolean>
    suspend fun getHistoryBooking(idHistoryBooking: String): Flow<HistoryBooking>
}