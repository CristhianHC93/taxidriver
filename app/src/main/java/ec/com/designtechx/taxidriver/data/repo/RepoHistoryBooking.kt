package ec.com.designtechx.taxidriver.data.repo

import ec.com.designtechx.taxidriver.data.irepo.IRepoHistoryBooking
import ec.com.designtechx.taxidriver.data.model.HistoryBooking
import ec.com.designtechx.taxidriver.provider.HistoryBookingProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoHistoryBooking @Inject constructor(private val historyBookingProvider: HistoryBookingProvider) :
    IRepoHistoryBooking {

    override suspend fun create(historyBooking: HistoryBooking): Flow<Boolean> = flow {
        historyBookingProvider.create(historyBooking).collect { emit(it) }
    }

    override suspend fun updateQualifyClient(
        idHistoryBooking: String,
        calification: Float
    ): Flow<Boolean> = flow {
        historyBookingProvider.updateCalificationClient(idHistoryBooking, calification)
            .collect { emit(it) }
    }

    override suspend fun updateQualifyDriver(
        idHistoryBooking: String,
        calification: Float
    ): Flow<Boolean> = flow {
        historyBookingProvider.updateCalificationDriver(idHistoryBooking, calification)
            .collect { emit(it) }
    }

    override suspend fun getHistoryBooking(idHistoryBooking: String): Flow<HistoryBooking> = flow {
        historyBookingProvider.getHistoryBooking(idHistoryBooking).collect { emit(it) }
    }
}