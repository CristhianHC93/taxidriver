package ec.com.designtechx.taxidriver.ui.activity

import android.app.KeyguardManager
import android.app.NotificationManager
import android.content.Context
import android.content.Intent
import android.media.MediaPlayer
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.WindowManager
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.ActivityNotificationBookingBinding
import ec.com.designtechx.taxidriver.ui.view_models.NotificationBookingVM
import ec.com.designtechx.taxidriver.utils.Constants

/**
 * @author @Cristhian Holguin
 */

@AndroidEntryPoint
class NotificationBookingActivity : AppCompatActivity() {
    private lateinit var binding: ActivityNotificationBookingBinding

//    private val viewModel by lazy {
//        ViewModelProvider(
//            this,
//            NotificationBookingVMFactory()
//        ).get(NotificationBookingVM::class.java)
//    }

    private val viewModel: NotificationBookingVM by viewModels()

    private lateinit var idClient: String
    private lateinit var searchById: String

    private var counter = 0
    private var mHandler = Handler(Looper.getMainLooper())

    private var mMediaPlayer: MediaPlayer? = null
    private var runnable = Runnable {
        counter -= 1
        binding.txtCounter.text = counter.toString()
        if (counter > 0) initTimer() else cancelBooking()
    }

    private fun initTimer() {
        mHandler.postDelayed(runnable, 1000)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBinding()
        listenVM()
        counter = getString(R.string.time_default).toInt()
        intent?.extras?.let {
            idClient = it.getString(Constants.ID_CLIENT).toString()
            searchById = it.getString(Constants.SEARCH_BY_ID).toString()
            fillView(it)
            eventClick()
        }

        mMediaPlayer = MediaPlayer.create(this, R.raw.ringtone)
        mMediaPlayer?.isLooping = true
        configNotificationOff()
        initTimer()
        checkIfCancelOrBusyBooking()
    }

    private fun configBinding() {
        binding = ActivityNotificationBookingBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun fillView(bundle: Bundle) {
        binding.txtOrigin.text = bundle.getString(Constants.M_ORIGIN)
        binding.txtDestination.text = bundle.getString(Constants.DESTINATION)
    }

    private fun eventClick() {
        binding.btAcceptBooking.setOnClickListener { acceptBooking() }
        binding.btCancelBooking.setOnClickListener { cancelBooking() }
    }

    /**
     * Method for configure notification when app closed
     */
    private fun configNotificationOff() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            setShowWhenLocked(true)
            setTurnScreenOn(true)
            val keyguardManager = getSystemService(Context.KEYGUARD_SERVICE) as KeyguardManager
            keyguardManager.requestDismissKeyguard(this, null)
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
                window.addFlags(
                    WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON or
                            android.R.attr.showWhenLocked or
                            android.R.attr.turnScreenOn
                )
            } else {
                window.addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON)
            }
        }
    }

    /**
     * listen if booking is canceled or Busy.
     * If not exist info of client Booking then booking is canceled and open Main Activity,
     * else check if booking is busy for other driver
     * If is busy then cancel notification and open Main Activity
     */
    private fun checkIfCancelOrBusyBooking() =
        lifecycleScope.launchWhenStarted { viewModel.getClientBookingListen(idClient) }

    private fun acceptBooking() {
        mHandler.removeCallbacks(runnable)
        val manager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(2)
        checkIfClientBookingAccept()
    }

    private fun cancelBooking() {
        mHandler.removeCallbacks(runnable)

        if (searchById.isNotEmpty()) lifecycleScope.launchWhenStarted {
            viewModel.updateStatusClientBooking(
                idClient,
                Constants.CANCEL
            )
        }

        lifecycleScope.launchWhenStarted { viewModel.deleteDriverFound() }
        val manager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.cancel(2)
        startActivity(
            Intent(
                this,
                MainActivity::class.java
            ).apply { putExtra(Constants.DATA_OF_USER, true) })
        finish()
    }

    /**
     * Method for know is booking is canceled or Busy .
     * If exist info of client Booking and exist node idDriver and status then check if status is Created and idDriver is empty
     * then delete register in node active_driver, update status and idDriver in clientBooking, later open Main Activity
     * with args idClient for open MapDriverBooking else open Main Acitivy without idCLient for open MapFragment
     */
    private fun checkIfClientBookingAccept() =
        lifecycleScope.launchWhenStarted { viewModel.getClientBooking(idClient) }

    private fun openMainActivity(idClient: String, msg: Int?) {
        if (msg != null) Toast.makeText(this, getString(msg), Toast.LENGTH_LONG).show()

        val intentNot = Intent(this, MainActivity::class.java).apply {
            if (idClient.isNotEmpty()) {
                putExtra(Constants.OF_NOTIFICATION, true)
                putExtra(Constants.ID_CLIENT, idClient)
            }
        }
        intentNot.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        intentNot.action = Intent.ACTION_RUN
        startActivity(intentNot)
    }


    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.clientBookingListen.collect {
                if (it.idClient.isNotEmpty()) {
                    if (it.idClient != "-1") {
                        if (it.idDriver.isNotEmpty() && it.status.isNotEmpty())
                            if ((it.status == Constants.ACCEPT || it.status == Constants.CANCEL) && it.idDriver != viewModel.idUser.value) {
                                val manager =
                                    getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                                manager.cancel(2)
                                openMainActivity("", R.string.client_not_available)
                            }
                    } else openMainActivity("", R.string.cancel_request)
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.clientBooking.collect {
                if (it.idClient.isNotEmpty()) {
                    if (it.idClient != "-1" && it.status.isNotEmpty()) {
                        if (it.status == Constants.CREATE && it.idDriver.isEmpty()) {
                            lifecycleScope.launchWhenStarted {
                                viewModel.removeLocation(
                                    Constants.ACTIVE_DRIVER,
                                    viewModel.idUser.value
                                )
                            }
                            lifecycleScope.launchWhenStarted {
                                viewModel.updateStatusAndIdDriverClientBooking(
                                    idClient,
                                    Constants.ACCEPT,
                                    viewModel.idUser.value
                                )
                            }
                            lifecycleScope.launchWhenStarted {
                                viewModel.saveStatusBookingPref(
                                    Constants.CREATE
                                )
                            }
                            openMainActivity(idClient, null)
                        } else openMainActivity("", R.string.busy_trip)
                    } else openMainActivity("", R.string.cancel_request)
                }
            }
        }
    }

    override fun onPause() {
        super.onPause()
        if (mMediaPlayer != null) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.pause()
            }
        }
    }

    override fun onStop() {
        super.onStop()
        if (mMediaPlayer != null) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.release()
            }
        }
    }

    override fun onResume() {
        super.onResume()
        if (mMediaPlayer != null) {
            if (!mMediaPlayer!!.isPlaying) {
                mMediaPlayer!!.start()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        mHandler.removeCallbacks(runnable)
        if (mMediaPlayer != null) {
            if (mMediaPlayer!!.isPlaying) {
                mMediaPlayer?.pause()
            }
        }
    }
}