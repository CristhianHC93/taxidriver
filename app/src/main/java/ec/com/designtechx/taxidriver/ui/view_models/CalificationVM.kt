package ec.com.designtechx.taxidriver.ui.view_models

import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.IRepoClientBooking
import ec.com.designtechx.taxidriver.data.irepo.IRepoHistoryBooking
import ec.com.designtechx.taxidriver.data.model.ClientBooking
import ec.com.designtechx.taxidriver.data.model.HistoryBooking
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class CalificationVM @Inject constructor(
    private val dataStoreManager: DataStoreManager,
    private val iRepoClientBooking: IRepoClientBooking,
    private val iRepoHistoryBooking: IRepoHistoryBooking
) : ViewModel() {

    private val _historyBooking = MutableStateFlow(HistoryBooking())

    //private val _clientBooking = MutableStateFlow(ClientBooking())
    private val _status = MutableStateFlow("")
    private val _idClient = MutableStateFlow("")
    private val _completeSaveOrUpdate = MutableStateFlow(Results.UNANSWERED)

    val historyBooking: StateFlow<HistoryBooking> get() = _historyBooking
    val idClient: StateFlow<String> get() = _idClient
    val status: StateFlow<String> get() = _status
    val completeSaveOrUpdate: StateFlow<Results> get() = _completeSaveOrUpdate

//    fun getIdUser(idClient: String) = viewModelScope.launch(Dispatchers.IO) {
//        iRepoAuth.getId().collect { getStatus(idClient, it) }
//    }

    fun getStatus(idClient: String) = viewModelScope.launch(Dispatchers.IO) {
        _idClient.value = idClient
        dataStoreManager.getAny(DataStoreManager.KEY_STATUS).collect { configStatus(it, idClient) }
    }

    private fun configStatus(status: String, idClient: String) =
        viewModelScope.launch {
            _status.value = status
            getClientBooking(idClient)
        }

    private fun getClientBooking(idClient: String) = viewModelScope.launch {
        iRepoClientBooking.getClientBooking(idClient).collect { convertToHistory(it) }
    }

    private fun convertToHistory(clientBooking: ClientBooking) {
        val historyBooking = HistoryBooking(
            clientBooking.idHistoryBooking,
            clientBooking.idClient,
            clientBooking.idDriver,
            clientBooking.destination,
            clientBooking.origin,
            clientBooking.time,
            clientBooking.km,
            clientBooking.status,
            clientBooking.originLat,
            clientBooking.originLng,
            clientBooking.destinationLat,
            clientBooking.destinationLng
        )
        _historyBooking.value = historyBooking
    }

    suspend fun getHistoryBooking(historyBooking: HistoryBooking, calification: Float) =
        viewModelScope.launch {
            iRepoHistoryBooking.getHistoryBooking(historyBooking.idHistoryBooking).collect {
                when (status.value) {
                    Constants.V_DRIVER -> if (it.idHistoryBooking == Constants.ID_NOT_EXIST) saveHistory(
                        historyBooking.apply {
                            calificationClient = calification
                        }) else updateCalificationClient(it.idHistoryBooking, calification)
                    Constants.V_CLIENT -> if (it.idHistoryBooking == Constants.ID_NOT_EXIST) saveHistory(
                        historyBooking.apply {
                            calificationDriver = calification
                        }) else updateCalificationDriver(it.idHistoryBooking, calification)
                }
            }
        }

    private suspend fun updateCalificationDriver(idHistoryBooking: String, calification: Float) {
        viewModelScope.launch(Dispatchers.IO) { deleteClientBooking() }
        viewModelScope.launch(Dispatchers.IO) {
            iRepoHistoryBooking.updateQualifyDriver(idHistoryBooking, calification).collect {
                _completeSaveOrUpdate.value = if (it) Results.OK else Results.FAIL
            }
        }
    }

    private suspend fun updateCalificationClient(idHistoryBooking: String, calification: Float) {
        viewModelScope.launch(Dispatchers.IO) { deleteClientBooking() }
        viewModelScope.launch(Dispatchers.IO) {
            iRepoHistoryBooking.updateQualifyClient(idHistoryBooking, calification)
                .collect { _completeSaveOrUpdate.value = if (it) Results.OK else Results.FAIL }
        }
    }

    fun clearDataStoreManager(key: Preferences.Key<String>) =
        viewModelScope.launch(Dispatchers.IO) { dataStoreManager.clearAny(key) }

    private fun deleteClientBooking() = viewModelScope.launch {
        iRepoClientBooking.delete(idClient.value).collect { }
    }

    private suspend fun saveHistory(historyBooking: HistoryBooking) = viewModelScope.launch {
        iRepoHistoryBooking.create(historyBooking)
            .collect { _completeSaveOrUpdate.value = if (it) Results.OK else Results.FAIL }
    }

}