package ec.com.designtechx.taxidriver.data.repo

import android.app.Activity
import com.google.firebase.auth.PhoneAuthProvider
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import ec.com.designtechx.taxidriver.provider.AuthProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoAuth @Inject constructor(private val authProvider: AuthProvider) : IRepoAuth {

    override suspend fun sendCodeVerification(
        phone: String,
        activity: Activity,
        callback: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    ) {
        authProvider.sendCodeVerification(phone, activity, callback)
    }

    override fun getId(): Flow<String> =
        flow { if (authProvider.existSession()) emit(authProvider.getUserId()) }

    override suspend fun signIn(verificationId: String, code: String): Flow<Boolean> = flow {
        authProvider.signInPhone(verificationId, code).collect { emit(it) }
    }


    override suspend fun sessionOn() = flow { emit(authProvider.existSession()) }
    override suspend fun logout() = authProvider.logout()
}