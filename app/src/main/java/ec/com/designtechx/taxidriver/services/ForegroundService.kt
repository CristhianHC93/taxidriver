package ec.com.designtechx.taxidriver.services

import android.Manifest
import android.annotation.SuppressLint
import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.*
import androidx.core.app.ActivityCompat
import com.google.android.gms.maps.model.LatLng

import androidx.core.app.NotificationCompat
import ec.com.designtechx.taxidriver.R

import ec.com.designtechx.taxidriver.channel.NotificationHelper
import ec.com.designtechx.taxidriver.provider.AuthProvider
import ec.com.designtechx.taxidriver.provider.GeoFireProvider
import ec.com.designtechx.taxidriver.utils.Constants


class ForegroundService : Service() {
    private val geofireProvider by lazy { GeoFireProvider.instance!! }
    private val authProvider by lazy { AuthProvider.instance!! }

//    private val handler: Handler = Handler(Looper.getMainLooper())
    //private var currentLatLng: LatLng? = null

    //private var locationRequest: LocationRequest? = null
    private val locationManager: LocationManager by lazy { getSystemService(Context.LOCATION_SERVICE) as LocationManager }
    private var locationListenerGPS: LocationListener? = null

    /*private var runnable: Runnable = object : Runnable {
        override fun run() {
            Log.d("SERVICIO", "Temporizador prueba")
            handler.postDelayed(this, 1000)
        }
    }*/

    override fun onCreate() {
        super.onCreate()
        startLocation()
    }

    @SuppressLint("MissingPermission")
    private fun startLocation() {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            return
        }

        locationListenerGPS = object : LocationListener {
            override fun onLocationChanged(location: Location) {
                updateLocation(LatLng(location.latitude, location.longitude))
            }

            @Deprecated("Deprecated in Java")
            override fun onStatusChanged(s: String?, i: Int, bundle: Bundle?) {}
            override fun onProviderEnabled(provider: String) {}
            override fun onProviderDisabled(provider: String) {}
        }

        locationManager.requestLocationUpdates(
            LocationManager.GPS_PROVIDER,
            2000,
            10F,
            locationListenerGPS!!
        )
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        NotificationCompat.Builder(this, CHANNEL_ID_F).apply {
            priority = NotificationManager.IMPORTANCE_HIGH
            setSmallIcon(R.drawable.icon_car)
            setContentTitle("Viaje en curso")
            setContentText("App corriendo en segundo plano")
            setCategory(Notification.CATEGORY_SERVICE)
        }.build()
        startMyForegroundService()
        return START_STICKY
    }


    private fun startMyForegroundService() {
        val channelName = NotificationHelper.CHANNEL_NAME
        val channel = NotificationChannel(
            CHANNEL_ID_F, channelName, NotificationManager.IMPORTANCE_HIGH
        ).apply {
            lightColor = Color.BLUE
            lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        }
        (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).apply {
            createNotificationChannel(channel)
        }
        val notification = NotificationCompat.Builder(this, CHANNEL_ID_F).apply {
            setSmallIcon(R.drawable.icon_car)
            setContentTitle("Viaje en curso")
            setContentText("App corriendo en segundo plano")
            priority = NotificationManager.IMPORTANCE_HIGH
            setCategory(Notification.CATEGORY_SERVICE)
        }.build()
        startForeground(50, notification)
    }

    private fun updateLocation(latLng: LatLng) {
        if (authProvider.existSession()) {
            geofireProvider.changeReferent(Constants.WORK_DRIVER)
                .saveLocation(authProvider.getUserId(), latLng)
        }
    }

    private fun stopLocation() {
        locationListenerGPS?.let { locationManager.removeUpdates(it) }
    }

    override fun onDestroy() {
        stopLocation()
        super.onDestroy()
    }

    companion object {
        private const val CHANNEL_ID_F = NotificationHelper.CHANNEL_ID
    }
}