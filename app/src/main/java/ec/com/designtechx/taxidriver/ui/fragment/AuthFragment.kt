package ec.com.designtechx.taxidriver.ui.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.FragmentAuthBinding
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.GeneralHelper
import javax.inject.Inject

@AndroidEntryPoint
class AuthFragment : Fragment() {
    private var _binding: FragmentAuthBinding? = null
    private val binding get() = _binding!!

    @Inject
    lateinit var generalHelper: GeneralHelper

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentAuthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        clickButton()
    }

    private fun clickButton() {
        binding.btLogin.setOnClickListener { moveToFragment(R.id.loginFragment) }
        binding.btRegister.setOnClickListener { moveToFragment(R.id.registerFragment) }
        binding.btSendCode.setOnClickListener { sendCode() }
    }

    private fun sendCode() {
        binding.btSendCode.isEnabled = false
        val txtPhone = binding.txtPhone.text.toString()
        val phone = if (txtPhone[0].equals('0', true)) txtPhone.drop(1) else txtPhone
        if (generalHelper.isOnlineNet()) {
            if (phone.isNotEmpty()) {
                val code = binding.ccp.selectedCountryCodeWithPlus
                findNavController().navigate(R.id.phoneAuthFragment,
                    Bundle().apply { putString(Constants.PHONE, code.plus(phone)) })
            } else
                Toast.makeText(
                    requireActivity(), getString(R.string.required_phone), Toast.LENGTH_SHORT
                ).show()
        } else
            Toast.makeText(
                requireActivity(), getString(R.string.without_internet), Toast.LENGTH_LONG
            ).show()
        binding.btSendCode.isEnabled = true
    }

    private fun moveToFragment(fragment: Int) {
        findNavController().navigate(fragment)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}