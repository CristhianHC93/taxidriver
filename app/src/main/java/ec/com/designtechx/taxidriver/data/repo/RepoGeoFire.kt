package ec.com.designtechx.taxidriver.data.repo

import android.util.Log
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import ec.com.designtechx.taxidriver.data.irepo.IRepoGeoFire
import ec.com.designtechx.taxidriver.provider.GeoFireProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoGeoFire @Inject constructor(private val geoFireProvider: GeoFireProvider) : IRepoGeoFire {
    override suspend fun getActive(referent: String, latLng: LatLng, radius: Double):
            Flow<Map<String, Any>> = flow {
        geoFireProvider.changeReferent(referent).getActiveDriver(latLng, radius)
            .collect { emit(it) }
    }

    override suspend fun isDriverWorking(idUser: String): Flow<Boolean> = callbackFlow {
        val geo = geoFireProvider.isDriverWorking(idUser)
        val listener = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                trySend(snapshot.exists())
            }

            override fun onCancelled(error: DatabaseError) {
                trySend(false)
            }
        }
        geo.addValueEventListener(listener)
        awaitClose { geo.removeEventListener(listener) }
    }

    override suspend fun saveLocation(
        referent: String,
        idUser: String,
        latLng: LatLng
    ): Flow<Boolean> =
        flow {
            try {
                geoFireProvider.changeReferent(referent).saveLocation(idUser, latLng)
                emit(true)
            } catch (e: Exception) {
                Log.e(TAG, e.message.toString())
                emit(false)
            }
        }

    override suspend fun removeLocation(referent: String, idUser: String): Flow<Boolean> =
        flow {
            try {
                geoFireProvider.changeReferent(referent).removeLocation(idUser)
                emit(true)
            } catch (e: Exception) {
                Log.e(TAG, e.message.toString())
                emit(false)
            }
        }

    override suspend fun getLocation(referent: String, idUser: String): Flow<LatLng> = flow {
        geoFireProvider.changeReferent(referent).getLocation(idUser).collect { emit(it) }
    }

    companion object {
        val TAG = RepoGeoFire::class.java.simpleName.toString()
    }
}