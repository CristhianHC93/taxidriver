package ec.com.designtechx.taxidriver.ui.activity

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.lifecycle.lifecycleScope
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.ActivitySplashScreenBinding
import ec.com.designtechx.taxidriver.ui.view_models.SplashScreenVM
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.GeneralHelper
import ec.com.designtechx.taxidriver.utils.Results
import javax.inject.Inject

@AndroidEntryPoint
@SuppressLint("CustomSplashScreen")
class SplashScreenActivity : AppCompatActivity() {
    private lateinit var binding: ActivitySplashScreenBinding
    private val viewModel: SplashScreenVM by viewModels()

    @Inject
    lateinit var generalHelper: GeneralHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBiding()
        if (generalHelper.isOnlineNet()) listenVM()
        else {
            Toast.makeText(
                applicationContext, getString(R.string.without_internet),
                Toast.LENGTH_LONG
            ).show()
            finish()
        }
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.existSession.collect {
                when (it) {
                    Results.OK -> viewModel.getDataUser()
                    Results.NON_EXISTENT -> openAuthActivity()
                    else -> {}
                }
            }
        }

        lifecycleScope.launchWhenCreated {
            viewModel.dataOfUser.collect {
                when (it) {
                    Results.OK -> openMainActivity(true)
                    Results.EMPTY -> openMainActivity(false)
                    Results.NON_EXISTENT -> openAuthActivity()
                    else -> {
                    }
                }
            }
        }
    }

    private fun configBiding() {
        binding = ActivitySplashScreenBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun openMainActivity(dataOfUser: Boolean) {
        startActivity(Intent(applicationContext, MainActivity::class.java).apply {
            putExtra(Constants.DATA_OF_USER, dataOfUser)
        })
        finish()
    }

    private fun openAuthActivity() {
        startActivity(Intent(applicationContext, AuthActivity::class.java))
        finish()
    }
}