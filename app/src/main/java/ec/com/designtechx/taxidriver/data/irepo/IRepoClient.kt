package ec.com.designtechx.taxidriver.data.irepo

import android.content.Context
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.flow.Flow
import java.io.File

interface IRepoClient {
    suspend fun getClient(id:String): Flow<Client>
    suspend fun saveClient(client: Client): Flow<Boolean>
    suspend fun updateClient(client: Client): Flow<Results>
    suspend fun saveImageClient(client: Client, imageFile: File, context: Context, id:String): Flow<String>
}