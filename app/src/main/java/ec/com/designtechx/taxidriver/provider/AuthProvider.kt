package ec.com.designtechx.taxidriver.provider

import android.app.Activity
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*
import ec.com.designtechx.taxidriver.utils.Constants
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import java.util.concurrent.TimeUnit

class AuthProvider {
    private val mAuth: FirebaseAuth =
        FirebaseAuth.getInstance().apply { setLanguageCode(Constants.LANGUAJE) }

    fun register(user: String, password: String): Task<AuthResult> {
        return mAuth.createUserWithEmailAndPassword(user, password)
    }

    fun login(user: String, password: String): Task<AuthResult> {
        return mAuth.signInWithEmailAndPassword(user, password)
    }

    fun sendEmailConfirmation(): Task<Void>? {
        return mAuth.currentUser?.sendEmailVerification()
    }

    fun resetPassword(email: String): Task<Void> {
        return mAuth.sendPasswordResetEmail(email)
    }

    fun getUserId(): String {
        mAuth.currentUser?.uid?.let { return it }
        return ""
    }

    fun existSession(): Boolean {
        return mAuth.currentUser != null
    }

    fun logout() {
        return mAuth.signOut()
    }

    @ExperimentalCoroutinesApi
    fun signInPhone(verificationId: String, code: String): Flow<Boolean> = callbackFlow {
        if (verificationId.isEmpty() || code.isEmpty()) {
            send(false)
        } else {
            mAuth.signInWithCredential(PhoneAuthProvider.getCredential(verificationId, code))
                .addOnCompleteListener { trySend(it.isSuccessful) }
        }
        awaitClose { }
    }

    fun sendCodeVerification(
        phone: String,
        activity: Activity,
        callback: PhoneAuthProvider.OnVerificationStateChangedCallbacks
    ) {
        val options = PhoneAuthOptions.newBuilder(mAuth)
            .setPhoneNumber(phone) // Phone number to verify
            .setTimeout(60L, TimeUnit.SECONDS) // Timeout and unit
            .setActivity(activity) // Activity (for callback binding)
            .setCallbacks(callback) // OnVerificationStateChangedCallbacks
            .build()
        PhoneAuthProvider.verifyPhoneNumber(options)
    }

    companion object {
        var instance: AuthProvider? = null
            get() {
                if (field == null)
                    field = AuthProvider()
                return field
            }
            private set
    }
}

