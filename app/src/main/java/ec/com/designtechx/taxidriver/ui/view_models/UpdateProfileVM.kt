package ec.com.designtechx.taxidriver.ui.view_models

import android.content.Context
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import ec.com.designtechx.taxidriver.data.irepo.IRepoClient
import ec.com.designtechx.taxidriver.data.irepo.IRepoDriver
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import java.io.File
import javax.inject.Inject

@HiltViewModel
class UpdateProfileVM @Inject constructor(
    private val dataStoreManager: DataStoreManager,
    private val IRepoDriver: IRepoDriver,
    private val IRepoClient: IRepoClient,
    private val IRepoAuth: IRepoAuth
) : ViewModel() {

    private val _idUser = MutableStateFlow("")
    private val _updateProfile = MutableStateFlow(Results.UNANSWERED)
    private val _uriImage = MutableStateFlow("")
    private val _profileClient = MutableStateFlow(Client())
    private val _profileDriver = MutableStateFlow(Driver())
    private val _status = MutableStateFlow("")
    private val _dataOfUser = MutableStateFlow(Results.UNANSWERED)

    val idUser: StateFlow<String> get() = _idUser
    val updateProfile: StateFlow<Results> get() = _updateProfile
    val uriImage: StateFlow<String> get() = _uriImage
    val profileClient: StateFlow<Client> get() = _profileClient
    val profileDriver: StateFlow<Driver> get() = _profileDriver
    val status: StateFlow<String> get() = _status
    val dataOfUser: StateFlow<Results> get() = _dataOfUser

    init {
        viewModelScope.launch { IRepoAuth.getId().collect { _idUser.value = it } }
        viewModelScope.launch {
            dataStoreManager.getAny(DataStoreManager.KEY_STATUS).collect {
                _status.value = it
                when (it) {
                    Constants.V_DRIVER -> checkDataDriver()
                    Constants.V_CLIENT -> checkDataClient()
                }
            }
        }
    }

    private suspend fun checkDataDriver() {
        IRepoDriver.getDriver(idUser.value)
            .collect { driver ->
                if (driver.id != "0") {
                    val checkData =
                        driver.name.isNotEmpty() && driver.vehicleBrand.isNotEmpty() && driver.vehiclePlate.isNotEmpty()
                    _dataOfUser.value = if (checkData) Results.OK else Results.EMPTY
                    _profileDriver.value = driver
                }
            }
    }

    private suspend fun checkDataClient() {
        IRepoClient.getClient(idUser.value).collect { client ->
            if (client.id != "0") {
                val checkData = client.name.isNotEmpty() && client.email.isNotEmpty()
                _dataOfUser.value = if (checkData) Results.OK else Results.EMPTY
                _profileClient.value = client
            }
        }
    }

    suspend fun updateDriver(driver: Driver) {
        IRepoDriver.updateDriver(driver).collect { _updateProfile.value = it }
    }

    suspend fun updateClient(client: Client) {
        IRepoClient.updateClient(client).collect { _updateProfile.value = it }
    }

    suspend fun saveImageDriver(driver: Driver, imageFile: File, context: Context) {
        IRepoDriver.saveImageDriver(driver, imageFile, context, _idUser.value)
            .collect { _uriImage.value = it }
    }

    suspend fun saveImageClient(client: Client, imageFile: File, context: Context) {
        IRepoClient.saveImageClient(client, imageFile, context, _idUser.value)
            .collect { _uriImage.value = it }
    }
}