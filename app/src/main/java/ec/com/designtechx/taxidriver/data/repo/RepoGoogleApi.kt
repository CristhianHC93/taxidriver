package ec.com.designtechx.taxidriver.data.repo

import com.google.android.gms.maps.model.LatLng
import ec.com.designtechx.taxidriver.data.irepo.IRepoGoogleApi
import ec.com.designtechx.taxidriver.provider.GoogleApiProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

@ExperimentalCoroutinesApi
class RepoGoogleApi @Inject constructor(private val googleApiProvider: GoogleApiProvider) :
    IRepoGoogleApi {

    override suspend fun getDirections(originLatLng: LatLng, destineLatLng: LatLng) = flow {
        googleApiProvider.getDirections(originLatLng, destineLatLng).collect { emit(it) }
    }
}