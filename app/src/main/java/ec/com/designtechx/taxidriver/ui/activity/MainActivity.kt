package ec.com.designtechx.taxidriver.ui.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.widget.Toast
import androidx.activity.viewModels
import androidx.core.content.ContextCompat
import androidx.lifecycle.lifecycleScope
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.ActivityMainBinding
import ec.com.designtechx.taxidriver.ui.view_models.MainVM
import ec.com.designtechx.taxidriver.utils.*
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

//    private val viewModel by lazy {
//        ViewModelProvider(this, MainVMFactory(DataStoreManager.getInstance(this))).get(
//            MainVM::class.java
//        )
//    }

    private val viewModel:MainVM by viewModels()

    lateinit var navController: NavController
    @Inject
    lateinit var generalHelper: GeneralHelper

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBinding()
        setupToolBarAndNavigation()
    }

    private fun configBinding() {
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

    private fun setupToolBarAndNavigation() {
        setSupportActionBar(binding.toolBar)
        val navHostFragment =
            supportFragmentManager.findFragmentById(binding.navMainFragment.id) as NavHostFragment
        navController = navHostFragment.navController
        NavigationUI.setupActionBarWithNavController(this, navController)
        intent.extras?.let {
            val bundle = Bundle().apply {
                putString(Constants.ID_CLIENT, it.getString(Constants.ID_CLIENT))
                putBoolean(Constants.OF_NOTIFICATION, it.getBoolean(Constants.OF_NOTIFICATION, false))
                putBoolean(Constants.DATA_OF_USER, it.getBoolean(Constants.DATA_OF_USER,true))
            }
            navController.setGraph(R.navigation.main_navigation, bundle)
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return when (navController.currentDestination?.id) {
            R.id.requestDriverFragment -> showMsgBackPressed()
            R.id.mapClientBookingFragment -> showMsgBackPressed()
            R.id.mapDriverBookingFragment -> showMsgBackPressed()
            R.id.calificationFragment -> showMsgBackPressed()
            R.id.updateProfileFragment -> listenDataUser()
            else -> navController.navigateUp()
        }
    }

    private fun listenDataUser(): Boolean {
        lifecycleScope.launchWhenStarted {
            viewModel.dataOfUser.collect {
                when (it) {
                    Results.OK -> navController.navigateUp()
                    Results.EMPTY -> showAlert()
                    else -> { }
                }
            }
        }
        return true
    }

    private fun showAlert() {
        Toast.makeText(
            this,
            applicationContext.getString(R.string.required_data_for_continue),
            Toast.LENGTH_LONG
        ).show()
    }

    private fun showMsgBackPressed(): Boolean {
        Toast.makeText(
            this,
            applicationContext.getString(R.string.not_can_return),
            Toast.LENGTH_LONG
        ).show()
        return true
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)
        //Helper.setColorItemMenu(menu.findItem(R.id.salir).icon,ContextCompat.getColor(applicationContext, R.color.colorPrimary))
        val listDrawable = listOf(
            menu.findItem(R.id.exit).icon,
            menu.findItem(R.id.profile).icon,
            menu.findItem(R.id.history).icon
        )
        generalHelper.setColorItemMenu(
            listDrawable,
            ContextCompat.getColor(applicationContext, R.color.colorPrimary)
        )
        return true
    }
}