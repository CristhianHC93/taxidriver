package ec.com.designtechx.taxidriver.data.irepo

import ec.com.designtechx.taxidriver.data.model.FCMBody
import ec.com.designtechx.taxidriver.data.model.FCMResponse
import kotlinx.coroutines.flow.Flow

interface IRepoNotification {
    suspend fun sendNotification(body: FCMBody): Flow<FCMResponse>
}