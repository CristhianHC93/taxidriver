package ec.com.designtechx.taxidriver.utils

enum class Results {
    OK ,USER_WITH_DATA, FAIL , UNANSWERED , EMPTY, NON_EXISTENT, ERROR,
    NOT_ACTIVE , WITHOUT_PERMISSION

}