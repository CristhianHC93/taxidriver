package ec.com.designtechx.taxidriver.ui.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.IRepoInfo
import ec.com.designtechx.taxidriver.data.model.Info
import ec.com.designtechx.taxidriver.data.repo.RepoGoogleApi
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class DetailRequestVM @Inject constructor(
    private val repoGoogleApi: RepoGoogleApi,
    private val iRepoInfo: IRepoInfo
) : ViewModel() {

    private val _response = MutableStateFlow("")
    private val _info = MutableStateFlow(Info())

    val response: StateFlow<String> get() = _response
    val info: StateFlow<Info> get() = _info

    init {
        viewModelScope.launch { iRepoInfo.getInfo().collect { _info.value = it } }
    }

    suspend fun getDirections(originLatLng: LatLng, desetinationLatLng: LatLng) {
        repoGoogleApi.getDirections(originLatLng, desetinationLatLng)
            .collect { _response.value = it }
    }

}