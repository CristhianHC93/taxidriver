package ec.com.designtechx.taxidriver.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import ec.com.designtechx.taxidriver.provider.*
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.GeneralHelper
import ec.com.designtechx.taxidriver.utils.LocationManagerD
import javax.inject.Singleton

/**
 * Created by Cristhian Holguin on 26 February 2021
 */

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun generalHelper(@ApplicationContext context: Context) = GeneralHelper(context)

    @Singleton
    @Provides
    fun dataStoreManager(@ApplicationContext context: Context) = DataStoreManager(context)

    @Singleton
    @Provides
    fun providerAuth() = AuthProvider()

    @Singleton
    @Provides
    fun providerСlient() = ClientProvider()

    @Singleton
    @Provides
    fun providerDriver() = DriverProvider()

    @Singleton
    @Provides
    fun providerImage() = ImageProvider()

    @Singleton
    @Provides
    fun providerClientBooking() = ClientBookingProvider()

    @Singleton
    @Provides
    fun providerDriverFound() = DriverFoundProvider()

    @Singleton
    @Provides
    fun providerGeoFire() = GeoFireProvider()

    @Singleton
    @Provides
    fun providerGoogleApi(@ApplicationContext context: Context) = GoogleApiProvider(context)

    @Singleton
    @Provides
    fun providerHistoryBooking() = HistoryBookingProvider()

    @Singleton
    @Provides
    fun providerInfo() = InfoProvider()

    @Singleton
    @Provides
    fun providerNotification() = NotificationProvider()

    @Singleton
    @Provides
    fun providerToken() = TokenProvider()

    @Singleton
    @Provides
    fun providerLocationMaganer(@ApplicationContext context: Context) = LocationManagerD(context)
}