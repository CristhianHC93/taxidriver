package ec.com.designtechx.taxidriver.data.model

data class Client(var id:String="", val email:String="", val password:String="", val name:String="",val phone:String="",var image:String = ""){
    constructor(id: String, phone: String) : this(id, "", "", "", phone)
    constructor(
        id: String,
        email: String,
        name: String
    ) : this(id, email,"", name,"")
}
