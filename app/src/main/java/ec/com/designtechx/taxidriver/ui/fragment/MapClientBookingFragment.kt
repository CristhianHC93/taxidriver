package ec.com.designtechx.taxidriver.ui.fragment

import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.*
import com.google.maps.android.ktx.addMarker
import com.google.maps.android.ktx.addPolyline
import com.google.maps.android.ktx.awaitMap
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.databinding.FragmentMapClientBookingBinding
import ec.com.designtechx.taxidriver.ui.view_models.MapClientBookingVM
import ec.com.designtechx.taxidriver.utils.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.json.JSONArray
import org.json.JSONObject

@AndroidEntryPoint
class MapClientBookingFragment : Fragment() {
    private var _binding: FragmentMapClientBookingBinding? = null
    private val binding get() = _binding!!

    private val viewModel: MapClientBookingVM by viewModels()

    private var marketDriver: Marker? = null
    private var polyLine: Polyline? = null

    private lateinit var mMap: GoogleMap
    private var mIsFirstTime = true
    private var mStartLatLng: LatLng? = null
    private var mEndLatLng: LatLng? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapClientBookingBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        configInit()
        configMap()
        lifecycleScope.launchWhenStarted { viewModel.getClientBooking() }
        listenVM()
//        lifecycleScope.launchWhenStarted { viewModel.getStatusClientBookingListener() }
    }

    private fun configInit() {
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            Toast.makeText(requireActivity(), getString(R.string.not_can_return), Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun configMap() {
        val mapFragment =
            childFragmentManager.findFragmentById(binding.map.id) as? SupportMapFragment
        lifecycleScope.launchWhenCreated {
            mMap = mapFragment?.awaitMap()!!
            mMap.mapType = GoogleMap.MAP_TYPE_NORMAL
            mMap.uiSettings.isZoomControlsEnabled = true
        }
    }


    private fun startBooking() {
        val destinationLatLng = LatLng(
            viewModel.clientBooking.value.destinationLat,
            viewModel.clientBooking.value.destinationLng
        )
        mMap.clear()
        mMap.addMarker {
            position(destinationLatLng)
            title(getString(R.string.destine))
            icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_blue))
        }
        marketDriver?.remove()
        marketDriver = mMap.addMarker {
            position(viewModel.driverLocationListener.value)
            title(getString(R.string.m_you_driver))
            icon(BitmapDescriptorFactory.fromResource(R.drawable.uber_car))
        }
    }

    private fun finishBooking() {
        lifecycleScope.launch { viewModel.saveStatusBookingPref(Constants.END) }
        lifecycleScope.launch { viewModel.clearDataStoreManager(DataStoreManager.KEY_ID_DRIVER) }
        findNavController().navigate(R.id.calificationFragment, Bundle().apply {
            putString(Constants.ID_CLIENT, viewModel.idUser.value)
            putDouble(Constants.PRICE, 0.0)
        })
    }

    private fun listenVM() {
        lifecycleScope.launchWhenCreated {
            viewModel.driver.collect {
                if (it.id.isNotEmpty() && it.id != Constants.ID_NOT_EXIST) fillView(it)
            }
        }

        lifecycleScope.launchWhenCreated {
            viewModel.driverLocationListener.collect {
                if (it.longitude != 0.0 && it.latitude != 0.0) listenDriverLocation(it)
            }
        }

        lifecycleScope.launchWhenCreated {
            viewModel.statusClientBookingListener.collect {
                if (it.isNotEmpty()) listenStatusClientBooking(it)
            }
        }

        lifecycleScope.launchWhenCreated {
            viewModel.direction.collect {
                if (it.isNotEmpty()) drawRoute(it)
            }
        }

        //lifecycleScope.launchWhenCreated { viewModel.statusBooking.collect { if (it.isNotEmpty() && it != Constants.V_START) startBooking() } }

    }

    private fun drawRoute(response: String) {
        try {
            val jsonObject = JSONObject(response)
            val jsonArray: JSONArray = jsonObject.getJSONArray("routes")
            val route = jsonArray.getJSONObject(0)
            val polyline = route.getJSONObject("overview_polyline")
            val points = polyline.getString("points")
            val mPolylineList = DecodePoints.decodePoly(points)
            polyLine?.remove()
            polyLine = mMap.addPolyline {
                color(Color.DKGRAY)
                width(8f)
                startCap(SquareCap())
                jointType(JointType.ROUND)
                addAll(mPolylineList)
            }
        } catch (_: Exception) {
        }
    }

    private fun listenStatusClientBooking(statusClientBooking: String) {
        when (statusClientBooking) {
            Constants.ACCEPT -> binding.txtStatusBooking.text = getString(R.string.accepted)
            Constants.START -> {
                binding.txtStatusBooking.text = getString(R.string.started)
                lifecycleScope.launch(Dispatchers.IO) { viewModel.saveStatusBookingPref(Constants.V_START) }
                startBooking()
            }
            Constants.END -> {
                finishBooking()
                binding.txtStatusBooking.text = getString(R.string.finished)
            }
        }

    }

    private fun listenDriverLocation(latLng: LatLng) {
        if (mIsFirstTime) {
            marketDriver?.remove()
            marketDriver = mMap.addMarker {
                position(latLng)
                title(getString(R.string.m_you_driver))
                icon(BitmapDescriptorFactory.fromResource(R.drawable.uber_car))
            }
            mMap.animateCamera(
                CameraUpdateFactory.newCameraPosition(
                    CameraPosition.Builder()
                        .target(viewModel.driverLocationListener.value)
                        .zoom(15f)
                        .build()
                )
            )
            mIsFirstTime = false
            lifecycleScope.launchWhenStarted { viewModel.getStatusClientBookingListener() }
        }
        if (mStartLatLng != null) mEndLatLng = mStartLatLng
        mStartLatLng = latLng

        if (mEndLatLng != null) {
            CarMoveAnim.carAnim(marketDriver!!, mEndLatLng!!, mStartLatLng!!)
        }
    }

    private fun fillView(driver: Driver) {
        val origin = "Recoger en: ${viewModel.clientBooking.value.origin}"
        val destine = "Destino: ${viewModel.clientBooking.value.destination}"
        binding.txtOriginDriverBooking.text = origin
        binding.txtDestinationDriverBooking.text = destine
        binding.txtDriverBooking.text = driver.name
        binding.txtEmailDriverBooking.text = driver.email

        if (driver.image.isNotEmpty())
            Picasso.get().load(driver.image).into(binding.imageDriverBooking)

        if (viewModel.statusBooking.value == Constants.V_START) startBooking()
        else {
            mMap.addMarker {
                position(
                    LatLng(
                        viewModel.clientBooking.value.originLat,
                        viewModel.clientBooking.value.originLng
                    )
                )
                title(getString(R.string.pick_up_here))
                icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin_red))
            }
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}