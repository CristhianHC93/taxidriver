package ec.com.designtechx.taxidriver.ui.view_models

import androidx.datastore.preferences.core.Preferences
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.*
import ec.com.designtechx.taxidriver.data.model.*
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MapDriverBookingVM @Inject constructor(
    private val dataStoreManager: DataStoreManager,
    private val iRepoLocationManager: IRepoLocationManager,
    private val iRepoGeoFire: IRepoGeoFire,
    private val iRepoAuth: IRepoAuth,
    private val iRepoToken: IRepoToken,
    private val iRepoClient: IRepoClient,
    private val iRepoClientBooking: IRepoClientBooking,
    private val iRepoGoogleApi: IRepoGoogleApi,
    private val iRepoNotification: IRepoNotification
) : ViewModel() {

    private val _client = MutableStateFlow(Client())
    private val _direction = MutableStateFlow("")
    private val _statusBooking = MutableStateFlow("")
    private val _clientBooking = MutableStateFlow(ClientBooking())
    private val _updateStatusClientBooking = MutableStateFlow(Results.UNANSWERED)
    private val _updateIdHistoryBooking = MutableStateFlow(Results.UNANSWERED)
    private val _locationGPS = MutableStateFlow(mapOf<String, Any>())
    private val _locationOK = MutableStateFlow(Results.UNANSWERED)
    private val _idUser = MutableStateFlow("")
    private val _token = MutableStateFlow("")

    val client: StateFlow<Client> get() = _client
    val direction: StateFlow<String> get() = _direction
    val statusBooking: StateFlow<String> get() = _statusBooking
    val clientBooking: StateFlow<ClientBooking> get() = _clientBooking
    val updateStatusClientBooking: StateFlow<Results> get() = _updateStatusClientBooking
    val updateIdHistoryBooking: StateFlow<Results> get() = _updateIdHistoryBooking
    val locationGPS: StateFlow<Map<String, Any>> get() = _locationGPS
    val locationOK: StateFlow<Results> get() = _locationOK
    val idUser: StateFlow<String> get() = _idUser
    val token: StateFlow<String> get() = _token

    init {
        viewModelScope.launch { iRepoAuth.getId().collect { _idUser.value = it } }
    }

    fun getStatusBooking(idClient: String) = viewModelScope.launch {
        if (statusBooking.value.isEmpty())
            dataStoreManager.getAny(DataStoreManager.KEY_STATUS_BOOKING)
                .collect { configBooking(it, idClient) }
    }

    private fun configBooking(statusBooking: String, idClient: String) =
        viewModelScope.launch {
            _statusBooking.value = statusBooking
            if (statusBooking == Constants.CREATE) {
                saveStatusBookingPref(Constants.V_RIDE)
                saveIdClientPref(idClient)
            }
        }

    fun saveStatusBookingPref(status: String) = viewModelScope.launch {
        dataStoreManager.saveAny(DataStoreManager.KEY_STATUS_BOOKING, status)
    }

    private fun saveIdClientPref(id: String) = viewModelScope.launch {
        dataStoreManager.saveAny(DataStoreManager.KEY_ID_CLIENT, id)
    }

    suspend fun getClientBooking(idUser: String) = viewModelScope.launch {
        iRepoClientBooking.getClientBooking(idUser).collect { getClientBooking(it) }
    }

    private fun getClientBooking(clientBooking: ClientBooking) {
        _clientBooking.value = clientBooking
        viewModelScope.launch { getClient(clientBooking.idClient) }
    }

    private fun getClient(idClient: String) = viewModelScope.launch {
        iRepoClient.getClient(idClient)
            .collect { if (it.id != "-1") _client.value = it.apply { id = idClient } }
    }

    @ExperimentalCoroutinesApi
    suspend fun checkOnLocation() = viewModelScope.launch {
        iRepoLocationManager.checkOnLocation().collect {
            when (it) {
                Results.OK -> startLocationGps()
                else -> {
                }
            }
        }
    }

    /*suspend fun requestActiveLocation(fragment: Fragment,resultGPS: ActivityResultLauncher<IntentSenderRequest>) =
        iRepoLocationManager.requestGPSSettings(fragment,resultGPS)

    private suspend fun requestPermissionLocation(resultPermission: ActivityResultLauncher<String>) =
        iRepoLocationManager.requestPermission(resultPermission)*/

    @ExperimentalCoroutinesApi
    suspend fun startLocationGps() = viewModelScope.launch {
        iRepoLocationManager.startLocationGPS().collect {
            _locationGPS.value = it
            _locationOK.value = Results.OK
            drawRoute(LatLng(it["LAT"] as Double, it["LNG"] as Double))
        }
    }

    private fun drawRoute(latLng: LatLng) {
        if (statusBooking.value == Constants.V_START)
            viewModelScope.launch {
                getDirections(
                    latLng, LatLng(
                        clientBooking.value.destinationLat, clientBooking.value.destinationLng
                    )
                )
            }
        else
            viewModelScope.launch {
                getDirections(
                    latLng, LatLng(
                        clientBooking.value.originLat, clientBooking.value.originLng
                    )
                )
            }
    }

    private fun cleanDataStoreManager(key: Preferences.Key<String>) =
        viewModelScope.launch { dataStoreManager.clearAny(key) }

    suspend fun saveLocation(referent: String, latLng: LatLng) = viewModelScope.launch {
        iRepoGeoFire.saveLocation(referent, idUser.value, latLng).collect {}
    }

    fun deleteLocation(referent: String) = viewModelScope.launch {
        iRepoGeoFire.removeLocation(referent, idUser.value).collect { }
    }

    suspend fun updateStatusClientBooking(idUser: String, status: String) =
        viewModelScope.launch {
            _statusBooking.value = status
            saveStatusBookingPref(status)
            if (status != Constants.END) drawRoute(
                LatLng(locationGPS.value["LAT"] as Double, locationGPS.value["LNG"] as Double)
            )
            iRepoClientBooking.updateStatus(idUser, status)
                .collect {
                    if (status != Constants.END) {
                        _updateStatusClientBooking.value = if (it) Results.OK else Results.FAIL
                    }
                }

        }

    private suspend fun getDirections(latLngOrigin: LatLng, latLngDestine: LatLng) =
        viewModelScope.launch {
            iRepoGoogleApi.getDirections(latLngOrigin, latLngDestine)
                .collect { _direction.value = it }
        }

    fun getToken(idUser: String) = viewModelScope.launch {
        iRepoToken.getToken(idUser).collect { _token.value = it }
    }

    fun sendNotification(fcmBody: FCMBody) = viewModelScope.launch {
        iRepoNotification.sendNotification(fcmBody).collect { }
    }

    fun updateIdHistoryBooking(idClientBooking: String) = viewModelScope.launch {
        iRepoClientBooking.updateIdHistoryBooking(idClientBooking)
            .collect {
                if (it) configUpdateIdHistoryBooking() else _updateIdHistoryBooking.value =
                    Results.FAIL
            }
    }

    private fun configUpdateIdHistoryBooking() {
        _updateIdHistoryBooking.value = Results.OK
        //cleanDataStoreManager(DataStoreManager.KEY_STATUS_BOOKING)
        saveStatusBookingPref(Constants.END)
        cleanDataStoreManager(DataStoreManager.KEY_ID_CLIENT)
    }
}