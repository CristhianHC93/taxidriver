package ec.com.designtechx.taxidriver.ui.activity

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.data.model.HistoryBooking
import ec.com.designtechx.taxidriver.databinding.ActivityHistoryBookingDetailClientBinding
import ec.com.designtechx.taxidriver.ui.view_models.HistoryBookingDetailVM
import ec.com.designtechx.taxidriver.utils.Constants
import java.util.*

@AndroidEntryPoint
class HistoryBookingDetailActivity : AppCompatActivity() {
    private lateinit var binding: ActivityHistoryBookingDetailClientBinding

    private val viewModel: HistoryBookingDetailVM by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        configBinding()
        intent.extras?.let {
            val idHistoryBooking = it.getString(Constants.ID_HISTORY_BOOKING).toString()
            if (idHistoryBooking.isNotEmpty()) viewModel.getHistoryBooking(idHistoryBooking) else finish(
                R.string.msg_problem_recover_register
            )
        }
        listenVM()
        eventClick()
    }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted { viewModel.historyBooking.collect { fillHistoryBookingData(it)} }
        lifecycleScope.launchWhenStarted { viewModel.client.collect { fillDriverData(it,viewModel.historyBooking.value)} }
        lifecycleScope.launchWhenStarted { viewModel.driver.collect { fillClientData(it,viewModel.historyBooking.value)} }
    }

    private fun fillClientData(driver: Driver, historyBooking: HistoryBooking) {
        binding.txtCalificationHistoryBookingDetail.text =
            historyBooking.calificationDriver.toString()
        if (!historyBooking.calificationClient.isNaN())
            binding.ratingBarHistoryBookingDetail.rating = historyBooking.calificationClient
        binding.txtNameBookingDetail.text = driver.name.uppercase(Locale.ROOT)
        if (driver.image.isNotEmpty()) {
            Picasso.get().load(driver.image).into(binding.circleImageHistoryBookingDetail)
        }
    }

    private fun fillDriverData(client: Client, historyBooking: HistoryBooking) {
        binding.txtCalificationHistoryBookingDetail.text =
            historyBooking.calificationClient.toString()
        if (!historyBooking.calificationDriver.isNaN())
            binding.ratingBarHistoryBookingDetail.rating = historyBooking.calificationDriver
        binding.txtNameBookingDetail.text = client.name.uppercase(Locale.ROOT)
        if (client.image.isNotEmpty()) {
            Picasso.get().load(client.image).into(binding.circleImageHistoryBookingDetail)
        }
    }

    private fun fillHistoryBookingData(historyBooking: HistoryBooking) {
        binding.txtOriginHistoryBookingDetail.text = historyBooking.origin
        binding.txtDestinationHistoryBookingDetail.text = historyBooking.destination
    }

    private fun configBinding() {
        binding = ActivityHistoryBookingDetailClientBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }


    private fun eventClick() {
        binding.circleImageBack.setOnClickListener { finish(null) }
    }

    private fun finish(msg: Int?) {
        if (msg != null) Toast.makeText(
            applicationContext, applicationContext.getString(msg), Toast.LENGTH_SHORT
        ).show()
        finish()
    }
}