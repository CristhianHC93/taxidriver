package ec.com.designtechx.taxidriver.data.model

data class ClientBooking(
    val idClient: String = "",
    val idDriver: String = "",
    val destination: String = "",
    val origin: String = "",
    val time: String = "",
    val km: String = "",
    val status: String = "",
    val originLat:Double = 0.0,
    val originLng:Double = 0.0,
    val destinationLat:Double = 0.0,
    val destinationLng:Double = 0.0,
    var idHistoryBooking: String = "",
    var price: Double = 0.0
)