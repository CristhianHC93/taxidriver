package ec.com.designtechx.taxidriver.data.irepo

import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.flow.Flow

interface IRepoGeoFire {
    suspend fun getActive(referent: String, latLng: LatLng, radius: Double): Flow<Map<String, Any>>
    suspend fun isDriverWorking(idUser: String): Flow<Boolean>
    suspend fun saveLocation(referent: String, idUser: String, latLng: LatLng): Flow<Boolean>
    suspend fun removeLocation(referent: String, idUser: String): Flow<Boolean>
    suspend fun getLocation(referent: String, idUser: String): Flow<LatLng>
}