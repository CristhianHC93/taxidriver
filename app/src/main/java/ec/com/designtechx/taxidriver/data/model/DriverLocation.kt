package ec.com.designtechx.taxidriver.data.model

import com.google.android.gms.maps.model.LatLng

data class DriverLocation(val id:String="",var latLng: LatLng?=null)
