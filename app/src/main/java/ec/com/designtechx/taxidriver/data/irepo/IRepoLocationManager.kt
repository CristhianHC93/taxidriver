package ec.com.designtechx.taxidriver.data.irepo

import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.fragment.app.Fragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.tasks.Task
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.flow.Flow

interface IRepoLocationManager {
    suspend fun checkOnLocation(): Flow<Results>
    suspend fun requestGPSSettings(fragment: Fragment,resultGPS: ActivityResultLauncher<IntentSenderRequest>)
    suspend fun requestPermission(resultPermission: ActivityResultLauncher<String>)
    suspend fun startLocationGPS(): Flow<Map<String, Any>>
    suspend fun startLocation(): Flow<LatLng>
    suspend fun stopLocation(): Flow<Task<Void>?>
}