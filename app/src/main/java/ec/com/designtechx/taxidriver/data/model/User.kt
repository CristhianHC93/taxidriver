package ec.com.designtechx.taxidriver.data.model

data class User(var id:String, var email:String, val password:String, var name:String,val phone:String){
    constructor(id: String,email: String,name: String) : this(id,email,"",name,"")
//    constructor(id: String,phone: String):this(id,"","","",phone)
//    constructor():this("","","","","")
}