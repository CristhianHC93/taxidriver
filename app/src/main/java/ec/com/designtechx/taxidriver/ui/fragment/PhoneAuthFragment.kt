package ec.com.designtechx.taxidriver.ui.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.databinding.FragmentPhoneAuthBinding
import ec.com.designtechx.taxidriver.ui.activity.AuthActivity
import ec.com.designtechx.taxidriver.ui.activity.MainActivity
import ec.com.designtechx.taxidriver.ui.view_models.PhoneAuthVM
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.Results

@AndroidEntryPoint
class PhoneAuthFragment : Fragment() {
    private var _binding: FragmentPhoneAuthBinding? = null
    private val binding get() = _binding!!

    private val viewModel: PhoneAuthVM by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentPhoneAuthBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listenVM()
        eventChange()
        eventClick()
        val phone = requireArguments().getString(Constants.PHONE, "")
        if (phone.isEmpty()) {
            (requireActivity() as AuthActivity).navController.navigateUp()
        } else {
            viewModel.sendCodeVerification(phone, requireActivity(), getCallback())
        }
    }

    private fun getCallback(): PhoneAuthProvider.OnVerificationStateChangedCallbacks {
        return object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            override fun onVerificationCompleted(p0: PhoneAuthCredential) {
                p0.smsCode?.let {
                    binding.txtCodeVerification.setText(it)
                }
            }

            override fun onVerificationFailed(e: FirebaseException) {
                e.localizedMessage?.let { verificationFailed(it) }
            }

            override fun onCodeSent(
                verificationId: String,
                forceResendingToken: PhoneAuthProvider.ForceResendingToken
            ) {
                // CODIGO DE VERIFICACION SE ENVIA TRAVES DE MENSAJE DE TEXT SMS
                super.onCodeSent(verificationId, forceResendingToken)
                viewModel.setVerificationId(verificationId)
            }
        }
    }

    private fun listenVM() {
        lifecycleScope.launchWhenCreated {
            viewModel.complete.collect {
                when (it) {
                    Results.OK -> startActivity(false)
                    Results.FAIL -> failSave()
                    Results.EMPTY -> startActivity(false)
                    Results.USER_WITH_DATA -> startActivity(true)
                    else -> {
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.verificationId.collect {
                if (it.isNotEmpty()) codeSent()
            }
        }
    }

    private suspend fun failSave() {
        Toast.makeText(
            requireActivity(), getString(R.string.not_login), Toast.LENGTH_SHORT
        ).show()
        viewModel.logout()
        findNavController().navigateUp()
    }

    private fun codeSent() {
        Toast.makeText(requireActivity(), getString(R.string.code_sended), Toast.LENGTH_SHORT)
            .show()
        enabledEdition()
    }

    private fun enabledEdition() {
        binding.progressBar?.visibility = View.GONE
        binding.btCodeVerification.isEnabled = true
        binding.txtCodeVerification.isEnabled = true
    }

    private fun verificationFailed(msg: String) {
        Toast.makeText(requireActivity(), msg, Toast.LENGTH_SHORT).show()
        findNavController().navigateUp()
    }

    private fun eventChange() {
        binding.txtCodeVerification.doAfterTextChanged {
            if (it.toString().length >= 6)
                signIn(it.toString())
        }
    }

    private fun eventClick() {
        binding.btCodeVerification.setOnClickListener { signIn(binding.txtCodeVerification.text.toString()) }
    }

    private fun signIn(code: String) {
        if (code.isNotEmpty() && code.length >= 6) {
            lifecycleScope.launchWhenStarted { viewModel.signIn(if (code[0] == '0') code.substring(1,code.length-1) else code) }
        } else {
            Toast.makeText(requireContext(), getString(R.string.enter_code), Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun startActivity(dataOfUser: Boolean) {
        startActivity(
            Intent(
                requireContext(), MainActivity::class.java
            ).apply {
                putExtra(Constants.DATA_OF_USER, dataOfUser)
            }
        )
    }


    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}