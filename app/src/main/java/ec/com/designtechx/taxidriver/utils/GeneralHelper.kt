package ec.com.designtechx.taxidriver.utils

import android.annotation.SuppressLint
import android.content.Context
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import androidx.core.graphics.BlendModeColorFilterCompat
import androidx.core.graphics.BlendModeCompat
import com.google.gson.Gson
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.User

class GeneralHelper constructor(var context: Context) {

    @SuppressLint("MissingPermission")
    fun isOnlineNet(): Boolean {
        try {
            val connectivityManager =
                (context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
            val activeNetwork = connectivityManager.activeNetwork ?: return false
            val networkCapabilities =
                connectivityManager.getNetworkCapabilities(activeNetwork) ?: return false
            return when {
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return false
    }


    //Metod for set color of all the itemMenu of  menu
    fun setColorItemMenu(drawableList: List<Drawable?>, color: Int) {
        drawableList.forEach { drawable ->
            drawable?.mutate()
            drawable?.colorFilter = BlendModeColorFilterCompat.createBlendModeColorFilterCompat(
                color,
                BlendModeCompat.SRC_ATOP
            )
        }
    }


    fun getStatus(): String {
        val prefs = context.getSharedPreferences(
            context.getString(R.string.prefs_file),
            Context.MODE_PRIVATE
        )
        return prefs.getString(Constants.KEY_STATUS, null)!!
    }

    fun saveSession(user: User) {
        val prefs = context.applicationContext.getSharedPreferences(
            context.getString(R.string.prefs_file),
            Context.MODE_PRIVATE
        ).edit()
        val json = Gson().toJson(user)
        prefs.putString(Constants.KEY_USER, json)
        prefs.apply()
    }

    fun appInstalled(uri: String): Boolean {
        val pm = context.packageManager
        return try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }
}