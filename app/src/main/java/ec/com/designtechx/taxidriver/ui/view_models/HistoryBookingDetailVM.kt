package ec.com.designtechx.taxidriver.ui.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import ec.com.designtechx.taxidriver.data.irepo.IRepoClient
import ec.com.designtechx.taxidriver.data.irepo.IRepoDriver
import ec.com.designtechx.taxidriver.data.irepo.IRepoHistoryBooking
import ec.com.designtechx.taxidriver.data.model.Client
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.data.model.HistoryBooking
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class HistoryBookingDetailVM @Inject constructor(
    private val dataStoreManager: DataStoreManager,
    private val iRepoHistoryBooking:IRepoHistoryBooking,
    private val iRepoDriver: IRepoDriver,
    private val iRepoClient: IRepoClient,
    private val iRepoAuth: IRepoAuth
) : ViewModel() {

    private val _idUser = MutableStateFlow("")
    val idUser: StateFlow<String> get() = _idUser

    private val _status = MutableStateFlow("")
    val status: StateFlow<String> get() = _status

    private val _driver = MutableStateFlow(Driver())
    val driver: StateFlow<Driver> get() = _driver

    private val _client = MutableStateFlow(Client())
    val client: StateFlow<Client> get() = _client

    private val _historyBooking = MutableStateFlow(HistoryBooking())
    val historyBooking: StateFlow<HistoryBooking> get() = _historyBooking
    init {
        viewModelScope.launch { iRepoAuth.getId().collect { _idUser.value = it } }
        viewModelScope.launch {
            dataStoreManager.getAny(DataStoreManager.KEY_STATUS).collect {
                _status.value = it
            }
        }
    }

    fun getHistoryBooking(idHistoryBooking: String)=viewModelScope.launch{
        iRepoHistoryBooking.getHistoryBooking(idHistoryBooking).collect {
            _historyBooking.value = it
            when(status.value){
                Constants.V_DRIVER -> getDriver(it.idDriver)
                Constants.V_CLIENT -> getClient(it.idClient)
            }
        }
    }

    private fun getClient(idClient: String)=viewModelScope.launch {
        iRepoClient.getClient(idClient).collect { if (it.id != Constants.ID_NOT_EXIST) _client.value = it}
    }

    private fun getDriver(idDriver: String)=viewModelScope.launch {
        iRepoDriver.getDriver(idDriver).collect { if (it.id != Constants.ID_NOT_EXIST) _driver.value = it}
    }
}