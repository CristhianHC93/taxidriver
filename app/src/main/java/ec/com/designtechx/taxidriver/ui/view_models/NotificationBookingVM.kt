package ec.com.designtechx.taxidriver.ui.view_models

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.IRepoAuth
import ec.com.designtechx.taxidriver.data.irepo.IRepoClientBooking
import ec.com.designtechx.taxidriver.data.irepo.IRepoDriverFound
import ec.com.designtechx.taxidriver.data.irepo.IRepoGeoFire
import ec.com.designtechx.taxidriver.data.model.ClientBooking
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class NotificationBookingVM @Inject constructor(
    private val iRepoClientBooking: IRepoClientBooking,
    private val iRepoGeoFire: IRepoGeoFire,
    private val iRepoAuth: IRepoAuth,
    private val iRepoDriverFound: IRepoDriverFound,
    private val dataStoreManager: DataStoreManager
) : ViewModel() {

    private val _clientBooking = MutableStateFlow(ClientBooking())
    private val _clientBookingListen = MutableStateFlow(ClientBooking())
    private val _idUser = MutableStateFlow("")


    val clientBooking: StateFlow<ClientBooking> get() = _clientBooking
    val clientBookingListen: StateFlow<ClientBooking> get() = _clientBookingListen
    val idUser: StateFlow<String> get() = _idUser

    init {
        viewModelScope.launch { iRepoAuth.getId().collect { _idUser.value = it } }
    }

    fun saveStatusBookingPref(status: String) = viewModelScope.launch(Dispatchers.IO) {
        dataStoreManager.saveAny(DataStoreManager.KEY_STATUS_BOOKING, status)
    }

    suspend fun deleteDriverFound() = iRepoDriverFound.delete(idUser.value).collect { }

    fun getClientBooking(idClientBooking: String) = viewModelScope.launch {
        iRepoClientBooking.getClientBooking(idClientBooking).collect { _clientBooking.value = it }
    }

    fun getClientBookingListen(idClientBooking: String) = viewModelScope.launch {
        iRepoClientBooking.getClientBookingListen(idClientBooking)
            .collect { _clientBookingListen.value = it }
    }

    suspend fun removeLocation(referent: String, idUser: String) {
        iRepoGeoFire.removeLocation(referent, idUser).collect { }
    }

    suspend fun updateStatusClientBooking(idClient: String, status: String) {
        iRepoClientBooking.updateStatus(idClient, status).collect { }
    }

    suspend fun updateStatusAndIdDriverClientBooking(
        idClient: String, status: String, idDriver: String
    ) {
        iRepoClientBooking.updateStatusAndIdDriver(idClient, status, idDriver).collect { }
    }

}