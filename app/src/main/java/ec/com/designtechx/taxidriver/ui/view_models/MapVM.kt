package ec.com.designtechx.taxidriver.ui.view_models

import android.content.Context
import android.location.Geocoder
import android.util.Log
import androidx.activity.result.ActivityResultLauncher
import androidx.activity.result.IntentSenderRequest
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.google.android.gms.common.api.Status
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.CameraPosition
import com.google.android.gms.maps.model.LatLng
import com.google.android.libraries.places.api.model.Place
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener
import dagger.hilt.android.lifecycle.HiltViewModel
import ec.com.designtechx.taxidriver.data.irepo.*
import ec.com.designtechx.taxidriver.data.model.Driver
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.DataStoreManager
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MapVM @Inject constructor(
    private val dataStoreManager: DataStoreManager,
    private val iRepoGeoFire: IRepoGeoFire,
    private val iRepoToken: IRepoToken,
    private val iRepoDriver: IRepoDriver,
    private val iRepoDriverFound: IRepoDriverFound,
    private val iRepoAuth: IRepoAuth,
    private val iRepoLocationManager: IRepoLocationManager
) : ViewModel() {

    private val _idClient = MutableStateFlow("")
    val idClient: StateFlow<String> get() = _idClient

    private val _nameOrigin = MutableStateFlow("")
    private val _latLonOrigin = MutableStateFlow(LatLng(0.0, 0.0))
    private val _nameDestine = MutableStateFlow("")
    private val _latLonDestine = MutableStateFlow(LatLng(0.0, 0.0))
    private val _locationPlaceSelected = MutableStateFlow(LatLng(0.0, 0.0))
    private val _selectInMap = MutableStateFlow(1)
    private val _isConnect = MutableStateFlow(true)
    private val _locationGPS = MutableStateFlow(mapOf<String, Any>())
    private val _location = MutableStateFlow(LatLng(0.0, 0.0))
    private val _idUser = MutableStateFlow("")
    private val _active = MutableStateFlow(mapOf<String, Any>())
    private val _isWorking = MutableStateFlow(Results.UNANSWERED)
    private val _completeSave = MutableStateFlow(Results.UNANSWERED)
    private val _completeDelete = MutableStateFlow(Results.UNANSWERED)
    private val _idClientPref = MutableStateFlow("")
    private val _idDriverPref = MutableStateFlow("")
    private val _status = MutableStateFlow("")
    private val _profileDriver = MutableStateFlow(Driver())

    val nameOrigin: StateFlow<String> get() = _nameOrigin
    val latLonOrigin: StateFlow<LatLng> get() = _latLonOrigin
    val nameDestine: StateFlow<String> get() = _nameDestine
    val latLonDestine: StateFlow<LatLng> get() = _latLonDestine
    val locationPlaceSelected: StateFlow<LatLng> get() = _locationPlaceSelected
    val selectInMap: StateFlow<Int> get() = _selectInMap
    val isConnect: StateFlow<Boolean> get() = _isConnect
    val locationGPS: StateFlow<Map<String, Any>> get() = _locationGPS
    val location: StateFlow<LatLng> get() = _location
    val idUser: StateFlow<String> get() = _idUser
    val active: StateFlow<Map<String, Any>> get() = _active
    val isWorking: StateFlow<Results> get() = _isWorking
    val idDriverPref: StateFlow<String> get() = _idDriverPref
    val idClientPref: StateFlow<String> get() = _idClientPref
    val status: StateFlow<String> get() = _status
    val profileDriver: StateFlow<Driver> get() = _profileDriver

    init {
        viewModelScope.launch { iRepoAuth.getId().collect { _idUser.value = it } }
        viewModelScope.launch {
            dataStoreManager.getAny(DataStoreManager.KEY_STATUS).collect { _status.value = it }
        }

        viewModelScope.launch {
            iRepoGeoFire.isDriverWorking(idUser.value).collect {
                _isWorking.value = if (it) Results.OK else Results.NON_EXISTENT
            }
        }

    }

    fun getStatusBooking() = viewModelScope.launch {
        dataStoreManager.getAny(DataStoreManager.KEY_STATUS_BOOKING).collect { configBooking(it) }
    }


    private fun configBooking(statusBooking: String) = viewModelScope.launch {
        when (statusBooking) {
            Constants.V_RIDE -> if (status.value == Constants.V_DRIVER) getIdClientPref() else _idDriverPref.value =
                "+1"
            Constants.V_START -> if (status.value == Constants.V_DRIVER) getIdClientPref() else _idDriverPref.value =
                "+1"
            Constants.END -> if (status.value == Constants.V_DRIVER) getIdClientPref() else _idClient.value =
                idUser.value
            else -> {
                dataStoreManager.clearAny(DataStoreManager.KEY_STATUS_BOOKING)
                if (status.value == Constants.V_DRIVER) deleteDriverFound() //else deleteClientBooking()
            }
        }
//        if (statusBooking == Constants.V_RIDE || statusBooking == Constants.V_START)
//            if (status.value == Constants.V_DRIVER) getIdClientPref() else _idDriverPref.value =
//                "+1"
//        else {
//            dataStoreManager.clearAny(DataStoreManager.KEY_STATUS_BOOKING)
//            if (status.value == Constants.V_DRIVER) deleteDriverFound() //else deleteClientBooking()
//        }
    }

//    private fun getIdDriverPref() =
//        viewModelScope.launch { _idDriverPref.value = "+1" }

    private fun getIdClientPref() = viewModelScope.launch {
        dataStoreManager.getAny(DataStoreManager.KEY_ID_CLIENT).collect { _idClientPref.value = it }
    }

    suspend fun saveLocation(referent: String, latLng: LatLng) = viewModelScope.launch {
        iRepoGeoFire.saveLocation(referent, idUser.value, latLng).collect {
            _completeSave.value = if (it) Results.OK else Results.FAIL
        }
    }

    suspend fun deleteLocation(referent: String) = viewModelScope.launch {
        iRepoGeoFire.removeLocation(referent, idUser.value).collect {
            _completeDelete.value = if (it) Results.OK else Results.FAIL
        }
    }

    suspend fun activeLocation(referent: String, latLng: LatLng) = viewModelScope.launch {
        iRepoGeoFire.getActive(referent, latLng, 10.0).collect { _active.value = it }
    }

    suspend fun startLocationGps() = viewModelScope.launch {
        iRepoLocationManager.startLocationGPS().collect {
            _locationGPS.value = it
            _location.value = LatLng(it["LAT"] as Double, it["LNG"] as Double)
        }
    }

    suspend fun createToken() =
        viewModelScope.launch { iRepoToken.createToken(idUser.value).collect { } }

    private suspend fun deleteDriverFound() {
        iRepoDriverFound.delete(idUser.value).collect { }
    }

    suspend fun logOut() = iRepoAuth.logout()

    fun animateCamera(mMap: GoogleMap, latLng: LatLng) = viewModelScope.launch {
        if (latLng.latitude != 0.0 && latLng.longitude != 0.0){
        mMap.animateCamera(
            CameraUpdateFactory.newCameraPosition(
                CameraPosition.Builder().target(latLng).zoom(18f).build()
            )
        )}
    }

    suspend fun checkOnLocation(fragment: Fragment, resultGPS: ActivityResultLauncher<IntentSenderRequest>,resultPermission: ActivityResultLauncher<String>) =
        iRepoLocationManager.checkOnLocation().collect {
            when (it) {
                Results.OK -> startLocation()
                Results.NOT_ACTIVE -> requestActiveLocation(fragment,resultGPS)
                Results.WITHOUT_PERMISSION -> {
                    requestPermissionLocation(resultPermission)
                }
                else -> {
                }
            }
        }

    suspend fun requestActiveLocation(fragment: Fragment,resultGPS: ActivityResultLauncher<IntentSenderRequest>) =
        iRepoLocationManager.requestGPSSettings(fragment,resultGPS)

    private suspend fun requestPermissionLocation(resultPermission: ActivityResultLauncher<String>) =
        iRepoLocationManager.requestPermission(resultPermission)


    private suspend fun startLocation() =
        viewModelScope.launch {
            iRepoLocationManager.startLocation().collect { _location.value = it }
        }

    fun stopLocation() = viewModelScope.launch { iRepoLocationManager.stopLocation() }

    fun connect() = viewModelScope.launch { _isConnect.value = !isConnect.value }

    suspend fun getDriver(idDriver: String) {
        iRepoDriver.getDriver(idDriver)
            .collect { driver ->
                if (driver.id != "0") {
                    _profileDriver.value = driver.apply { id = idDriver }
                }
            }
    }

    fun changeSelectMap() = viewModelScope.launch {
        when (selectInMap.value) {
            Constants.ORIGIN -> _selectInMap.value = Constants.DESTINE
            Constants.DESTINE -> _selectInMap.value = Constants.CAR
            Constants.CAR -> _selectInMap.value = Constants.ORIGIN
        }
    }

    fun listenPlaceSelected(context: Context): PlaceSelectionListener {
        return object : PlaceSelectionListener {
            override fun onPlaceSelected(p0: Place) {
                p0.latLng?.let {
                    viewModelScope.launch { changeLocateSelect(it, context) }
                    viewModelScope.launch { _locationPlaceSelected.value = p0.latLng!! }
                }
            }

            override fun onError(p0: Status) {
                p0.statusMessage?.let { Log.e(this@MapVM::class.java.simpleName, it) }
            }
        }
    }

    fun changeLocateSelect(
        latLng: LatLng,
        context: Context
    ) {
        if (latLng.latitude != 0.0 && latLng.longitude != 0.0) {
            when (selectInMap.value) {
                Constants.ORIGIN -> {
                    _nameOrigin.value = getNamePosition(latLng, context)
                    _latLonOrigin.value = latLng
                }
                Constants.DESTINE -> {
                    _nameDestine.value = getNamePosition(latLng, context)
                    _latLonDestine.value = latLng
                }
            }
        }
    }

    private fun getNamePosition(latLng: LatLng, context: Context): String {
        val geocoder = Geocoder(context)
        val addressList =
            geocoder.getFromLocation(latLng.latitude, latLng.longitude, 1)
        return if (addressList!!.size > 0) {
            val city = addressList[0].locality
            //val country = addressList[0].countryName
            val address = addressList[0].getAddressLine(0)
            "$address $city"
        } else ""
    }

}