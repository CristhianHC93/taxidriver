package ec.com.designtechx.taxidriver.provider

import android.content.Context
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.google.firebase.storage.UploadTask
import ec.com.designtechx.taxidriver.utils.CompressorBitmapImage
import java.io.File

class ImageProvider {
    private lateinit var mStorage: StorageReference

    fun selectRef(ref: String): ImageProvider {
        mStorage = FirebaseStorage.getInstance().reference.child(ref)
        return this
    }

    suspend fun saveImage(context: Context, image: File, idUser: String): UploadTask {
        val imageByte = CompressorBitmapImage.getImage(context, image.path, 500, 500)
        val storage = mStorage.child("$idUser.jpg")
        mStorage = storage
        return storage.putBytes(imageByte!!)
    }

    fun getStorage(): StorageReference {
        return mStorage
    }
}