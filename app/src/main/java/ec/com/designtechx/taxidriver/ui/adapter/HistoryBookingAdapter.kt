package ec.com.designtechx.taxidriver.ui.adapter

import android.Manifest
import android.app.Activity
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.telephony.PhoneNumberUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.ActivityCompat
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.database.FirebaseRecyclerAdapter
import com.firebase.ui.database.FirebaseRecyclerOptions
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.squareup.picasso.Picasso
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.HistoryBooking
import ec.com.designtechx.taxidriver.provider.ClientProvider
import ec.com.designtechx.taxidriver.provider.DriverProvider
import ec.com.designtechx.taxidriver.ui.activity.HistoryBookingDetailActivity
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.GeneralHelper
import javax.inject.Inject


data class HistoryBookingAdapter(
    val options: FirebaseRecyclerOptions<HistoryBooking>,
    val context: Context,
    val statusDriver: Boolean
) :
    FirebaseRecyclerAdapter<HistoryBooking, HistoryBookingAdapter.ViewHolder>(options) {
    private val driverProvider = DriverProvider.instance!!
    private val mClientProvider = ClientProvider.instance!!
    @Inject
    lateinit var generalHelper: GeneralHelper

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        internal val txtName: TextView = view.findViewById(R.id.txt_name)
        internal val txtOrigin: TextView = view.findViewById(R.id.txt_origin)
        internal val txtDestine: TextView = view.findViewById(R.id.txt_destine)

        //internal val txtDate: TextView = view.findViewById(R.id.txt_date)
        internal val txtCalification: TextView = view.findViewById(R.id.txt_calification)
        internal val image: ImageView = view.findViewById(R.id.image)
        internal val ibtWhastapp: ImageView = view.findViewById(R.id.ibt_whastapp)
        internal val ibtEmail: ImageView = view.findViewById(R.id.ibt_email)
        internal val ibtCall: ImageView = view.findViewById(R.id.ibt_call)
        internal val ibtView: ImageView = view.findViewById(R.id.ibt_view)
        //internal val mView: View = view

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.card_history_booking, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(
        holder: ViewHolder,
        position: Int,
        historyBooking: HistoryBooking
    ) {
        val id = getRef(position).key

        holder.txtOrigin.text = historyBooking.origin
        holder.txtDestine.text = historyBooking.destination

        //holder.txtDate.text = convertDate(historyBooking.timestamp.toString())
        if (statusDriver) configDriver(historyBooking, holder) else configClient(
            historyBooking,
            holder
        )
        holder.ibtView.setOnClickListener { openActivity(id) }
        holder.ibtWhastapp.setOnClickListener { openWhastApp(holder.ibtWhastapp.contentDescription) }
        holder.ibtEmail.setOnClickListener {openEmail() }
        holder.ibtCall.setOnClickListener { callAction(holder.ibtWhastapp.contentDescription)}
    }

    private fun openEmail() {
//        if (email.isNotEmpty()){
//
//        }
    }

    private fun callAction(phone: CharSequence) {
        if (phone.isNotEmpty()) {
            val num: Uri = Uri.parse("tel:$phone")
            val i = Intent(Intent.ACTION_CALL, num)
            val ac = context as Activity
            if (ActivityCompat.checkSelfPermission(
                    ac,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return
            }
            ac.startActivity(i)
        }
    }

    private fun openWhastApp(number: CharSequence) {
        val isWhatsAppInstalled: Boolean = generalHelper.appInstalled("com.whatsapp")
        if (isWhatsAppInstalled) {

            if (number.isNotEmpty()) {
                val sendIntent = Intent("android.intent.action.MAIN")
                sendIntent.component =
                    ComponentName("com.whatsapp", "com.whatsapp.Conversation")
                sendIntent.putExtra(
                    "jid",
                    PhoneNumberUtils.stripSeparators(
                        number.substring(
                            1,
                            number.length
                        )
                    ) + "@s.whatsapp.net"
                ) //phone number without "+" prefix
                val activity = context as Activity
                activity.startActivity(sendIntent)
            }
        } else
            Toast.makeText(
                context, context.getString(R.string.app_not_installed), Toast.LENGTH_SHORT
            ).show()
    }

    private fun openActivity(id: String?) {
        val intent = Intent(context, HistoryBookingDetailActivity::class.java)
        intent.putExtra(Constants.ID_HISTORY_BOOKING, id)
        context.startActivity(intent)
    }

    private fun configDriver(historyBooking: HistoryBooking, holder: ViewHolder) {
        holder.txtCalification.text = historyBooking.calificationDriver.toString()
        mClientProvider.getClient(historyBooking.idClient)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot.exists()) {
                        val name = dataSnapshot.child("name").value.toString()
                        holder.txtName.text = name
                        holder.ibtWhastapp.contentDescription =
                            dataSnapshot.child("phone").value.toString()
                        holder.ibtEmail.contentDescription =
                            dataSnapshot.child("email").value.toString()
                        if (dataSnapshot.hasChild("image")) {
                            val image = dataSnapshot.child("image").value.toString()
                            Picasso.get().load(image).into(holder.image)
                        }
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }

    private fun configClient(historyBooking: HistoryBooking, holder: ViewHolder) {
        holder.txtCalification.text = historyBooking.calificationClient.toString()
        driverProvider.getDriverAdapter(historyBooking.idDriver)
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    if (dataSnapshot.exists()) {
                        val name = dataSnapshot.child("name").value.toString()
                        holder.txtName.text = name
                        holder.ibtWhastapp.contentDescription =
                            dataSnapshot.child("phone").value.toString()
                        holder.ibtEmail.contentDescription =
                            dataSnapshot.child("email").value.toString()
                        if (dataSnapshot.hasChild("image")) {
                            val image = dataSnapshot.child("image").value.toString()
                            Picasso.get().load(image).into(holder.image)
                        }
                    }
                }

                override fun onCancelled(databaseError: DatabaseError) {}
            })
    }
}