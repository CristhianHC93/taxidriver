package ec.com.designtechx.taxidriver.data.irepo

import android.app.Activity
import com.google.firebase.auth.PhoneAuthProvider
import kotlinx.coroutines.flow.Flow

interface IRepoAuth {
    suspend fun sendCodeVerification(phone: String, activity: Activity,callback: PhoneAuthProvider.OnVerificationStateChangedCallbacks)
    fun getId(): Flow<String>
    suspend fun signIn(verificationId: String, code: String): Flow<Boolean>
    suspend fun sessionOn(): Flow<Boolean>
    suspend fun logout()
}