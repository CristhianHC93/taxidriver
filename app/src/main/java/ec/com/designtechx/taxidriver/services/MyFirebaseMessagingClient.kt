package ec.com.designtechx.taxidriver.services

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.drawable.Icon
import android.media.RingtoneManager
import android.os.PowerManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.channel.NotificationHelper
import ec.com.designtechx.taxidriver.reciver.AcceptReceiver
import ec.com.designtechx.taxidriver.reciver.CancelReceiver
import ec.com.designtechx.taxidriver.ui.activity.NotificationBookingActivity
import ec.com.designtechx.taxidriver.utils.Constants
import kotlinx.coroutines.DelicateCoroutinesApi
import kotlinx.coroutines.ExperimentalCoroutinesApi

@DelicateCoroutinesApi
@ExperimentalCoroutinesApi
class MyFirebaseMessagingClient : FirebaseMessagingService() {

    override fun onMessageReceived(p0: RemoteMessage) {
        super.onMessageReceived(p0)
        //val notification = p0.notification
        val data = p0.data
        val title = data["title"]
        val body = data["body"]
        if (title != null && title.isNotEmpty()) {
            val idClient = data[Constants.ID_CLIENT]
            val origin = data[Constants.M_ORIGIN]
            val destination = data[Constants.DESTINATION]
            val searchById = data[Constants.SEARCH_BY_ID]

            when {
                title.contains("SOLICITUD DE SERVICIO") -> {
                    showNotificationActions(title, body!!, idClient!!, searchById!!)
                    showNotificationActivity(
                        idClient,
                        origin!!,
                        destination!!,
                        searchById
                    )
                }
                title.contains("VIAJE CANCELADO") -> {
                    val manager =
                        getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
                    manager.cancel(2)
                    //showNotification(title, body ?: "")
                }
                else -> showNotification(title, body ?: "")
            }
        }
    }

    private fun showNotificationActivity(
        idClient: String, origin: String, destination: String, searchById: String
    ) {
        val pm = baseContext.getSystemService(POWER_SERVICE) as PowerManager
        val isScreenOn = pm.isInteractive
        if (!isScreenOn) {
            val wakeLock = pm.newWakeLock(
                PowerManager.PARTIAL_WAKE_LOCK or PowerManager.ACQUIRE_CAUSES_WAKEUP or
                        PowerManager.ON_AFTER_RELEASE, "AppName:MyLock"
            )
            wakeLock.acquire(10000)
        }
        val intent = Intent(baseContext, NotificationBookingActivity::class.java).apply {
            putExtra(Constants.ID_CLIENT, idClient)
            putExtra(Constants.M_ORIGIN, origin)
            putExtra(Constants.DESTINATION, destination)
            putExtra(Constants.SEARCH_BY_ID, searchById)
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        }
        startActivity(intent)
    }

    private fun showNotification(title: String, body: String) {
        val intent = PendingIntent.getActivity(
            baseContext, 0, Intent(), PendingIntent.FLAG_IMMUTABLE
        )
        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationHelper = NotificationHelper(baseContext, soundUri)
        val builder = notificationHelper.getNotification(title, body, intent)
        notificationHelper.getManager().notify(1, builder.build())
    }

    private fun showNotificationActions(
        title: String, body: String, idClient: String, searchById: String
    ) {
        val acceptAction = createBtAccept(idClient, searchById) as Notification.Action
        val cancelAction = createBtCancel(idClient,  searchById) as Notification.Action

        val soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationHelper = NotificationHelper(baseContext, soundUri)
        val builder = notificationHelper.getNotificationActions(
            title, body, acceptAction, cancelAction
        )
        notificationHelper.getManager().notify(2, builder.build())
    }

    private fun createBtAccept(idClient: String, searchById: String): Any {
        val acceptIntent = Intent(this, AcceptReceiver::class.java)
        acceptIntent.putExtra(Constants.ID_CLIENT, idClient)
        acceptIntent.putExtra(Constants.OF_NOTIFICATION, true)
        acceptIntent.putExtra(Constants.SEARCH_BY_ID, searchById)
        val acceptPendingIntent = PendingIntent.getBroadcast(
            this, NOTIFICATION_CODE,
            acceptIntent, PendingIntent.FLAG_IMMUTABLE
        )
        return Notification.Action.Builder(
            Icon.createWithResource(this, R.drawable.icon_car),
            getString(R.string.m_accept), acceptPendingIntent
        ).build()
    }

    private fun createBtCancel(idClient: String, searchById: String): Any {
        val cancelIntent = Intent(this, CancelReceiver::class.java)
        cancelIntent.putExtra(Constants.ID_CLIENT, idClient)
        cancelIntent.putExtra(Constants.SEARCH_BY_ID, searchById)
        val cancelPendingIntent = PendingIntent.getBroadcast(
            this, NOTIFICATION_CODE, cancelIntent, PendingIntent.FLAG_IMMUTABLE
        )
        return Notification.Action.Builder(
            Icon.createWithResource(this, R.drawable.icon_car),
            getString(R.string.m_cancel), cancelPendingIntent
        ).build()
    }

    companion object {
        const val NOTIFICATION_CODE = 100
    }
}