package ec.com.designtechx.taxidriver.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.widget.TextView
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.Marker
import com.squareup.picasso.Callback
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView
import ec.com.designtechx.taxidriver.R
import java.lang.Exception
import kotlin.collections.HashMap

data class PopupAdapter(
    val context: Context,
    val inflater: LayoutInflater,
    val image: HashMap<String, String>
) : GoogleMap.InfoWindowAdapter {
    private var popup: View? = null
    private var lastMarker: Marker? = null

    override fun getInfoWindow(p0: Marker): View? {
        return null
    }

    @SuppressLint("InflateParams")
    override fun getInfoContents(marker: Marker): View {
        if (popup == null) {
            popup = inflater.inflate(R.layout.popup, null)
        }

        if (lastMarker == null || lastMarker?.id != marker.id) {
            lastMarker = marker
            val tv = popup!!.findViewById<TextView>(R.id.title)
            val icon: CircleImageView = popup!!.findViewById(R.id.icon)
//            val btRequest: Button = popup!!.findViewById(R.id.bt_request)
            tv.text = marker.title
            // tv.setText(marker.getSnippet());
            val image = this.image[marker.tag]
            if (image!!.isEmpty()) {
                icon.setImageResource(R.drawable.ic_person)
            } else {
                Picasso.get().load(image).into(icon, MarkerCallback(marker))
            }
        }

        return popup!!
    }

    internal data class MarkerCallback(val marker: Marker) : Callback {
        override fun onSuccess() {}
            /*if (marker.isInfoWindowShown)
                marker.showInfoWindow()
            else
                marker.hideInfoWindow()*/

        override fun onError(e: Exception?) {
            Log.e(javaClass.simpleName, "Error loading thumbnail!")
        }
    }
}