package ec.com.designtechx.taxidriver.ui.fragment

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.activity.addCallback
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.google.android.gms.maps.model.LatLng
import dagger.hilt.android.AndroidEntryPoint
import ec.com.designtechx.taxidriver.R
import ec.com.designtechx.taxidriver.data.model.*
import ec.com.designtechx.taxidriver.databinding.FragmentRequestDriverBinding
import ec.com.designtechx.taxidriver.ui.view_models.RequestDriverVM
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.Results

@AndroidEntryPoint
class RequestDriverFragment : Fragment() {
    private var _binding: FragmentRequestDriverBinding? = null
    private val binding get() = _binding!!

    private val viewModel: RequestDriverVM by viewModels()

    private val latLngOrigin by lazy {
        LatLng(
            requireArguments().getDouble(Constants.ORIGIN_LAT),
            requireArguments().getDouble(Constants.ORIGIN_LNG)
        )
    }
    private val latLngDestine by lazy {
        LatLng(
            requireArguments().getDouble(Constants.DESTINE_LAT),
            requireArguments().getDouble(Constants.DESTINE_LNG)
        )
    }

    //    private val latlngDriver by lazy {
//        LatLng(
//            requireArguments().getDouble(Constants.DRIVER_LAT),
//            requireArguments().getDouble(Constants.DRIVER_LNG)
//        )
//    }
    private val nameDestine by lazy { requireArguments().getString(Constants.DESTINE_NAME, "") }
    private val nameOrigin by lazy { requireArguments().getString(Constants.ORIGIN_NAME, "") }
    private val idDriver by lazy { requireArguments().getString(Constants.ID_DRIVER, "") }

    private val listDriverFound = mutableListOf<String>()
    private val listToken = mutableListOf<String>()
    private var counter = 0
    private var mCounterDriversAvailable = 0

    private var mTimeLimit = 0
    private val mHandler: Handler = Handler(Looper.getMainLooper())
    private var mIsFinishSearch = false


    private var runnable: Runnable = object : Runnable {
        override fun run() {
            if (mTimeLimit < 60) {
                mTimeLimit++
                mHandler.postDelayed(this, 1000)
            } else {
                cancelRequest()
                mHandler.removeCallbacks(this)
            }
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentRequestDriverBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        listenVM()
        binding.animation.playAnimation()
        if (idDriver.isEmpty()) getCloseDriver() else getToken(idDriver)
        eventClick()
        requireActivity().onBackPressedDispatcher.addCallback(this) {
            Toast.makeText(requireActivity(), getString(R.string.not_can_return), Toast.LENGTH_LONG)
                .show()
        }
    }

    private fun getToken(idDriver: String) =
        lifecycleScope.launchWhenCreated { viewModel.getToken(idDriver) }

    private fun eventClick() {
        binding.btCancelRequest.setOnClickListener { cancelRequest() }
    }

    private fun cancelRequest() =
        lifecycleScope.launchWhenCreated { viewModel.getClientBooking(viewModel.idUser.value) }

    private fun deleteDriverFound() =
        listDriverFound.forEach { lifecycleScope.launchWhenStarted { viewModel.deleteDriverFound(it) } }

    private fun getCloseDriver() {
        lifecycleScope.launchWhenStarted {
            viewModel.activeLocation(Constants.ACTIVE_DRIVER, latLngOrigin)
        }
    }

    private fun checkIfDriverIsAvailable() {
        listDriverFound.forEach {
            lifecycleScope.launchWhenStarted {
                viewModel.getDriverFoundByIdDriver(
                    it
                )
            }
        }
        if (mCounterDriversAvailable == listDriverFound.size) {
            getDriversToken()
        }
    }

    private fun getDriversToken() {
        if (listDriverFound.size == 0) {
            getCloseDriver()
            return
        }
        binding.textViewLookingFor.text = getString(R.string.wait_response)
        listDriverFound.forEach { getToken(it) }
    }

    private fun sendNotification() {
        val map = mapOf(
            Pair("title", "SOLICITUD DE SERVICIO"),
            Pair(
                "body",
                "Un cliente esta solicitando \n Recoger en: $nameOrigin\n Destino: $nameDestine"
            ),
            Pair(Constants.ID_CLIENT, viewModel.idUser.value),
            Pair(Constants.M_ORIGIN, nameOrigin),
            Pair(Constants.DESTINATION, nameDestine),
            Pair(Constants.SEARCH_BY_ID, if (idDriver.isEmpty()) "" else "yes")
        )
        val body = FCMBody(listToken, "high", map)
        lifecycleScope.launchWhenStarted { viewModel.sendNotification(body) }
    }

    private fun sendNotificationCancelToDrivers(idDriver: String) {
        if (listToken.size > 0) {
            //String token = dataSnapshot.child("token").getValue().toString();
            val map: MutableMap<String, String> = HashMap()
            map["title"] = "VIAJE CANCELADO"
            map["body"] = "El cliente cancelo la solicitud"

            // ELIMINAR DE LA LISTA DE TOKEN
            // EL TOKEN DEL CONDUCTOR QUE ACEPTO EL VIAJE
            listToken.remove(idDriver)
            val fcmBody = FCMBody(listToken, "high", map)
            lifecycleScope.launchWhenStarted { viewModel.sendNotificationCancelAll(fcmBody) }
        }
    }

    private fun sendNotificationCancel() {
        if (listToken.size > 0) {
            val map = mapOf(
                Pair("title", "VIAJE CANCELADO"),
                Pair("body", "El cliente cancelo la solicitud")
            )
            val body = FCMBody(listToken, "high", map)
            lifecycleScope.launchWhenStarted { viewModel.sendNotificationCancel(body) }
        } else
            moveToFragment(
                R.string.cancel_request,
                R.id.mapFragment,
                Bundle().apply { putBoolean(Constants.DATA_OF_USER, true) })
    }

    private fun moveToFragment(msg: Int?, fragment: Int, bundle: Bundle) {
        if (msg != null)
            Toast.makeText(
                requireContext(),
                requireActivity().getString(msg),
                Toast.LENGTH_SHORT
            )
                .show()
        findNavController().navigate(fragment, bundle)
    }

    private fun createClientBooking() {
        val clientBooking = ClientBooking(
            viewModel.idUser.value,
            "",
            nameDestine,
            nameOrigin,
            "",
            "",
            Constants.CREATE,
            latLngOrigin.latitude,
            latLngOrigin.longitude,
            latLngDestine.latitude,
            latLngDestine.longitude
        )
        // ESTAMOS RECORRIENDO LA LISTA DE LOS CONDUCTORES ENCONTRADOS PARA ALMACENARLOS EN FIREBASE
        lifecycleScope.launchWhenStarted { viewModel.createClientBooking(clientBooking) }
    }

    private fun checkStatusClientBooking() =
        lifecycleScope.launchWhenStarted { viewModel.getClientBookingListen(viewModel.idUser.value) }

    private fun listenVM() {
        lifecycleScope.launchWhenStarted {
            viewModel.active.collect { geo ->
                when (geo["NAME"]) {
                    "onKeyEntered" -> {
                        listDriverFound.add(geo["KEY"] as String)
                        mCounterDriversAvailable += 1
                    }
                    "onGeoQueryReady" -> checkIfDriverIsAvailable()
                    else -> {
                    }
                }
            }
        }

        lifecycleScope.launchWhenStarted {
            viewModel.token.collect {
                if (it.isNotEmpty()) {
                    listToken.add(it)
                    if (idDriver.isEmpty()) {
                        counter += 1
                        if (counter == listDriverFound.size) {
                            sendNotification()
                        }
                    } else sendNotification()
                }
            }
        }
        lifecycleScope.launchWhenCreated {
            viewModel.clientBooking.collect {
                if (it.idClient.isNotEmpty()) {
                    if (it.idClient != "-1") {
                        deleteDriverFound()
                        viewModel.deleteClientBooking(viewModel.idUser.value)
                        sendNotificationCancel()
                    } else {
                        moveToFragment(
                            null,
                            R.id.mapFragment,
                            Bundle().apply { putBoolean(Constants.DATA_OF_USER, true) })
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.completeCreateCB.collect {
                if (it == Results.OK) {
                    Toast.makeText(
                        requireContext(), getString(R.string.m_success), Toast.LENGTH_SHORT
                    ).show()
                    createDriverFound()
//                    mHandler.postDelayed(runnable, 1000)
                    checkStatusClientBooking()

                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.clientBookingListen.collect {
                if (it.idClient.isNotEmpty()) {
                    if (idDriver.isEmpty()) {
                        if (it.status == Constants.ACCEPT && it.idDriver.isNotEmpty()) {
                            sendNotificationCancelToDrivers(it.idDriver)
                            viewModel.saveStatusBookingPref(Constants.CREATE)
                            moveToFragment(null, R.id.mapClientBookingFragment, Bundle())
                        }
                    } else {
                        if (it.status == Constants.ACCEPT) {
                            findNavController().navigate(R.id.mapClientBookingFragment)
                        } else if (it.status == Constants.CANCEL) {
                            moveToFragment(
                                R.string.driver_not_accept,
                                R.id.mapFragment,
                                Bundle().apply { putBoolean(Constants.DATA_OF_USER, true) })
                        }
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.listDriverFound.collect {
                if (it.size > 0) {
                    for (d in it) {
                        listDriverFound.remove(d.idDriver)
                        mCounterDriversAvailable -= 1
                    }
                }
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.responseNotificationCancel.collect {
                if (it.success > 0)
                    moveToFragment(
                        R.string.cancel_request,
                        R.id.mapFragment,
                        Bundle().apply { putBoolean(Constants.DATA_OF_USER, true) })
            }
        }
        lifecycleScope.launchWhenStarted {
            viewModel.responseNotification.collect {
                if (it.success > 0) createClientBooking()
            }
        }

        /*

        lifecycleScope.launchWhenStarted {
            viewModel.responseGoogleApi.collect {
                if (it.isNotEmpty()) {
                    val jsonObject = JSONObject(it)
                    val jsonArray: JSONArray = jsonObject.getJSONArray("routes")
                    val route = jsonArray.getJSONObject(0)
                    val legs = route.getJSONArray("legs")
                    val leg = legs.getJSONObject(0)
                    val distance = leg.getJSONObject("distance")
                    val duration = leg.getJSONObject("duration")
                    val distanceText = distance.getString("text")
                    val durationText = duration.getString("text")

                }
            }
        }*/
    }

    private fun createDriverFound() {
        if (idDriver.isEmpty())
            listDriverFound.forEach {
                lifecycleScope.launchWhenStarted {
                    viewModel.createDriverFound(DriverFound(it, viewModel.idUser.value))
                }
            }
    }


    /*Only 1 driver*/

/*    @ExperimentalCoroutinesApi
    private fun getDirection(latLngDriverLocation: LatLng) =
        lifecycleScope.launchWhenStarted { viewModel.getDirections(latLngOrigin, latLngDriverLocation) }*/

    override fun onDestroyView() {
        super.onDestroyView()
        mHandler.removeCallbacks(runnable)
        mIsFinishSearch = true
        _binding = null
    }
}