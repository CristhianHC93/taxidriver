package ec.com.designtechx.taxidriver.provider

import com.firebase.geofire.GeoFire
import com.firebase.geofire.GeoLocation
import com.firebase.geofire.GeoQueryEventListener
import com.google.android.gms.maps.model.LatLng
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.ktx.Firebase
import ec.com.designtechx.taxidriver.utils.Constants
import ec.com.designtechx.taxidriver.utils.Results
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow

class GeoFireProvider {

    private lateinit var mDataBase: DatabaseReference
    private lateinit var mGeoFire: GeoFire

    fun changeReferent(referent: String): GeoFireProvider {
        mDataBase = Firebase.database.reference.child(referent)
        mGeoFire = GeoFire(mDataBase)
        return this
    }

    fun saveLocation(idDriver: String, latLng: LatLng) {
        mGeoFire.setLocation(idDriver, GeoLocation(latLng.latitude, latLng.longitude))
    }

    fun removeLocation(idDriver: String) {
        mGeoFire.removeLocation(idDriver)
    }

    @ExperimentalCoroutinesApi
    fun getActiveDriver(latLng: LatLng, radius: Double): Flow<Map<String, Any>> = callbackFlow {
        val callback = object : GeoQueryEventListener {
            override fun onKeyEntered(key: String, location: GeoLocation) {
                trySend(
                    mapOf(
                        Pair("NAME", "onKeyEntered"), Pair("KEY", key),
                        Pair("LAT", location.latitude), Pair("LNG", location.longitude)
                    )
                )
            }

            override fun onKeyExited(key: String) {
                trySend(
                    mapOf(
                        Pair("NAME", "onKeyExited"), Pair("KEY", key),
                        Pair("LAT", 0.0), Pair("LNG", 0.0)
                    )
                )
            }

            override fun onKeyMoved(key: String, location: GeoLocation) {
                trySend(
                    mapOf(
                        Pair("NAME", "onKeyMoved"), Pair("KEY", key),
                        Pair("LAT", location.latitude), Pair("LNG", location.longitude)
                    )
                )
            }

            override fun onGeoQueryReady() {
                trySend(
                    mapOf(
                        Pair("NAME", "onGeoQueryReady"), Pair("KEY", Results.OK.toString()),
                        Pair("LAT", 0.0), Pair("LNG", 0.0)
                    )
                )
            }

            override fun onGeoQueryError(error: DatabaseError) {
                trySend(
                    mapOf(
                        Pair("NAME", "onGeoQueryError"), Pair("KEY", error.details),
                        Pair("LAT", 0.0), Pair("LNG", 0.0)
                    )
                )
            }
        }
        val geo = mGeoFire.queryAtLocation(GeoLocation(latLng.latitude, latLng.longitude), radius)
        geo.addGeoQueryEventListener(callback)
        awaitClose { geo.removeGeoQueryEventListener(callback) }
    }

    fun isDriverWorking(idDriver: String): DatabaseReference {
        changeReferent(Constants.WORK_DRIVER)
        return mDataBase.child(idDriver)
    }

    @ExperimentalCoroutinesApi
    fun getLocation(idDriver: String): Flow<LatLng> = callbackFlow {
        val callback = object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                if (snapshot.exists()) {
                    val lat = snapshot.child("0").value.toString().toDouble()
                    val lgn = snapshot.child("1").value.toString().toDouble()
                    trySend(LatLng(lat, lgn))
                }

            }

            override fun onCancelled(error: DatabaseError) {
            }
        }

        mDataBase.child(idDriver).child("l").addValueEventListener(callback)
        awaitClose { mDataBase.child(idDriver).child("l").removeEventListener(callback) }
    }

    companion object {
        var instance: GeoFireProvider? = null
            get() {
                if (field == null)
                    field = GeoFireProvider()
                return field
            }
    }
}